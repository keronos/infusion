---
layout: default
title: l'index
categories: info
---

Ce site propose des recettes élaborées dans le cadre des ateliers culinaires proposés dans les centres sociaux de la rive droite de Bordeaux. 

Elles ont été imaginées par Bénédicte Lambert et Stéphanie Labadie, deux créatrices culinaires animées par l'envie de la transmission, la gourmandise et le plaisir des bon produits.

Vous pouvez explorer par <a href="categories.html">catégories</a>, par autrice ou par niveau de difficulté.
