---
author: Stéphanie Labadie
description: Papillottes de chou farci au riz persan 
title: Papillottes de chou farci au riz persan  
licence: CC-by-sa
categories: plat
niveau: moyen
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 40 min
temps-preparation: 40 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/papillottes_de_chou_farci_au_riz_persan.jpeg
layout: recette
---

# Ingrédients: 

* 1 chou vert 
* 40 cl de bouillon de légumes 
* 400 gr de riz long blanc
* 4 c. à soupe de pistache décortiqués
* 3 c. à soupe de raisin blond
* 1 orange
* ½ c. à café de filament de safran
* 2 c. à soupe de beurre clarifié (Ghee)
* 6 gousses de cardamome
* 1 bâton de cannelle
* 10 grains de poivre noir
* 4 clous de girofle

**ETAPE 1 : le riz Persan** 

Mettre un fond d’eau très chaude dans un petit bol et ajouter les filaments de safran. 

Zester l’orange et presser le jus.

Faire chauffer dans une casserole 1 litre d’eau.

Préparer les épices (bâton de cannelle, gousse de cardamome, grains de poivre et clous de girofle). Prendre une marmite, mettre une noisette de beurre et faire chauffer. Ajouter les épices et laisser cuire pendant 2 mn en remuant. Ajouter le riz et laissez le un peu brunir en remuant souvent pendant 3 mn environ. 

Verser l’eau de la casserole dedans. 

Mélanger et ajouter le bol avec l’eau et les filaments de safran, le jus et le zeste de l’orange. 

Saler, couvrir hermétiquement et laisser mijoter à feu doux pendant 20 mn environ, en remuant de temps en temps. Au besoin, s’il manque de l’eau pour la cuisson du riz en ajouter au fur et à mesure.

En fin de cuisson du riz, en principe, les épices sont remontées à la surface. Retirer le bâton de cannelle, les grains de poivre, les clous de girofle et les gousses de cardamome. 

Ouvrir ces dernières, retirer les graines à l’intérieur et les ajouter au riz.

...............................

Faire griller les pistaches dans une poêle à sec. 

Mettre les raisins blonds, les pistaches grillées, mélanger le tout. 

Remettre le couvercle et laisser cuire encore 5 mn à feu doux.

Il ne doit plus y avoir d’eau.

**ETAPE 2 : les paupiettes de chou farcies au riz** 

Blanchissez les feuilles de chou dans un grand volume d’eau salée pendant 3 minutes environ. 

Puis plongez les dans un bol d’eau froide et égouttez les sur du papier absorbant 

Eliminez la côte centrale si elle est trop grande 

Répartissez la farce au centre de chaque feuille de chou. 

Repliez les vers l’intérieur de façon à former un petit paquet. Maintenez les à l’aide d’un fil alimentaire, ou pas si vos paupiettes se tiennent bien. Auter option: faire des petits boudins, plus pratique !

Préchauffez le four à 180 degrés. Déposez les petits paquets dans un plat allant au four. Ajoutez le bouillon de légumes et faites cuire 15 minutes. Servez chaud avec un peu de bouillon. 

**Astuce**: 

Vous pouvez aussi réaliser cette recette avec des feuilles de vignes pour rester dans l'esprit originel de ce plat iranien. 