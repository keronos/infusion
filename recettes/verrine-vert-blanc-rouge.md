---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: Délicieux mélanges de saveurs d'été relevé au chorizo en verrine.
image_url: .https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/verrine-legumes.png
title: Verrine vert blanc rouge
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 verrines
temps-cuisson: 15 min
temps-preparation: 20 min
---

### Ingrédients

- 500g de tomates pelées, épépinées
- 1 bouquet de basilic
- 125 g de chair d'avocat
- 100 g de chorizo fort
- 250 g de ricotta
- 1 cs de vinaigre de xérès
- 1 blanc d'oeuf
- 2 g d'agar-agar
- sel, poivre

### Préparation

1. Chauffez les tomates dans une casserole, mixez-les
2. Ajoutez agar-agar, vinaigre, sel, faites cuire 1 min. à petits bouillon, laissez refroidir
3. Dorez le chorizo recto verso à feu moyen-doux dans une poêle antiadhésive sans gras. Quand il croustille, égouttez, séchez sur du papier absorbant, ciselez-le en bâtonnets.
4. Quand la purée de tomates commence à figer mais qu'elle n'est pas encore ferme, fouettez le blanc d'oeuf et 1 pincée de sel en neige très ferme ; mêlez à la tomate en tournant de bas en haut avec une spatule. Veuillez à ne pas écraser les blancs
5. Lavez, séchez et ciselez le basilic. Emiettez la ricotta et poivrez-la. Détaillez la chair d'avocat en dès.
6. Montez les verrines en alternant mousse de tomate, basilic ciselé, ricotta poivrée, chorizo et dés d'avocat.
