---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gateau_renverse_aux_pommes_caramel_et_sesame_noir.jpeg
author: Stéphanie Labadie
description: Gâteau renversé aux pommes, caramel et sésame noir 
title: Gâteau renversé aux pommes, caramel et sésame noir  
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 30 min
layout: recette
---

# Pour les fruits: 

5 pommes acidulées 

70 g de beurre demi sel 

130 g de sucre blanc 

.......................................

Pelez les fruits, épépinez les et coupez les en quartiers. 

Dans une sauteuse, faites fondre le beurre avec le sucre. 

Quand la coloration du caramel est atteinte, ajoutez les fruits. 

Laissez cuire 20 à 30 minutes, en remuant les fruits tout en les gardant entiers, jusqu’à ce qu’ils soient gorgés de caramel. 

## Pour la pâte: 

3 oeufs 

50 g de cassonade 

150 g de farine 

1 c à café de levure chimique 

100 g d poudre d’amande

50 g de beurre demi sel fondu 

10 cl de lait 

80 g d égraines de courge 

30 g de graines de sésame noir 

1 pincée de fleur de sel 

.......................................

Préchauffez le four à 180 degrés. 

Torréfiez les graines de courge dans une poêle pendant 2 à 3 minutes. 

Mélangez les oeufs avec la cassonade. Ajoutez la farine, la levure, la poudre d’amande puis le beurre fondu, le lait et la fleur de sel. Incorporez le sésame et les graines de courges torréfiées. 

Disposez les fruits dans un moule à gâteaux préalablement beurré, puis versez la pâte sur les fruits. 

Enfournez 40 minutes. 

### Dressage : 

Démoulez le gâteau encore chaud en faisant attention aux éclaboussures de caramel brûlant. Lassiez refroidir et servez avec de la crème fraîche, une crème anglaise, un coulis de fruits, ou nature, ça marche bien aussi! 