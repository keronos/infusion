---
author: Bénédicte Lambert
description: Potée de chou-rouge à la bière
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/potee-chou-biere.jpg
title: Potée de chou-rouge à la bière
licence: CC-by-sa
categories: plat
niveau: débutant
date: 26/10/2018
nombre: 4 personnes
temps-cuisson: 80 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

* 1/2 chou rouge
* 1 oignon
* 2CS d'huile d'olive
* 1cc de paprika fumé
* 30 cl de bière
* sel, poivre

## Déroulé

1. Couper le chou en petits morceaux ou le passer au robot avec la râpe.  
2. Faire blanchir le chou pendant 5 min dans une casserole d'eau bouillante.  
3. Couper l'oignon en 2 puis en lanières.  
4. Dans un faitout ou une grande casserole, faire chauffer l'huile à feu assez vif.  
5. Jeter les oignons et les faire dorer puis ajouter le paprika fumé.  
6. Faire revenir le chou rouge bien égoutté pendant 5/10 minutes.  
7. Déglacer à la bière et couvrir. Faire cuire 1h environ à couvert.  

## Astuces

Ajouter en fin de cuisson une touche de miel si la potée est trop acide ou amère.  
Parsemer de persil et remplacer le paprika fumé par 150g de  tofu fumé.  