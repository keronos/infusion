---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/croquettes_de_patates_douces_aux_amandes.jpeg
author: Stéphanie Labadie
description: Croquettes de patates douces aux amandes 
title: Croquettes de patates douces aux amandes  
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 04/01/2022
nombre: 4 à 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 30 min
layout: recette
---

# Ingrédients: 

* 400 gr de patates douces 
* 200 de potimarron 
* 100 g de parmesan 
* 100 g d’amandes en poudre 
* 2 oeufs 
* 200 g de chapelure 
* 200 g de farine 
* 100 g d’amandes effilées 

## Déroulé: 

Cuire les patates douces et le potimarron au cuit vapeur, et le mixer en purée 

Mélanger avec les amandes en poudre et le parmesan, assaisonner à votre convenance ( sel, épices, herbes fraîches). 

Dans le creux de la main, moulez de jolies boulettes rondes. Faire de même jusqu’à épuisement de la purée. 

...................................

Pour la panure, mettre la farine dans une assiette, les oeufs battus dans une autre et enfin la chapelure et les amandes effilées dans une 3e assiette. 

Rouler la boulette dans la farine, puis dans les oeufs et enfin dans la panure. Procéder de la sorte deux fois de suite pour chaque boulette si vous souhaitez une grosse couche de panure. 

...................................

Ensuite deux solutions : faire cuire au four les boulettes à 180 degrés pendant 20 à 30 minutes avec une filet d’huile d’olive par dessus, ou les faire frire dans de l’huile de tournesol. 

Servir avec une salade d’herbes fraîches, ou un sandwich type falafel avec des crudités, ou déguster avec une crème aux côtes de chou fleur, ou une sauce vierge. Ou rien du tout, c’est délicieux tout simplement ! 

**ASTUCES**:

Vous pouvez mettre au coeur de ces boulettes ce qui vous fait plaisir, les restes du frigo feront très bien l’affaire : un reste de viande hachées ou de poulet, ou pourquoi pas de la mozzarella ou du comté. 

Ces boulettes peuvent aussi être servies sans rien dedans pour l’apéro en version mini, succès garanti! 