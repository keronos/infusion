---
author: Stéphanie Labadie
description: Carottes râpées aux amandes et à la fleur d’oranger
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/carotte-orientale.jpg
title: Carottes râpées aux amandes et à la fleur d’oranger
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 08/02/2021
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

* 500 g de carottes
* 1 pomme
* 2 oranges
* 1/2 bouquet de coriandre
* 2 c à soupe de jus de citron
* 1 poignée de raisins secs
* 60 g d’amandes émondées
* 3 c à soupe d’huile d’olive
* 3 c à soupe d’eau de fleur d’oranger
* 1 pincée de cumin
* 1 c à café de graines de cumin
* Sel et poivre

## Déroulé

Versez le jus d’une orange dans un bol et ajoutez les raisins secs. Laissez gonfler. Epluchez et râpez les carottes et la pomme . Lavez et ciselez la coriandre. Levez les suprêmes de l’autre orange et coupez les quartiers en deux dans la longueur.

Préparez la sauce dans le saladier: mélangez le jus de citron, l’eau de fleur d’oranger, une pincée de cumin, les graines de cumin, la coriandre ciselée, du sel et du poivre, puis ajoutez l’huile d’olive.

Versez les carottes râpées, ajoutez les demi-quartiers d’orange et mélangez soigneusement.

Réservez au réfrigérateur 30 mn.

Concassez grossièrement les amandes dans un mortier. Avant de servir, ajoutez les raisons secs et leurs jus aux carottes, mélangez et parsemez d’amandes concassées. Servez frais.

ASTUCES ///

Vous pouvez remplacer les carottes par du chou vert.