---
author: Stéphanie Labadie
description: Lemon curd à la noix de coco et petits fond de tarte aux zestes de citron
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/lemon-curd.jpg
title: Lemon curd coco
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 20 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

Pour le lemon curd:

- 10 cl de jus de citron
- 75 g de sucre
- 3 oeufs
- 1 cuillère à soupe de maïzena
- 30 g de noix de coco râpée

Fonds de tarte:

- 170 g de farine
- 80 g de poudre d’amande
- 130 g de beurre
- 50 g de sucre muscovado
- 1 oeuf
- Le zeste d’un citron vert

## Déroulé

Lavez les citrons et prélevez les zestes au-dessus d'une casserole.

Dans une terrine, battez les oeufs et le sucre. Ajoutez le jus de citron et la Maïzena diluée dans l’eau. Versez dans la casserole qui contient les zestes et ajoutez le beurre en morceaux.

Faites cuire à feu doux jusqu'à épaississement (5 minutes) sans cesser de remuer. Rajoutez la moitié de la noix de coco râpée.

Otez du feu et mettez en pot.

Mélanger la farine, la poudre d’amande et le sucre. Ajouter le beurre en petit morceaux, effritez entre vos mains pour obtenir une consistance sableuse. Ajoutez la moitié du zeste, mélanger avec l’oeuf, un peu d’eau au besoin, et pétrir. Réserver et mettre au frais au moins 1 heure.

L’étaler sur des petits fonds de tarte ou sous forme de biscuits ronds.

Enfournez de 10 à 25 minutes selon la taille due fond de tarte à cuire.

**Dressage**: disposez des petits cuillères de lemon curd sur les fonds de tarte ou les biscuits cuits et refroidis. Saupoudrez de noix de coco râpée, et de un deux zestes de citron restants ou de pistache concassée.

**Petit plus**: Vous pouvez garder le lemon curd en pot et le servir avec des yaourts ou sur du bon pain grillé, voir des tranches de brioche. Agrémenté de quelques fruits frais émincés: fraises, pêches, poires, pommes etc…

**Variante** : existe en version [vegan](lemon-curd-vegan.html)
