---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/flatebreads_pesto_au_yaourt_legumes_rotis.jpeg
author: Stéphanie Labadie
description: Flatbreads, pesto au yaourt et légumes rôtis 
title: Flatbreads, pesto au yaourt et légumes rôtis 
licence: CC-by-sa
categories: plat
niveau: moyen
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 40 min
layout: recette
---

# Pour les flats breads : 

350 g de farine 

350 g de yaourt 

1 c à café de levure chimique 

2 pincées de sel 

2 c à soupe d’huile d’olive 

........................................................

Mélanger les ingrédients avec une spatule dans un saladier, puis pétrir à la main. 

Former une boule et laisser reposer 30 minutes à température ambiante dans le récipient couvert de film alimentaire. 

Divisez la pâte en 6 pâtons, les étaler au rouleau à pâtisserie sur le plan de travail légèrement fariné, sur une épaisseur de 5 mm maximum ( ou plus fin si vous aimez la pâte un peu plus croustillante ) . 

Cuire les flatbreads dans une poêle anti adhésive quelques minutes sur chaque face, selon l'épaisseur. 

Vous pouvez aussi enfourner à 180 degrés au four. Au bout de 10 minutes , vous pouvez étaler un mélange de beurre et d’ail haché, comme un garlic bread, plus enfourner encore pour 5 minutes ! 

Une fois vos flatbreads cuits, vous pouvez les garnir! 

## Pour le pesto :

1 bouquet de basilic ( ou de persil / coriandre/ aneth etc) 

1 petite gousse d’ail 

20 g de noisettes torréfiées

20 g d’amandes torréfiées 

20 g de yaourt

40 g de parmesan râpé 

5 cl d’huile d’olive 

Le zeste d’1/2 citron 

1/2 c à café de gros sel 

Poivre du moulin 

........................................................

Dans le bol du mixeur, mettre la gousse d’ail pelée et grossièrement hâchée, l’herbe fraîche et le gros sel. Donner une impulsion rapide. Ajouter les noisettes et les amandes, pulser de nouveau. Ajoutez le yaourt, le zeste de citron pulser. Ajouter enfin le parmesan et l’huile d’olive. Pulsez et réserver.  

### Pour les légumes rôtis :

Les légumes de votre choix ( patate douce, potimarron, navets, oignons etc…)

4 gousses d’ail écrasées 

3 c à soupe d’huile d’olive 

1 c à café de moutarde à l’ ancienne 

1 c à soupe de sirop d’érable 

1 pincée de sel 

........................................................

Couper les légumes en cube de 1 cm de côté. Peler et écraser les gousses d’ail. 

Mélanger tous les autres ingrédients, et enrober les légumes avec cette marinade. 

Enfourner à 180 degrés pendant 20 à 30 minutes. Ajouter des feuille de sauge ciselée ou quelques brins de romarin, ou du thym si vous en avez. Ou des épices. 

#### Pour le dressage :

Garnir les flatbreads d’une bonne cuillère à soupe de pesto, puis disposer par dessus les légumes rôtis, avec quelques feuilles de coriandre ou une cuillère de crème citronnée par dessus. 

Bon appétit ! 