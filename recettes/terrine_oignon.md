---
author: Stéphanie Labadie
description: Petites terrines réconfortantes aux oignons confits, champignons et tofu fumé
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/terrine-oignon.png
title: Terrines aux oignons confits
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

### Ingrédients

- 3 oignons
- 300 g de champignons
- 1 gousse d’ail
- 2 échalotes
- 100 g de tofu fumé
- 70 g de féta
- 1 c à soupe de persil haché
- 8 pruneaux
- 25 cl de crème
- 2 oeufs
- Sel, poivre et épices ( curry, cannelle, curcuma etc…)

### Déroulé

Pour la cuisson des champignons:

1. les hâcher grossièrement, y compris les pieds, puis ciseler finement les échalotes.
2. Faites chauffer le beurre dans une casserole avec les échalotes, les faire revenir jusqu’à ce qu’une bonne odeur se dégage puis ajoutez les champignons et laissez cuire, toujours sur feu doux, en remuant de temps en temps avec une spatule en bois, ajouter l’ail haché à mi cuisson et attendre jusqu'à ce que toute l'eau de végétation soit évaporée.
3. Assaisonner avec le sel et le persil hâché.

Pour les oignons:

1. les ciseler et les faire revenir dans une grosse noisette de beurre, avec une cuillère à café de sucre.
2. Laisser cuire à feu doux et compote légèrement.
3. Y ajouter au besoin une pincée d’épices pour donner le gout du voyage!
4. Couper le tofu et la féta en petits cubes et les mélanger aux pruneaux coupés grossièrement.
5. Dans des petits ramequins: tapisser le fond d’oignons confits, disposer au dessus une couche de duxelles de champignons, puis le mélange tofu/féta/pruneaux, puis renouveler deux fois l’opération.
6. Verser le mélange crême oeufs, tasser bien les ingrédients dans le ramequin, et enfourner 15 minutes à 180 degrés.
7. Sortir du four et déguster froid ou encore tiède sur du bon pain grillé, ou en accompagnement d’une salade.

**Astuces**
Vous pouvez vous amuser à décliner ces verrines avec les ingrédients de votre choix, selon les restes de votre frigo.
