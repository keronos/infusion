---
author: Bénédicte Lambert
description: couronne friandise pour le carnaval
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/pate-d-amande.jpg
title: Couronne en pâte d'amandes
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 25/02/2019
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 5 min
layout: recette
---

## Ingrédients

Pour combien de couronnes ?

- 200g d'amandes
- 50g de miel
  parfums
- 1CS de jus de betterave
- ou 1cc de charbon actif + 3 gouttes d'HE de mandarine
- ou 1cc d'arôme d'amandes amères

## Déroulé

1. Moudre les amandes pour les réduire en poudre.
2. Ajouter le miel dans le robot et mixer.
3. Ajouter le parfum ou le colorant.
4. Étaler la pâte à l'aide d'un rouleau à pâtisserie et de papier sulfurisé.
5. Faire reposer au frais puis découper des formes à l'emporte-pièces.

## coût unitaire

## provenance des produits

- miel d'acacia de la Borda d'ambrosie fabriqué à Langon (Gironde)
- Betteraves bio produites à Bourg-sur-Gironde (Gironde)
- amandes bio roduites en Italie
