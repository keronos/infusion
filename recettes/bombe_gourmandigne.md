---
author: Stéphanie Labadie
description: Bombes de gourmandises aux amandes et fromage frais
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/bombe_gourmandigne.jpeg
title: Bombes de gourmandises
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 19/05/2021
nombre: 4 personnes
temps-cuisson: 10 min
temps-preparation: 30 min
layout: recette
---

## Ingrédients (pour 4 pers) :

* 400 gr de patates douces
* 200 de potimarron
* 100 g de parmesan
* 100 g d’amandes en poudre
* 200 g de fromage de chèvre frais
* 2 oeufs
* 200 g de chapelure
* 200 g de farine
* 100 g d’amandes effilées

## Déroulé :

Cuire les patates douces et le potimarron au cuit vapeur, et le mixer en purée

Mélanger avec les amandes en poudre et le parmesan, assaisonner à votre convenance ( sel, épices, herbes fraîches).

Dans le creux de la main, mettre une petite poignée de purée, disposer une pépite de fromage frais, et façonner une boulette par dessus.

Faire de même jusqu’à épuisement de la purée.

Pour la panure, mettre la farine dans une assiette, les oeufs battus dans une autre et enfin la chapelure et les amandes effilées dans une 3e assiette.

Rouler la boulette dans la farine, puis dans les oeufs et enfin dans la panure. Procéder de la sorte
deux fois de suite pour chaque boulette si vous souhaitez une grosse couche de panure.

Ensuite deux solution : faire cuire au four les boulettes à 180 degrés pendant 20 à 30 minutes avec
une filet d’huile d’olive par dessus, ou les faire frire dans de l’huile de tournesol.

Servir avec une salade d’herbes fraîches, ou un sandwich type falafel avec des crudités, ou déguster
avec une crème aux côtes de chou fleur, ou une sauce vierge. Ou rien du tout, c’est délicieux tout simplement !

Astuces : mélangez petit à petit la purée de légumes avec le mélange de poudre d’amandes et de parmesan, vous devez avoir une consistance assez solide pour confectionner des boulettes. S’il reste de la purée, vous pourrez la déguster en accompagnement d’un autre plat.

ASTUCES : vous pouvez mettre au coeur de ces boulettes ce qui vous fait plaisir, les restes du frigo
feront très bien l’affaire : un reste de viande hachées ou de poulet, ou pourquoi pas de la mozzarella
ou du comté.

Ces boulettes peuvent aussi être servies sans rien dedans pour l’apéro en version mini, succès garanti!