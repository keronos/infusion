---
author: Stéphanie Labadie
description: Croquettes d’avoines au maquereau, patates douces et épinards, mayonnaise légère à la betterave
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/croquettes-avoine-maquereau.jpeg
title: Croquettes d’avoines au maquereau
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 15/10/2021
nombre: 4 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 500 g de pommes de terre
* 1 petit oignon finement haché
* 150 g d’épinards
* 2 c à soupe de coriandre ciselée
* 250 g de maquereau fumé
* 15 g d beurre
* 3 oeufs
* 1 c à cafe de moutarde
* Jus de citron
* 75 g de farine salée et privée
* 15 cl d’huile de tournesol
* 100 g de flocons d’avoine
* 200 g de chapelure

### Déroulé
Faite cuire les pommes de terre à l’eau, lorsqu’elles sont bien tendres, retirez la peau et écrasez la chair.

Retirez la peau du maquereau et émiettez le.

Faite fondre l’oignon dans le beurre. Laissez dorer, ajouter 1/2 c à café de curry ou d’épice de
voter choix. Réservez.

Lavez les épinards et faites les cuire dans une casserole avec une cuillère d’eau, couvrez et
laissez cuire 90 sec à feu très doux. Egouttez, pressez pour enlever l’excèdent d’eau puis hachez grossièrement. Réservez.

Mélanger les pommes de terre avec les oignons et les épinards. Ajoutez un oeuf puis le poisson en prenant garde de ne pas trop l’émietter. Farinez vous les mains et formez de croquettes d’environ 8 cm de diamètres. Réservez les 30 minutes au frigo.

Pendant ce temps, réalisez la mayonnaise. Mélangez votre jaune d’oeuf avec la moutarde puis faites monter la mayonnaise avec l’huile de tournesol, aoutez 2 c à café de jus de citron (ou plus selon votre gout ), salez. Râpez la moitié d’une petite betterave cuite ou crue, et mélangez là à la mayonnaise. 

Faite monter le blanc de l’oeuf en neige, puis incorporez le délicatement à la mayonnaise. Rectifiez l’assaisonnement si besoin. Réservez au frais.

Prenez 3 assiettes, avec 1 oeuf battu, la farine, et les flocons d’avoine+ chapelure.

Trempez successivement les croquettes dans la farine, les oeufs et le mélange avoine/
chapelure. 

Faites chauffer l’huile dans une sauteuse et faites frire les croquettes 3 minutes environ de chaque côté.

Réduisez le feu quand elles ont une belle croûte dorée.

Vous pouvez aussi faire cuire vos boulettes au four 20 à 30 minutes à 180 degrés.

Servez avec la mayonnaise à la betterave, et une petite salade d’herbes fraîches.