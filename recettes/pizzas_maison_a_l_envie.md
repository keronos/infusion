---
author: Stéphanie Labadie
description: Pizza maison à l'envie 
title: Pizza maison à l'envie 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/pizzas_maison_a_l_envie.jpeg
layout: recette
---

# Pour la pâte à pizza: 

Pour une pâte d'environ 760 g :

* 500 g de farine T55. 
* 250- 260 ml d’eau 
* 1 cuillerée à café de sel
* 2 cuillerées à soupe d'huile d’olive
* 20 g de levure boulangère fraîche, ou 10 g de levure sèche.

.............................................

Verser la farine dans un saladier, y creuser un puits et ajouter eau et sel. Mélanger à la spatule en ajoutant l'huile d’olive

Dans un petit bol, faire fondre la levure dans un peu d'eau tiède avec 1 pincée de sucre puis l'ajouter en dernier

Malaxer jusqu'à rendre la pâte homogène et faire une boule qui se détache des parois. 

Note : si besoin ajuster la quantité d'eau si la pâte est un peu dure en en rajoutant quelques gouttes au fur et à mesure car la quantité exacte dépend de la farine 

Mettre un torchon dessus et laisser reposer à température ambiante/ tiède pendant 1 h environ, le temps que la pâte double de volume

Pétrir à nouveau la pâte juste pour chasser le gaz puis la diviser en 3 pâtons (d'environ 250/260 g), ou 4... selon son utilisation et les étaler sur une plaque de cuisson huilée. 

Laisser reposer une bonne 1/2 h puis garnir selon la recette de pizza choisie ! 

## Pour le pesto d’herbes fraîches:

* 1 botte de persil / coriandre/menthe etc… 
* 1 gousse d’ail 
* 6 abricots secs 
* 1 poignée d’amandes ( torréfiées c’est encore mieux )
* 1 filet de jus de citron 
* 5 à 8 cl d’huile d’olive, selon la consistance choisie ( plus ou moins épaisse) 

........................................

Ciseler très grossièrement les herbes fraîches et les mixer avec le reste des ingrédients. 

A déguster sur des pommes de terre bouillies, avec des pâtes, une salade, une viande, un poisson, un fromage de chèvre frais, des légumes crus à l’apéro ( carottes, celeri, pomme, concombres etc..)… ou une pizza!!! 

Cette recette peut se faire avec toutes les herbes fraîches, le pesto aura a chaque un gout différent et tout aussi délicieux: persil, aneth, ciboulette, cerfeuil etc…

### Pour le ketchup : 

* 200 g de de concentré de tomates
* 2 à 3 càs de sucre de canne roux
* 2 à 3 càs de vinaigre de cidre
* 100 ml d’eau
* Sel

........................................

Dans un poêlon, déposer le concentré de tomate, l’eau, le sucre et le vinaigre.

Cuire dix minutes à petits bouillons, saler. Remuer régulièrement.

Si le ketchup devient trop dense, rajouter un peu d’eau. Goûter et rectifier l’assaisonnement en sucre, sel , vinaigre. Il faut un bon équilibre entre les saveurs. On peut agrémenter ce ketchup d’épices, comme piment fumé, paprika, curry etc.

#### Garniture des pizzas: 

- courgette 
- Ricotta 
- Olives noires 
- Tomates cerises confites ( au four 20 mn à 160 degrés avec un filet d’huile d’olive/2 pincée de sel/1 c à café de sucre/sauge et romarin)
- Baies roses 
- Pesto d’herbes : persil/menthe/coriandre 
- Sauce ketchup maison 

Une fois votre garniture en place, deux solutions: déguster la pizza préalablement cuite avec la garniture fraîche dessus, ou faire cuite votre pizza entière entre 15 et 30 minute selon sa taille. 

Attention: ne rejoutez le basilic frais qu'une fois voter pizza sortie du four, avec un filet d'huile d'olive c'est parfait! 