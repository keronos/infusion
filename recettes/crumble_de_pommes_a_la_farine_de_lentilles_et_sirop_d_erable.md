---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/crumble_de_pommes_a_la_farine_de_lentilles_et_sirop_d_erable.jpeg
author: Stéphanie Labadie
description: Crumble de pommes à la farine de lentilles et sirop d'érable 
title:  Crumble de pommes à la farine de lentilles et sirop d'érable 
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette 
---

# Ingrédients: 

* 75 g de beurre 
* 60 g de sucre 
* 50 g de farine de lentilles 
* 4 pommes coupées en cubes 
* 500 g de raisins dénoyautés  
* 15 g de beurre coupé en cubes 
* 2 c à soupe de sirop d'érable 
* 2 c à soupe de sucre de canne 

.......................................

Chauffez votre four à 180 degrés.

Dans le fond d’un plat, disposez les fruits avec le beurre, le sucre et le sirop d'érable, et faites cuire pendant 20 minutes. 

Pendant ce temps, préparez votre pâte à crumble en effritant la farine, le beurre et le sucre, de sorte à obtenir une consistance sableuse. Parsemez vos fruits de ce mélange. Enfournez à nouveau pendant 20 minutes à 180 degrés. 

Servez tiède ou froid tel quel, ou avec une grosse cuillère de crème fraîche! 