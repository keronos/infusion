---
author: Bénédicte Lambert
description: Préparation à base de vinaigre permettant de conserver les légumes pour agrémenter salades ou plats cuisinés.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/prepa_chutney.png
title: Chutney de betterave
licence: CC-by-sa
categories: accompagnement 
niveau: débutant
date: 09/11/2018
nombre: 4 personnes
temps-cuisson: 45 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 400 g de betteraves
* 2 pommes en cubes
* 1 oignon coupé en fines lamelles
* 1 gousse d'ail
* 1 bâton de cannelle
* 250 ml de vinaigre de cidre de pomme
* 60 g de sucre
* 30 g de raisins secs
* sel, poivre

### Déroulé  

Dans une casserole, mettre la betterave râpée, les pommes en cubes, l'oignon en fines lamelles, l'ail haché, la cannelle, le sel, le poivre et le vinaigre, le sucre et les raisins puis mélangez le tout.

Faire cuire à feu doux pendant 45 minutes en remuant régulièrement.  

### Astuces

Remplacer la pomme par de la mangue.