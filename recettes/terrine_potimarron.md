---
author: Stéphanie Labadie
description: Terrines de potimarron confit, raisins, ciboulette et zestes de citron, crackers au sarrasin et graines de sésame
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/terrine_potimarron.jpg
title: Terrines de potimarron
licence: CC-by-Sa
categories: plat
niveau: débutant
date: 12/10/2021
nombre: 6 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

### Pour des petites terrines (pour 6 personnes)
* 1/2 potimarron ( ou butternut) coupé en petits dés
* 2 petites patates douces coupé en petits dés
* 3 gousses d’ail écrasée
* Huile d’olive
* 40 g de raisins secs ( ou tout autre fruit sec )
* 1 ciboule / ou ciboulette ciselée
* Le zeste d’un 1/2 de citron
* Un pointe de jus de citron

## Déroulé

Mettre les patates douces, le potimarron et l’ail à rôtir au four à 180 degrés, avec du sel et éventuellement une pincée de cumin et/ou de cannelle

Dans un saladier, mélanger la ciboulette, le citron et les légumes cuits écrasées à la fourchette, puis
réserver au frigo au moins 2 heures. 

Rajouter une cuillère soupe de yaourt bulgare si le coeur vous
en dit ( pour une texture plus crêmeuse).