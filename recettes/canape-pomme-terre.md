---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: Mélange pommes de terre et thon parfumé aux herbes et au citron.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/canape-thon.png
title: Canapés de pomme de terre
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
---

### Ingrédients

- 12 petites pommes de terre à chair ferme
- 24 tomates séchées
- 1 boîte de thon
- 2 cs de câpres
- 2 cs d'huile d'olive
- 1 citron
- 1 cs de persil ciselé

### Préparation

1. Lavez les pommes de terre, puis plongez-les dans une casserolles d'eau bouillante salée
2. comptez 15 min de cuisson et égoutez
3. Laissez refroidir, puis coupez les en 2 dans le sens de la longueur
4. lavez et sécher le citron. Râpez le zeste d'une demi-citron au-dessus du bol, ajoutez le persil ciselé, l'huile et les câpres, remuez
5. Egouttez les tomates séchées à l'huile d'olive
6. Pressez le citron. Egoutez le thon, émiettez-le.
7. Dans un autre bol, mélangez le avec le jus du citron, du sel et du poivre
8. Mettez une petite cuillère de préparation au thon sur chaque demi pomme de terre, puis parsemez du mélange zeste câpres.
9. Posez une lamelle de tomate séchée.
