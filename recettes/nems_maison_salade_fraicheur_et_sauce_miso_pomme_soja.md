---
author: Stéphanie Labadie
description: Nems maison, salade fraîcheur, et sauce miso pomme soja  
title: Nems maison, salade fraîcheur, et sauce miso pomme soja   
licence: CC-by-sa
categories: plat
niveau: moyen
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 20 à 30 min
temps-preparation: 30 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/nems_maison_salade_fraîcheur_et_sauce_miso_pomme_soja.jpeg
---

# Pour une quinzaine de nems: 

* 15 feuilles de brick 
* 150 de tofu fumé ou aux herbes 
* 1 poignées de raisons secs 
* 1/2 chou blanc ou vert 
* 1 oignons ciselé 
* 2 carottes taillées en allumettes 
* 1 c à café de miel 
* Jus de citron 

................................................

Préchauffez votre four à 180 degrés.

Coupez finement ( à la mandoline) votre chou. Faites rissoler du beurre dans la polée, et faire dorer l’oignon, puis ajouter le chou, avec 2 c à soupe d’eau ( ou de jus de pomme ou de clémentine, meilleur!), 1 c à soupe de soja, 1 à café de miel et une gousse d’ail écrasée. Couvrez pendant 10 minutes à petit feu. 

Rectifiez l’assaisonnement puis ajoutez votre tofu émietté, les raisins, et les carottes. 

Laissez cuire encore, ajoutez un peu de jus de citron pour donner du peps, et laissez refroidir puis réservez. 

................................................

Etalez votre 1ere feuille de brick. Disposez un peu carré de farce au tofu, en partant du bas, pour laisser une bande en haut du cercle. Puis enroulez votre nem sur lui même, rabattez les bord puis terminez de rouler pour former un cylindre. Ainsi de suite avec les autres nems. 

Disposez vos nems sur une plaque avec du papier cuisson. Badigeonnez vos nems d’huile olive ou d’arachide, et enfournez pendant 20 à 30 minutes. Les retournez à mi cuisson pour qu’ils soient croustillants sur toute la surface. 

Autre solution: les faire frire, vous aurez un résultat plus croustillant, plus proche de ce que sont les noms en général. 

## Pour la sauce des nems: 

1 c à soupe de pureé d’arachide ou d’amandes 

5 à soupe de jus de pomme 

1 c à soupe de vinaigre de riz 

1 c à soupe de soja 

2 c à café de miel 

1 c à soupe d'huile de sésame grillée 

................................................

Mélangez tous les ingrédients au fouet pour bien amalgamer tous les éléments, réservez. 

### Pour la salade: 

1/2 chou blanc 

1 pomme

2 carottes 

1 poignée de cacahuètes

1/2 botte de coriandre 

Ciboule 

**Et la vinaigrette**: 

- 1 c à soupe d’huile de sésame grillé 
- 2 c à soupe d’huile d’olive 
- 1 c à soupe de soja 
- 1 c à soupe de sirop d’érable ou d’agave ou de miel 
- 1 c à soupe de vinaigre balsamique

#### Dressage : 

Mélangez votre vinaigrette à la salade, puis disposez les noms dessus, avec quelques cacahuètes concassées et feuilles de coriandre, avec la sauce des nems à disposition dans une coupelle. 