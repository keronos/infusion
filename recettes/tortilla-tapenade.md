---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: Tortilla à la tapenade.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tortilla-tapenade.jpg
title: Tortilla à la tapenade
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 10 min
temps-preparation: 15 min
---

### Ingrédients

- 6 oeufs
- 2 cs de crème liquide
- 2 cs de fromage de brebis frais
- 2 cc de tapenade noire
- 2 cs d'huile d'olive
- sel, poivre

### Préparation

1. Cassez les oeufs dans une jatte
2. ajoutez la crême, salez et poivrez
3. mettez ue poêle anti-adhésive à chauffer avec un filet d'huile d'olive
4. Faites cuire les oeufs en 2 ou 3 fois, selon la taille de votre poêle, pour obtenir 2 à 3 omelettes
5. Dans un bol mélangez la brousse et la tapenade, poivrez
6. Etalez une fine couche de ce mélange sur les crêpes d'omelette puis roulez en serrant délicatement
7. coupez en tronçons et maintenez avec des piques en bois
