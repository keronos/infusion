---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/blinis_au_sarrasin_fondue_de_poireaux_feta_cranberries.jpeg
author: Stéphanie Labadie
description: Blinis au sarrasin, fondue de poireaux à la féta et cranberries 
title: Blinis au sarrasin, fondue de poireaux à la féta et cranberries 
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
---

# Pour les blinis: 

150 g de farine blanche ( ou d’épeautre si intolérance au gluten) 

150 de farine de Sarrazin  

50 cl de lait tiède 

15 g de levure boulangère 

1/2 botte d’aneth ou de coriandre grossièrement émincée 

3 oeufs 

1 c à soupe de crème fraîche 

...........................................

Mêler les deux farines, ajouter le sel, 3 jaunes d’œufs et la levure délayée dans 1/2 verre de lait tiède.
 Remuer délicatement en ajoutant du lait peu à peu (ne pas forcément tout mettre) jusqu'à obtenir une pâte à crêpes épaisse.

 Laisser reposer 2 à 3 heures dans un endroit tiède (pour que la pâte gonfle).

 Au moment de faire les blinis, ajouter les blancs d’œufs battus en neige, la crème et les herbes fraîches.

Faire cuire dans une poêle par petites louches.

Faire dorer comme des crêpes.

Réservez. 

## Pour la fondue de poireaux : 

2 poireaux 

50 de feta 

1 poignée de cranberries 

Emincez finement les poireaux, et faites les fondre à feu doux dans une poêle avec avec grosse noisette de beurre. 

Pour ne pas qu'ils déssèchent, ajoutez un peu de jus de mandarine ou à défaut un filet d'eau.

Salez et laissez cuire jusqu’à ce que les poireaux fondent en bouche. 

Laissez refroidir. 

Ajoutez la féta émiettée et les cranberries. 

### Dressage : 

Au moment de servir, disposez peu de fondue à la feta et cranberries, c'est prêt!  