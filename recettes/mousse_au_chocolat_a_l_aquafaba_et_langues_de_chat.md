---
author: Stéphanie Labadie
description: Mousse au chocolat à l’aquafaba et ses langues de chat
title: Mousse au chocolat à l’aquafaba et ses langues de chat 
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 17/01/2022
nombre: 4 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/mousse_au_chocolat_a_l_aquafaba_et_langues_de_chat.jpeg
---

# Pour la mousse au chocolat :

* 150 g de chocolat noir pâtissier
* 150 ml de jus d’une petite boite de pois chiches ( ou haricots rouges ou blancs (ce liquide est aussi appelé *aquafaba*)
* 1 càs de sucre glace 

..................................

Placer le jus de pois chiches dans un saladier au réfrigérateur pendant 10 min.

Battre le jus à l’aide d’un fouet électrique puissant 

Monter le jus en neige pendant 5 minutes.

Ajouter le sucre petit à petit tout en continuant à battre. Le sucre agit comme un stabilisateur. Le mélange final (5 minutes plus tard) doit être épais et onctueux : il ne doit pas tomber lorsque vous retournez le récipient.

Faire fondre le chocolat cassé en morceaux au bain-marie.

Incorporer délicatement le blanc au chocolat à l’aide d’une spatule.

Répartir la mousse dans 4 verres ou ramequins.

Les placer au réfrigérateur. C’est prêt 1 h plus tard : régalez-vous !

## Pour les langues de chat : 

* 60gr de beurre mou
* 90gr de sucre de canne
* 1 œuf
* 60gr de farine

............................

Allumez le four à 190 degrés 

Commencez par fouetter le beurre mou et le sucre.

Ajoutez l’oeuf.

Incorporez la farine.

Confectionnez une poche à douille avec une sachet en plastique, ou prendre une poche à douille si vous êtes bien équipé(e))! 

Remplir votre poche de la préparation, et la ramèner un maximum dans un des coins.

Préparez une plaque chemisée allant au four.

Dessinez de fines lignes de pâte de 7 cm environ avec ta poche à douille dont vous aurez coupé un cm dans le coin avec la pâte.

Placez la plaque au four et laisse cuire 12 à 15 minutes, selon la taille des biscuits.

Vous pouvez sortir les biscuits du four dès que les bords sont bien dorés.

Attendez quelques minutes avant de les bouger, ils sont fragiles. Puis laisser les durcir en refroidissant. 

Mangez les tous d’un coup, ou gardez une petite réserve dans une boîte hermétique. 