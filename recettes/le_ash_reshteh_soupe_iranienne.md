---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/le_ash_reshteh_soupe_iranienne.jpeg
author: Stéphanie Labadie
description: Le Ash Reshteh soupe iranienne aux légumineuses, verdure et nouilles  
title: Le Ash Reshteh soupe iranienne aux légumineuses, verdure et nouilles  
licence: CC-by-sa
categories: plat
niveau: moyen
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 60 min
temps-preparation: 50 min
layout: recette
---

# Ingrédients: 

* 100 g de pois chiches secs
* 100 g de haricots rouges secs
* 100  g de lentilles vertes 
* 100 g de lentilles blondes ou vertes sèches
* 400 g d’épinards, de préférence des jeunes feuilles ou des pousses
* 100 g de coriandre
* 75 g de persil
* 75 g de ciboulette ou de verts d’oignon
* 150 g de nouilles iraniennes « reshteh » ou de nouilles chinoises au blé tendre, ou à défaut de linguine
* 100 à 150 g d’oignon frit
* 15 à 30 g d’ail frit (du commerce ou fait maison, voir ci-dessous)
* 50 g de kashk (produit laitier iranien), substituer par 35 g de crème fraîche mélangée à 15 g de parmesan, ou juste de la crème ou du yaourt

**ETAPE 1 : l’ail frit**

Épluchez 50 g d’ail et émincez-le finement au couteau, au robot, ou avec une râpe bien tranchante et pas trop fine (on ne veut pas de la purée)

Chauffer 5 à 10 millimètres d’huile dans une petite poêle et ajoutez l’ail. Saupoudrez 1⁄4 c. à café de curcuma. Faites frire à feu moyen en remuant de temps en temps, pendant 5 à 10 minutes, jusqu’à ce que l’ail soit doré. Surveillez bien car l’ail peut vite brûler. 

Dès que l’ail est doré, transférez le dans une passoire fine et égouttez.

**ETAPE 2: l’oignon frit** 

Épluchez les oignons, coupez-les en deux, enlevez la racine avec la pointe du couteau, puis émincez en tranches de 3 mm environ.

Faites chauffer une grande casserole d’eau avec 1⁄2 c. à café de sel. Quand l’eau bout, ajoutez les oignons, et faites mijoter à feu vif pendant 5 - 10 minutes. Égouttez les oignons et étalez-les pour les laisser sécher au moins 30 minutes.

Faites chauffer 1 cm d’huile dans une poêle et quand elle est bien chaude. Ajoutez tout ou une partie des oignons en fonction de la taille de la poêle, et saupoudrez un peu de curcuma. Faites chauffer à feu vif pendant 15 à 20 minutes, jusqu’à ce que l’oignon soit doré et croustillant. Transférez dans une passoire fine et égouttez. Répétez avec le reste de l’oignon.

**ETAPE 3: la soupe**

La veille, mettez à tremper les légumineuses sauf les lentilles. 

Placez ensemble les pois chiches, les haricots rouges dans un grand bol d’eau. Laissez tremper 24h en changeant l’eau 2 ou 3 fois.

Lavez les épinards et les herbes aromatiques et séchez les dans une essoreuse à salade. 

Coupez les épinards grossièrement au couteau en morceaux de 5 cm environ. 

Hachez toutes les herbes aromatiques, tiges comprises, en morceaux de 1 cm maximum.

.....................

Égouttez les haricots et les pois chiche, et placez-les dans une grande marmite avec 1 litre d’eau et une petite partie de l’ail et l’oignon frit.

Portez à ébullition et faites mijoter jusqu’à ce que les haricots soient tendres.

Ajoutez les lentilles, ainsi que les épinards et les herbes. 

Mélangez et ajoutez un peu d’eau si besoin pour que les légumes soient couverts ( 25 cl à peu près). Laissez mijoter encore 1 h.

Réservez une partie de l’oignon et l’ail frit pour servir, et ajoutez le reste à la marmite. 

Ajoutez également les 150 g de nouilles en les cassant en deux.

Mélangez délicatement les nouilles pour bien les séparer. 

Attention, si les nouilles ne sont pas bien mélangées elles vont se coller pendant la cuisson et sont ensuite très difficiles à séparer. Faites cuire encore une trentaine de minutes en mélangeant de temps en temps. 

Ajoutez encore un peu d’eau si le mélange épaissit trop et colle au fond. La consistance finale doit être semi-épaisse, comme celle d’un yaourt brassé.

............................

Préparez le kashk en diluant avec un tout petit peu d’eau, afin d’obtenir la consistance d’une crème épaisse (1 à 2 c. à café suffisent). Ou faites la avec la crême fraîche et le parmesan. 

Ajoutez la moitié à la marmite (ou quantité au goût) et gardez le reste pour servir.

Enfin, ajustez le sel (comme les nouilles reshteh et le kashk sont très salés il vaut mieux le faire à la fin).

.............................

Servez dans des bols ou des assiettes creuses, parsemé de kashk, d’oignon frit, d’ail frit et de menthe ciselée



**Astuce**: les oignons frits sont en option, ne vous embêtez pas à passer forécement par cette étape, votre soupe sera très bonne aussi ! 

