---
author: Stéphanie Labadie
description: Barre de céréales aux pommes, abricots et noix de coco 
title: Barre de céréales aux pommes, abricots et noix de coco 
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 17/01/2022
nombre: pour 12 barres 
temps-cuisson: 30 min
temps-preparation: 20 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/barre_de_cereales_aux_pommes_abricots _et_noix_de_coco.jpeg
---

# Ingrédients: 

* 130 g de flocons d’avoine 
* 100 g de pomme cuite écrasée 
* 20 g de noix de coco râpée 
* 60 g de noix de cajou
* 60 g d’abricots moelleux grossièrement coupés 
* 20 g de dattes coupées grossièrement coupés
* 30 g d’huile de coco 
* 35 g de miel 
* 30 g de purée d’amandes

## Déroulé: 

Faites chauffer l’huile de coco avec le miel et la purée de cacahuètes. 

Mixez grossièrement les abricots avec les noix de cajou.

Mélangez les flocons d’avoine avec la pomme cuite et la poudre cajou abricots. Incorporez le mélange fondu avec le miel. 

Disposez la pâte obtenue dans les empreintes d’un moule en silicone tassant bien, sinon dans un moule rectangulaire ou sur une feuille de papier sulfurisée ( vous couperez au couteau les cubes ou les barres à la taille de votre choix !). 

Faites cuire les barres de céréales 30 à 40 minutes dans un four préchauffé à 140 degrés. 

Sortez les du four et laissez les bien refroidir avant de démouler. 

**ASTUCES** : 

vous pouvez agrémenter ces barres de céréales des gourmandises qui vous font envie : des pépites de chocolat noir, des amandes ou des noix de cajou, des figues séchées ou des pruneaux, ajouter de la cannelle ou une pincée de gingembre, à votre guise! 