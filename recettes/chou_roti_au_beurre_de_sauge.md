---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/chou_roti_au_beurre_de_sauge.jpeg
author: Stéphanie Labadie
description: Chou rôtis au beurre de sauge  
title: Chou rôtis au beurre de sauge 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
---

# Ingrédients: 

1 petit chou vert 

150 g de beurre pommade 

5 anchois émincé 

4 feuilles de sauge ciselée 

## Déroulé: 

Préchauffer le four à 180 degrés. Couper le chou en quatre en gardant les feuilles extérieures. 

Cuire les morceaux à la vapeur pendant 10 minutes. 

Mélanger le beurre pommade avec les anchois et la sauge. 

Tartiner les morceaux de chou et enfourner pour 20 minutes en arrosant régulièrement, jusqu’à ce que les feuilles extérieures commencent à dorer. 

A déguster en entrée avec une cuillère à soupe de crème liquide citronnée et d’un oeuf mollet, ou en en accompagnement d’une viande blanche ou d’un poisson, ou d’une céréale ou d’une légumineuse.  