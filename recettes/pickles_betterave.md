---
author: Stéphanie Labadie
description: Pickles de betterave et de fenouil
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/.jpg
title: Pickles de betterave
licence: CC-by-Sa
categories: accompagnement
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

* 1 ou 2 carottes taillés en lamelles
* 1 branche de céleri coupée en bâtonnets
* 100 g de sucre fin
* 200 g de vinaigre blanc
* 300 g d’eau
* 1 c à c de baies roses
* 1 c à c d de coriandre
* 1 brin d’herbe aromatique type aneth / estragon / coriandre

## Déroulé

Faire chauffer le vinaigre, l’eau et le sucre, ajouter le sel, dès ébullition, laisser refroidir. Déposer les légumes dans un pot stérilisé, avec les épices et les herbes.

Verser le liquide vinaigré dessus. Laisser refroidir et mettre au frigo.

Se déguste quelques heures après, et se conserve pendant des mois au frigidaire.

ASTUCE //// vous pouvez faire cette manipulation avec les légumes et les fruits de votre choix: chou
fleur et pomme, céleri et poire, betterave, chou rouge ou oignon rouge