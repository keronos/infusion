---
author: Stéphanie Labadie
description: Carottes orientales écrasées aux zestes d’orange et aux pistaches, avec leur sauce yaourt à la menthe.
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/carotte-orientale.jpg
title: Carottes orientales au zeste d'oranges
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 20 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

- 15 g de beurre donc
- 1 c à soupe d’huile d’olive
- 800 g de carottes épluchées en rondelles de 2 cm
- 1 cuillère à café de sucre
- 1/2 cuillère à café de cumin
- les suprêmes d’1 orange
- 1/2 cuillère à café de curcuma
- 20 cl cl de bouillon de légumes
- 1 gousse d’ail écrasée
- 1 citron pour le zeste + 1 c à soupe de jus
- 200 g de yaourt à la grecque
- 30 g de pistaches nature décortiquées et grossièrement hachées
- Sel et poivre

## Déroulé

Faites revenir le beurre et l’huile d’olive dans une grande sauteuse et faites y revenir les carottes avec le miel, les épices et la gousse d’ail écrasée pendant 5 minutes en remuant souvent, le temps qu’elle s’attendrissent et colorent légèrement.

Versez le bouillon, mettre sur feu doux et laisser cuire pendant 25 minutes jusqu’ à ce que les carottes soient fondantes et qu’il ne reste presque plus de liquide.

Puis mixez les avec une bonne pincée de sel par brèves impulsions pour obtenir une purée très grossière.

Laissez refroidir et ajouter les suprêmes d’orange coupées en petits cubes, et 1/2 c à café de zeste de citron, et un peu de poivre.

Mélangez le yaourt, la moitié du jus de citron, 1/2 c à café de zeste de citron et 1/4 de c à café de sel.

Étalez le yaourt sur une belles grande assiette, puis déposez la purée de carottes par dessus.

Parsemez de pistaches, et arrosez d’un filet d’huile d’olive.

**Coûts**
