---
author: Stéphanie Labadie
description: Canneloni aux épinards, citron confit, raisins secs et herbes fraîches
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/cannellonis-epinards-citron.jpg
title: Canneloni épinard citrons confits
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 25 min
layout: recette
---

## Ingrédients

- 12 cannellonis
- 250 g de ricotta
- 500 g d’épinards
- 1 botte de basilic
- 2 citrons confits
- 30 g de cranberries
- 1 feuille de laurier, 3 branches de thym
- 2 boîtes de tomates concassées de 400 g
- 2 gousses d’ail
- 2 oignons blancs
- 10 c à soupe de parmesan
- 1 cuillère à soupe de sucre
- Huile d’olive

## Déroulé

Préparation de la sauce tomate:

Faites chauffer une cuillère à soupe d’huile d’olive dans une poêle, y faire revenir un oignon finement ciselé avec la gousse d’ail écrasée. Puis verser le concassé et le concentré de tomates, le sucre, le thym et le laurier, du sel et du poivre, puis faire mijoter une demie heure sur feu doux pour obtenir une sauce un peu réduite ( mais pas trop: elle va servir à humidifier et cuire les cannellonis).

Pour les cannellonis :

Lavez les épinards, ( dans de l’eau additionnée de vinaigre d’alcool pour faciliter l’élimination des impuretés). Rincez, égouttez et réservez.

Emincez finement le deuxième oignon, puis le faire revenir dans une grande poêle avec une cuillère à soupe d’huile d’olive, puis ajouter les épinards égouttés. Laisser fondre les épinards jusqu’à ce que l’eau de cuisson soit évaporée.

Sortir du feu, hâcher le mélange oignons épinards et dans un saladier les mélanger au zeste de citrons confits coupé en très fines lanières, aux cranberries grossièrement hâchées, au basilic émincé, à 3 cuillères à soupe de parmesan, et à la ricotta. Salez et poivrez à votre convenance.

Mettre cette farce dans une poche à douille, découper un petit bout en bas et farcir un à un les cannellonis.

Dans le plat à four, déposer une première couche de sauce tomate, déposer les cannellonis, puis saupoudrez de 3 cuillères de parmesan. Recouvrir les cannellonis d’une fine couche de sauce tomates, puis terminer en saupoudrant de nouveau de 3 dernières cuillères de parmesan, et recouvrir d’une fine couche de chapelure de pain.

Enfournez pendant 25 minutes à 180 degrés au four.

**Petit plus**: pour une version plus hivernale mais moins légère, vous pouvez remplacer la sauce tomate par une béchamel parfumée à la mimolette.

**Coûts**
