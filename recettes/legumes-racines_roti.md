---
categories: entrée
layout: recette
author: Stéphanie Labadie
description: Légumes racines rôtis, beurre au gingembre et feuilles de combawa
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/legumes-racines_roti.jpeg
title: Légumes racines rôtis
licence: CC-By-Sa
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 10 min
temps-preparation: 10 min
---

### Ingrédients (pour 4 personnes)
* 4 petites patates douces
* 2 gros panais coupés en deux
* 4 petites betteraves
* 4 carottes coupées en deux
* 90 g de beurre
* 4 cl d’huile d’olive
* 5 feuilles de combava fraiches et grossièrement hachée ( vous en trouverez dans les épiceries asiatique)
* 10 g de gingembre frais pelé et finement haché
* 1 gousse d’ail écrasée
* 1 c à soupe de jus de citron +2 c à café pour servir
* 1/2 botte de coriandre
* 3 c à soupe d’huile d’olive
* 1/2 gousse d’ail
* 8 cl de crème double ou crème entière liquide
* 9 g de yaourt grec

### Préparation
Lavez et séchez les légumes, versez généreusement un filet d’huile d’olive, malaxez les avec les mains, ajoutez 1 c à café d’épices de voter choix si besoin ( curry, cumin, 4 épices etc..), versez un cm d’eau dans le fond du plat, puis fermez le hermétiquement avec une feuille de papier aluminium, et faites cuire 45 minutes à 180 degrés, jusqu’à ce qu’un couteau pénètre facilement dans les betteraves. Puis faites cuire à nouveau 10 minutes sans papier aluminium pour que les légumes dorent un peu.

Faites des entailles sur les légumes à 4 mm d’intervalle, en vous arrêtant à 1cm de la base.

Pendant que les légumes cuisent, préparez le beurre aux feuilles de combava.

Mettez dans une petite casserole le beurre, l’huile d’olive, les feuilles de combava, le gingembre et puis l’ail, puis faites chauffez le tout à feu moyen vif. Laissez cuire doucement 4 minutes jusqu’à ce que le beurre fonde et commence à bouillonner, puis retirez la casserole du feu et laissez infuser au moins 30 minutes.

Filtrez la préparation et enlevez les aromates. Ajoutez 1 c à soupe de jus de citron et 1 c à café de sel.

Versez le beurre sur les légumes incisés, et laisser rôtir au four pendant 20 minutes, jusqu’à ce que les légumes soient croustillants et caramélisés.

Dans un mortier, écrasez les feuille de coriandre avec l’huile d’olive, avec la demi gousse d’ail.

Salez à votre convenance, et ajoutez un filet de jus de citron si besoin.

Dans un bol fouettez 3 minutes la crême, le yaourt, et une pincée de sel, jusqu’à ce que le mangé soit aéré et mousseux. Réfrigérez jusqu’à utilisation.

Pour servir, versez la crème au yaourt dans un plat, puis disposez les légumes dessus, et répartissant le beurre au dessus et autour, garnissez de pesto à la coriandre, et 2 c à soupe de citron si l’envie vous tente. Et dégustez !