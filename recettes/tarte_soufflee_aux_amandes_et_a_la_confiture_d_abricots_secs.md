---
author: Stéphanie Labadie
description: Tarte soufflée aux amandes et à la confiture d’abricots secs 
title: Tarte soufflée aux amandes et à la confiture d’abricots secs  
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tarte_soufflee_aux_amandes_et_a_la_confiture_d_abricots_secs.jpeg
temps-preparation: 20 min
layout: recette
---

# Pour la pâte: 

* 140 g de beurre
* 2 g de sel
* 75 g de sucre glace
* 25 g de poudre d’amande
* 1 œuf
* 250 g de farine 

.....................................................

Les quantités vous permettront de faire 2 pâtes pour un moule 24 cm de diamètre. Mettez-en une au congélateur, elle se conserve très bien et ce sera autant de temps de gagné pour la prochaine tarte.

Frictionnez la farine, la poudre d’amande et le beurre jusqu’à obtenir une texture sableuse. Rajoutez le sucre glace, puis l’oeuf, et façonnez une belle boule de pâte. Filmez et placez au réfrigérateur pour au moins 2 heures.

Étalez votre pâte sur un plan de travail fariné. 

Foncez un plat à tarte avec votre pâte sucrée, puis replacez au frais pour 15 mn. Ainsi, la pâte ne rétrécira pas à la cuisson.

## Pour la Garniture: 

* 3 blancs d’oeufs 
* 75 g d sucre en poudre 
* 75 g de cerneaux de noix 
* 25 g de poudre d’amandes 
* Le zeste d’un citron 
* 2 poignées d’abricots secs 
* 15 cl de jus de pommes 
* 1 à soupe de sucre glace 

........................................

Faites tremper 30 minutes les abricots secs dans le jus de pommes chaud. Couvrez et occupez vous les blancs en neige. 

Montez les blancs d’oeufs en neige pas trop fermes, ajoutez le sucre en poudre. Continuez à fouetter, puis ajoutez les cerneaux de noix grossièrement gâchés, la poudre d’amandes et le zeste de citron. 

Mixez les abricots secs avec le jus de pomme, mais pas tout: juste la quantité nécessaire pour obtenir une consistance pâteuse. 

Sortez votre moule à tarte. Garnissez le au fond des abricots secs, puis déposer par dessus les blancs d’oeufs montés en neige. 

Posez par dessus des cerneaux de noix entiers réservés, et enfournez 25 à 30 minutes. 

A la sortie du four, laissez refroidir, puis saupoudrez de sucre glace pour la décoration, dégustez !!! 

### Pour le coulis de fruits :

3 bananes 

1 pomme 

2 poignées de fraises 

5 feuilles de menthe 

........................................

Mixez tous les ingrédients, qui peuvent varier au fil des saisons et de vos envies ! 

Déposez une cuillère sur une assiette et votre part de tarte par dessus, bonne dégustation ! 

**Astuces**: 

Vous pouvez varier les gouts en troquant les abricots de la tarte contre des pruneaux, des raisins, ou des figues séchées, ou un mélange! 

Idem pour le coulis, utilisez des fruits de saison ( ou du surgelé). 



