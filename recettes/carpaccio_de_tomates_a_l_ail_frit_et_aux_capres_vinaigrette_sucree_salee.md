---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/carpaccio_de_tomates_a_l_ail_frit_et_aux_capres_vinaigrette_sucree_salee.jpeg
author: Stéphanie Labadie
description: Carpaccio de tomates à l'ail frit et aux câpres, vinaigrette sucreé salée  
title: Carpaccio de tomates à l'ail frit et aux câpres, vinaigrette sucreé salée  
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 16/01/2022
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 15 min
layout: recette
---

# Ingrédients: 

2 grosses tomates charnues 

1 c à soupe de câpres 

5 gousses d’ail 

6 c à soupe d’huile d’olive 

Pelez les tomates, coupez de fines tranches.

Dans une casserole, faites frire l’ail finement émincé dans l’huile d’olive très chaude. 

Réservez l’ail et faites frire dans cette même huile les câpres 2 à 3 minutes, les mettre sur du papier absorbant. Réservez. 

## Pour la vinaigrette: 

3 c à soupe de sirop d’érable 

3 c à soupe de moutarde en grains ( à l’ancienne) 

4 c à soupe d’huile d’olive ( douce)

1 filet de jus de citron

........................................................

Mélangez tous les ingrédients pour obtenir une consistante nappante.

### Dressage: 

Disposez les tranches de tomates dans un plat. 

Versez la vinaigrette par dessus les tomates, puis parsemez les avec l’ail et les câpres frits.

**Astuce**: 

Vous pouvez réaliser cette même recette l'été avec des courgettes revenues dans la poéle et tièdes, idem avec des aubergines, ou l'hiver avec du potimarron ou des patates douces rôtries; ça fera une belle entrée ou un accompagnement original. 

Ou tout simplement avec une belle laitue, ou une salade bien croquante, effet garanti! 