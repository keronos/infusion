---
author: Stéphanie Labadie
description: Soupe betteraves coco 
title: Soupe betteraves coco 
licence: CC-by-sa
categories: entrée 
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson:  40 à 50 min
temps-preparation: 10 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/soupe_betterave_coco.jpeg
layout: recette
---

# Ingrédients: 

* 3 betteraves crues rouges de taille moyenne 
* 1 ou 2 patates douces 
* 40cl de lait de coco 
* 2 càs d’huile d’olive
* 1 oignon  
* 2 càc de cumin moulu 
* 1/2 càc de curcuma moulu
* sel et poivre
* graines de tournesols ou de courges ou herbes fraîches selon votre goût

## Déroulé : 

Emincez l’oignon, faites-le revenir à feu doux dans une casserole avec l’huile d’olive jusqu’à ce qu’il soit tendre. 

Pelez et coupez en morceaux les betteraves et patate douce.

Ajoutez les épices à l’oignon et faites-les griller rapidement, elles diffuseront ainsi mieux leurs saveurs.

Ajoutez les betteraves, la patate douce, le lait de coco, du sel et 60cl d’eau.

Portez à ébullition puis baissez le feu pour laisser mijoter environ 45 minutes jusqu’à ce que les betteraves soient tendres.

Mixez la soupe, servez en ajoutant un peu de crème fraîche, des graines grillées, des herbes fraîches selon votre envie.



