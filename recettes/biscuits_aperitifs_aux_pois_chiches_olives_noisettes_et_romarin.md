---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/biscuits_aperitifs_aux_pois_chiches_olives_noisettes_et_romarin.jpeg
author: Stéphanie Labadie
description: Biscuits apéritifs aux pois chiches, olives, noisettes et romarin
title: Biscuits apéritifs aux pois chiches, olives, noisettes et romarin
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 20 min
temps-preparation: 20 min
layout: recette
---

# Pour une fournée de biscuits : 

280 g de pois chiches cuits

90 g de farine 

4 c.s d'huile de coco 

1 oeuf

50 g de parmesan 

20 g de miel 

1 c à café de romarin 

1 poignée de noisettes concassées 

1/2 c à café de baies roses concassés 

..............................

Mixer tous les ingrédients sauf les noisettes, le romarin et les baies roses, que vous mélangerez en dernier avec une spatule. 

Formez des petites boules de pâte avec une cuillère ou à la main, et enfournez à 180 degrés pendant 15 à 20 minutes. 

**ASTUCE**

Vous pouvez ajouter des olives grossièrement hâchées à cette préparation, ou de la feta, des épices… au gré de vos envies ! 

