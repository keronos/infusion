---
author: Bénédicte Lambert
description: pour relever les tomates d'un peu de piment
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/rougail.jpg
title: Rougail de tomates
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 21/07/2018
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

- 2 tomates
- 1 gousse d'ail
- 1/2 cc de piment d'espelette
- zestes de combava ou combava
- sel

## Déroulé

Couper les tomates en petits dés (si possible en retirant la peau).  
Hacher finement l'ail.  
Mélanger avec les tomates et le piment.  
Zester le combava parcimonieusement et parsemer.  
Saler et conserver au frais.

**Astuces**

Garder le combava au congélateur pour le conserver et sortir au dernier moment pour le zester.
