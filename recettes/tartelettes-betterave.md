---
author: Bénédicte Lambert
description: Un gâteau à base de pâte de datte crue et de gelée de betterave à l'agar-agar
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tartellette-betterave.png
title: Tartelettes betterave et coco
licence: CC-by-sa
categories: dessert 
niveau: débutant
date: 09/11/2018
nombre: 4 personnes
temps-cuisson: 5 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 1 betterave crue
* 200ml de lait de coco
* agar agar
* 2cs de sucre

**Tarte**:

* 30g de graines de sarrasin
* 30g de graines de courge
* 30g de dattes moelleuses
* 4cc d'eau
* 20g de purée d'amandes
* noix de coco râpée

### Déroulé  

1.Mixer la betterave avec le lait de coco et le sucre.  
2. Filtrer le mélange.  
3. Faire cuire 1/4 de ce mélange avec l'agar agar jusqu'à ébullition puis mélanger le tout.  
4. Verser dans des petits moules en silicones.  
5. Faire prendre au frais.  
6. Hacher grossièrement le sarrasin et les courges.  
7. Mélanger avec les dattes hachées, la purée d'amandes et un peu de noix de coco. 
8. Ajouter de l'eau si nécessaire.  
9. Étaler cette pâte finement dans des mini moules ou à plat sur un papier sulfurisé et faire prendre au frais.  
10.  Démouler les domes de coco betterave sur les tartelettes et servir.