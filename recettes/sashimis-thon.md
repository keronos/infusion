---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: sashimis parfumés au thon.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/legumes-feta.jpg
title: Sashimis de thon, crème de citron
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
---

### Ingrédients pour 36 bouchèes

- 600 g de thon frais en tranche épaisse
- 4 citron
- 1 cs d'huile d'olive
- 1 feuille de nori
- 1 pincée de sucre
- 25 cl d'huile d'olive
- 2 feuille de citronnier ou un cumbava

### préparation :

- huile parfumée

  1. faites chauffer l'huile dans une casserole
  2. ajouter les feuilles de citronnier ou le zeste de cumbava
  3. laisser infuser 10 min hors dufeu, puis filtrer

- sauce

  1. Rincez les citrons, coupez les en gros morceaux
  2. faites les blancir 2 min à l'eau bouillante salée
  3. passez-les sous l'eau froide, égouttez-les
  4. Recommencez la même opération encore 2 fois de suite
  5. mettez les citrons blanchis dans un blender, faites fonctionner l'appareil en incorporant l'huile parfume, petit à petit, comme pour une mayonnaise
  6. salez, sucrez et poivrez, réservez

- sashimis
  1. Détaillez le thon en cubes de 3cm de côté
  2. huilez-les au pinceau
  3. faites chauffer une poêle et mettez les dès de thon à feu très vif 2 min
  4. laissez refroidir puis coupez-les en 36 tranches de 5 mm d'épaisseur
  5. Lavez le concombre, détaillez-le en lanière
  6. Emincez la feuille de nori en lanière

Assemblez dans une cuillière
