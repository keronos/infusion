---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/Boulettes_de_maquereau_aux_legumes_rotis.jpeg
author: Stéphanie Labadie
description: Boulettes de mâquereau aux légumes rôtis 
title: Boulettes de mâquereau aux légumes rôtis 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 30 min
layout: recette
---

# Pour les boulettes:

400 g de maquereaux fumé (c’est mieux ) ou en boîte

1 panais râpé 

1 petite boîte de mais grossièrement hâchée 

1/2 bouquet de coriandre

1/2 bouquet de persil plat 

2 gousses d’ail 

1 oignon rouge 

1 c à soupe de paprika fumé

2 c à soupe de sauce nuoc man 

1 oeuf 

## Pour la panure:

100 g farine 

2 oeufs 

150 g de chapelure 

1 c à soupe de semoule fine 

### Déroulé: 

Peler l’ail et l’oignon. Hâcher l’ail et l’oignon, le persil, l’aneth, et la chair de maquereau. 

Mélanger avec le nuoc man, le paprika, le mais et le panais râpé. Ajouter l’oeuf, poivrer et bien mélanger. Filmer et réserver au frais pendant 30 minutes. 

Verser la farine dans une assiette. Dans une autre assiette à soupe, battre les 2 oeufs avec 2 c à soupe d’eau. Et dans une troisième assiette, mélanger la semoule avec la chapelure. 

Façonner des boulettes et les passer tour à tour dans la farine, les oeufs et la chapelure. 

Verser un filet d'huile d'olive sur les boulettes et les enfourner à 180 defrés pendant 20 à 30 minutes. Il faut qu'elles soeint bien dorées et que les panais aient eu le temps de bien cuire à l'intérieur. 

#### Pour la sauce green goddess :

1 avocat mûr 

1 gousse d’ail

15 cl d’eau 

1 filet de jus de citron vert 

6 c à soupe d’huile d’olive 

1 c à café de gingembre ou 2 filet d’anchois, selon les gouts 

2 c à café de vinaigre à sushi

1 grande poignée de basilic 

1 grande poignée de persil plat 

1 c à soupe de feuille d’estragon

facultatif: 1 c à soupe de purée de cajou ou d’amandes

Mettre tous les ingrédient dans un mixeur et mélanger jusqu’à l’obtention d’une sauce lisse et onctueuse. Réserver dans une bouteille fermée au frigo. 



**ASTUCE**

 Cette sauce peut accompagner des salades, des légumes crus ou rôtis, une viande blanche ou du poisson… tout!!! Si vous souhaitez une sauce plus épaisse, il suffit de réduite la quantité d’eau 