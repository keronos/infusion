---
author: Stéphanie Labadie
description: Caviar de carottes cajou
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/caviar_de_carottes_cajou.jpeg
title: Caviar de carottes cajou
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 15/10/2021
nombre: 6 personnes
temps-cuisson: 0 min
temps-preparation: 25 min
layout: recette
---

### Ingrédients

* 4 carottes
* 3 gousse d’ail
* 1/2 c à café de cumin
* 1/2 c à café de cannelle ( ou curry selon vos gouts)
* 2 poignées de noix de cajou laissées à tremper dans l’eau
* 1/2 botte de coriandre
* 1 c à soupe de sucre
* 2 cl d’eau
* 2 pointées de sel
* Graines de tournesol et de courges torréfiées ( grillées au four à 160 degrés pendant 10 minutes)

### Déroulé

Taillez les carottes en rondelles, et les mettre à cuire dans une poêlée avec une bonne noisette
de beurre et d’huile d’olive, les épices, les russe d’ail écrasées, le sucre, le sel et l’eau, laisser les
parfums de mélanger, puis couvrir et laissez cuire pendant 15 minutes.

Unes les carottes cuites et l’eau absorbée, les laisser refroidir. Puis mixez les avec les noix de
cajou, un peu d’eau si besoin, un filet de jus de citron et les herbes fraîches.

Parsemez vos graines de tournesol ou de courges par dessus

A servir avec des légumes crus, des crackers ou du bon pain!