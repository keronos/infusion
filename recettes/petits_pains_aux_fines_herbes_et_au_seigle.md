---
author: Stéphanie Labadie
description: Petits pains aux fines herbes et au seigle
title: Petits pains aux fines herbes et au seigle 
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
image_url: /media.petits_pains_aux_fines_herbes_et_au_seigle.jpeg
layout: recette
---

# Pour 5 petits pains: 

* 15 cl d’eau tiède 
* 1 c à café de sucre de canne
* 1 c à café bombée de levure déshydratée 
* 250 g d farine de blé complet 
* 100 g de farine de seigle complet 
* 3/4 de c à café de sel 
* 2 gousses d’ail hachées 
* 2 c à soupe d’huile d’olive 
* 1 c à café de persil haché 
* 1 c à café de ciboulette hachée 

## Déroulé : 

Dans un pichet, mélangez l’eau, le sucre et la levure, puis laissez reposer pendant 15 minutes. Dans un saladier, mélangez les farines et le sel. Ajoutez le liquide et pétrir 5 minutes. 

Formez une boule,  déposez la dans le saladier, couvrez et laissez lever 1 heure. 

Formez 5 boules, puis roulez les en 5 pâtons fins. 

Mélangez les gousses d’ail écrasées ou hachées avec l’huile et les herbes, en badigeonner les patins, les nouer au centre et rabattre les extrémités en dessous. 

Déposez les sur une plaque couverte de papier cuisson et cuisez les au four à 180 degrés pendant 10 à 15 minutes. 

**Astuce**: 

Vous pouvez varier les gouts en mettant des graines de sésame, courge ou tournesol à la place de l'ail et des herbes, ou ajouter des olives, du gouda râpé ou des anchois ... tout est possible! 