---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/eton_mess_a_l_ananas_roti.jpeg
author: Stéphanie Labadie
description: Eton Mess à l'ananas rôti 
title: Eton Mess à l'ananas rôti 
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 20 mn
temps-preparation: 30 min
layout: recette
---

# Pour les ananas et le coulis :

* 1 petit ananas
* Le jus de 6 clémentines 
* 2 c à soupe de miel 
* 2 c à soupe de sucre de canne 
* 1 gousse de vanille 
* 1 c à café de mélasse de grenade 

.......................................

Préchauffez le four à 180 degrés. 

Coupez l’ananas en quartiers, puis en tranches de 2 cm d’épaisseur. 

Dans un plat à gratin, mélanger le jus des clémentines avec le miel, le sucre, la vanille, et la mélasse de grenade. Laissez rôtir 20 à 30 minutes le temps que l’ananas ait rôti et que le jus de cuisson ait un peu caramélisé. 

Vous pouvez aussi réaliser cette étape à la poele, ça marche aussi ( mettre moins de jus de clémentines)

## Pour la crême parfumée: 

* 20 cl de crême fraîche
* 100 g de mascarpone 
* 100 g de yaourt grec ( idéalement turc, plus compact, se tient mieux) 
* 1 à 2 c à soupe de sucre glace 
* 1 c à soupe d’eau de fleur d’oranger ( ou 1 c à soupe de crême de marron! ) 

.......................................

 A l’aide d’un batteur électrique, monter la crème en chantilly ferme. Pas trop longtemps pour ne pas la faire tourner. 

Ajoutez le mascarpone et le yaourt, battez à nouveau. Puis ajoutez progressivement le sucre glace à la crème chantilly, tout en continuant de battre. 

Avant le dressage, mélanger délicatement à la maryse la crème avec les morceaux meringues grossièrement concassée. 

Mélanger très peu pour bien conserver une consistance aérée. 

### Pour le dressage: 

Pour le dressage, déposer une première couche de crème meringuée, puis une couche d’ananas, une cuillère de jus, puis une nouvelle couche de crême. 

Terminer avec quelques ananas disposés à la surface de cette verrine, et une nouvelle cuillère de jus. 

Décorer avec quelques ananas restants, ou des graines de grenade, ou une feuille de menthe. 

Réserver au réfrigérateur jusqu’au moment de servir, pour qu’ils soient bien frais.