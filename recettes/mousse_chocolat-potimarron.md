---
author: Stéphanie Labadie
description: Mousse au chocolat et au potimarron, noix de coco torréfiée
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/mousse_chocolat-potimarron.jpg
title: Mousse au chocolat et au potimarron
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 19/05/2021
nombre: 4 personnes
temps-cuisson: 10 min
temps-preparation: 30 min
layout: recette
---

## Ingrédients (pour 4 pers) :

* 500 g de purée de potimarron
* 200 g de chocolat
* 20 cl de crème liquide

## Déroulé :
Faites fondre le chocolat coupé en morceaux avec la crème liquide dans une casserole au bain marie.

Incorporez le chocolat fondu et le sucre vanillé à la purée de potiron et bien mélangez

Versez la préparation dans des ramequins.

Mettez au frais pendant 2 heures minimum.

Servez bien frais saupoudré de cacao en poudre et/ou de noix de coco torréfiée.