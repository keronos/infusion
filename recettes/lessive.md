---
categories: produit ménager
layout: recette
author: Romain Foucher
description: De la lessive à base de cendre.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/lessive-a-la-cendre.jpg
title: Lessive à la cendre
licence: Creative commons
niveau: débutant
date: 26/12/2018
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
---
## Recette :

* Tamiser de la cendre
* 1/3 de cendre tamisée et 2/3 d'eau tiède
* Attendre 24h en remuant toutes les 6 h environ
* Filtrer le mélange (filtre à café)

### Dosage :

* (si ajout de vinaigre blanc ou de bicarbonate, pas de surdosage pour eau dure)
* Si eau douce : 40 ml pour linge peu sale à 60 ml pour linge très sale
* Si eau dure (calcaire) : 80 ml pur linge peu sale à 120 ml pour linge très sale

### Soin du linge :

* Couleurs : Ajouter du vinaigre blanc dans le tambour (1 ou 2 CS)
* Blanc : Ajouter du bicarbonate dans le tambour (1 ou 2 CS)
* Adoucisseur : Ajouter du vinaigre blanc dans assouplissant (1 ou 2 CS)
* Raviver le blanc : Ajouter des cristaux de soude dans le tambour (1 CS)

### Tâches tenaces :

* Huile et Graisse : Saupoudrer avec du blanc d'Espagne et frotter à sac avec du savon de Marseille. Puis machine
* Transpiration : Frotter au vinaigre blanc
* Rouge à lèvres : Frotter avec un mélange eau oxygénée + cristaux de soude
* Fruits : (si frais) eau bouillante à travers le tissu (si sec) frotter au vinaigre blanc
* Vin rouge et fruit rouge : savonner sans rincer dans un peu d'eau tiède puis machine
* Herbe : tamponner avec du vinaigre blanc puis machine

