---
author: Bénédicte Lambert
description: artichaut et olives pour une pâte à tartiner
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/tapenade.jpg
title: Tapenade d'olives et artichauts
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 12/05/2017
credit photo: Danielle Macinnes
nombre: 4/8 personnes
temps-cuisson: 10 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

- 1 fond d'artichaut
- 1 sachet d'olives vertes
- 6 tomates séchées
- 4 branches de basilic
- 2cc d'huile d'olive

## Déroulé

Tourner l'artichaut.
Faire cuire à la vapeur ou à l'eau une dizaine de minutes.
Mixer tous les ingrédients.
Rectifier l'assaisonnement.
