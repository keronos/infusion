---
author: Stéphanie Labadie
description: Jolis pamplemousses confits et crème vanille minute
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/pamplemousses-confits.jpg
title: Crême vanille aux pamplemousses confits
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 40 min
temps-preparation: 14 min
layout: recette
---

## Ingrédients

Pour les pamplemousses confits:

- 2 pamplemousses roses et bien fermes
- 150 g de sucre

Pour la crème vanille minute:

- 1 l de lait demi écrémé
- 2 jaunes d’oeufs
- 80g de sucre mucovado
- 1 gousse de vanille ou de la fève tonka
- 50 g de fécule de mais
- La moitié du zeste d’une orange

## Déroulé

Coupez les 2 extrémités des pamplemousses. Taillez les en quartiers puis chaque quartiers en deux tranches. Coupez la partie pointue pour regarder que l’écorce et un peu de chair.

Disposez les morceaux de pamplemousse dans une casserole, couvrez d’eau froide, portez) ébullition, puis égouttez les. Renouvelez cette opération 3 fois en remplissant à chaque fois la casserole d’eau froide.

Disposez les morceaux de pamplemousse dans la casserole, ajoutez 100 g de sucre et 5 cl d’eau. Portez à ébullition et laissez cuire à feu moyen jusqu’à complète évaporation du liquide. Les écorces de pamplemousse sont alors moelleuses et translucides.

Egouttez sur une grille pendant 48 heures à température, et les retournant régulièrement.
Les écorces de pamplemousse confites se conservent quelques jour dans une boîte hermétique.

Dans une casserole, faites bouillir le lait avec le sucre, les grains et la gousse de vanille. Dans un saladier, mélangez les oeufs et la fécule de mais. Ajoutez progressivement dans le saladier le lait en mélangeant énergiquement. Remettez le tout dans la casserole, enlevez la gousse de vanille et rajoutez les zestes d’orange. Laissez bouillir à feu doux pendant 2 minutes jusqu’à épaississement. Laissez tiédir , versez dans des petits bols et laisser refroidir.

Lors du dressage, ajoutez les pistaches concassées sur les petits pots, et disposez à coté quelques quartiers de pamplemousse confits.

**Petit plus**: les quartiers de pamplemousse peuvent tout aussi simplement accompagner une faisselle de brebis ou un bon fromage blanc non sucré.

**Coûts**
