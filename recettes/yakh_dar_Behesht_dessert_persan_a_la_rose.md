---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/yakh_dar_Behesht_dessert_persan_a_la_rose.jpg
author: Stéphanie Labadie
description: Yakh Dar Behesht dessert persan à la rose
title: Yakh Dar Behesht dessert persan à la rose
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 15min
temps-preparation: 10 min
layout: recette
---

# Ingrédients: 

* 1 litre de lait,
* 75 grammes de farine de riz,
* 150 grammes de sucre en poudre,
* 2 cas d'eau de rose (ou un peu moins si vous préférez),
* optionnel: 5 graines de cardamome écrasées légèrement
* pour la décoration: éclats de pistaches, suprêmes d’orange 

## Préparation:

Mettre le lait, la farine et le sucre dans une casserole. 

Mélanger constamment avec une cuillère en bois à feu moyen ou doux jusqu'à ce que le mélange épaississe. Cela prend une dizaine de minutes. 

Il faut veiller à ce que la préparation ne colle pas au fond de la casserole et sur les parois.

Ajouter l'eau de rose et les graines de cardamome et continuer à cuire et mélanger pendant environ 2 minutes supplémentaires. La texture sera celle d'une crème pâtissière.
b
Enlever les graines de cardamome puis verser la préparation dans des petits bols ou ramequins, voir même dans des moules en silicone. 

Laisser refroidir au réfrigérateur quelques heures et au moment de servir, décorer avec des éclats de pistaches. 

Servir avec quelques suprêmes d’orange ou fraises/prunes/pêches/poires etc...  selon la saison. 