---
author: Stéphanie Labadie
description: Salade de chou rouge braisé, orange et fêta panée au sésame 
title: Salade de chou rouge braisé, orange et fêta panée au sésame 
licence: CC-by-sa
categories: entrée 
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 40 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade_de_chou_rouge_braise_orange_et_feta_panee_au_sesame.jpeg
layout: recette
---


# Pour la salade: 

* 1 petit chou rouge 
* 2 oranges 
* 300 g de feta 
* 30 g de farine 
* 1 oeuf battu 
* 1 c à soupe de sésame blond 
* 1 c à soupe de sésame noir 
* 1/2 botte de coriandre 

Emincez finement le chou rouge à la mandoline. 

Garder une partie crue. 

Faire revenir l’autre moitié dans une grande poêle avec de l’huile d’olive. Laissez le chou s’attendrir puis ajoutez 2 c à soupe de soja, et 1 c à soupe de miel. L’idée est que ce chou commence à confire légèrement sans se flétrir. Eteignez le feu sans et réservez. 

Levez les suprêmes d’oranges. 

Coupez la féta en triangles de 5 cm de côté. Les rouler dans la farine, l’ouef battu, puis les graines de sésame. 

Mettre à cuire au four à 180 degrés pendant 15 minutes. Sortez et laissez tiédir. 

## Pour la vinaigrette : 

* 2 c à soupe d’huile d’olive
* 1 c à soupe d’huile de sésame grillé
* 1 c à soupe de soja
* 1 c. soupe de vinaigre basalmique
* 1 filet de jus de citron
* 1 c à café de sirop d’agave 

Mélangez tous les ingrédients et réservez. 

### Pour le dressage : 

Préparez la salade en mélangeant le chou cuit et cru avec les oranges, la féta, et quelques feuilles de coriandre ciselées. Bon appétit! 