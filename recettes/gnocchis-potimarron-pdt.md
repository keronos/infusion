---
author: Bénédicte Lambert
description: Des gnocchis savoureux au potimarron
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/gnocchis-pdt-potimarron.jpg
title: Gnocchis de pommes de terre et potimarron
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 10 min
temps-preparation: 30 min
layout: recette
---


## Ingrédients  
 
* 350g de pommes de terre
* 300g de potimarron
* 125g de farine de riz complet
* 100g de fécule de maïs
* 2CS d'huile d'olive 
* sel



## Déroulé

1. Éplucher les pommes de terre.  
2. Couper les légumes en cubes.  
3. Faire cuire à l'eau ou à la vapeur jusqu'à ce qu'ils soient tendres.  
4. Passer au presse-purée puis dessécher à la casserole si le mélange est humide.    
5. Laisser refroidir un peu.  
6. Mélanger les farines et le sel.   
7. Ajouter la purée dans le mix de farines.  
8. Bien mélanger, la pâte ne doit pas être collante, ajouter de la farine si nécessaire.  
9. Faire de petits boudins, les découper en rondelles de 2mm puis les écraser légèrement avec une fourchette.
10. Faire chauffer l'huile dans une poêle.  
11. Faire revenir 5 min sur chaque face.  
12. Servir aussitôt.  
 
**Astuces** 

Pour une version sans huile, les faire cuire à l'eau bouillante. Les gnocchis sont cuits lorsqu'ils remontent à la surface.