---
author: Bénédicte Lambert
description: une bonne idée pour faire manger des légumes aux enfants...et aux parents
image_url: https://upload.wikimedia.org/wikipedia/commons/8/8f/Burmese_style_samusa.jpg
title: Samoussas de légumes
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 12/05/2017
nombre: 4 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

- légumes : tomates cerises, fenouil, betteraves
- épices : coriandre, cumin, cannelle, paprika, gingembre
- 2cc de miel
- 8 feuilles de brick
- 4cc d'huile d'olive
- sel

## Déroulé

1. Préparer les légumes, les découper, les faire précuire pour les légumes durs (betteraves, fenouil, carottes).
2. Ajouter les épices et le miel.
3. Couper les feuilles de brick en 2. Replier en 2.
4. Mettre une cuiller de légumes et refermer en formant un triangle qui se replit sur lui.
5. Badigeonner d'huile.
6. Enfourner pour 15/20 min à 180°C.
