---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/houmous_a_l_ail_confit_et_aux_champignons_grilles.jpeg
author: Stéphanie Labadie
description: Houmous à l'ail confit et aux champignons grillés  
title: Houmous à l'ail confit et aux champignons grillés  
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 4 à 6 personnes
temps-cuisson: 40 min
temps-preparation: 20 min
layout: recette
---

# Ingrédients: 

* 2 têtes d’ail 
* 2 c à soupe d’huile d’olive 
* 300 g de pois chiches cuits 
* 2 c à soupe de jus de citron 
* 1 c à soupe de purée d’amandes 
* 3 c à soupe d’eau glacée 

....................

* 200 g de champignons ( shitakés, champignons de Paris, pleurotes …) 
* 2 c à soupe de sirop d’érable
* 1 c à soupe de tamarin 
* Jus de citron

## Déroulé: 

Préchauffez le four à 200 degrés. 

Arrosez les têtes d’ail avec 1 c à café d’huile, puis saupoudrez les d’un peu de sel. Enveloppez les dans du papier aluminium, et enfournez pour 40 minutes jusqu’à ce que les gousses d’ail soient fondantes et bien colorées. 

Retirez le papier d’aluminium et quand les gousses d’ail ont refroidi, pressez les pour récupérer les gousses. 

Pendant la cuisson de l'ail, faites rissoler vos champignon dans une poele, avec une grosse noisette de beurre et de l’huile d’olive. 

Salez, puis une fois cuits, les mettre dans un saladier. Mélanger avec le soja, le sirop d’érable et un zeste de jus de citron. Réservez. 

Retour au houmous: Misez tous les ingrédients du houmous dans un robot avec l’ail cuit, et 1 c à café de sel. Rectifiez l’assaisonnement si besoin, et ajoutez de l'eau glacée selon la texture souhaitée. 

### Pour le dressage : 

Etalez le houmous dans un bol peu profond, puis forme un grand puit au center avec le dos d’une cuillère. 

Ajoutez l’aneth sur les champignons puis mélangez, et disposez cette préparation dans le puit et autour du plat. 

Avec quelques amandes ou pignon de pins torréfiés pour le gout et la déco, bon appétit!!! 

**Astuce**: 

Vous pouvez réaliser ce houmous sans forcément confire les têtes d'ail, et les remplacer simplement par une gusse d'ail fraîche, mixée avec la préparation. 