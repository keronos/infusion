---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/lemon_curd_a_la_noix_de_coco_et_son_crumble_vegan.jpeg
author: Stéphanie Labadie
description: Lemon curd à la noix de coco et son crumble vegan
title: Lemon curd à la noix de coco et son crumble vegan 
licence: CC-by-sa
categories:  dessert
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
---

# Version vegan du lemon curd:

- 60 g d’eau
- 110 g de jus de citron
- 1 jaune d’oeuf
- 70 g de sirop d’agave
- 10 g de maizena
- 40 g d’huile de coco
- 50 de noix de coco râpée 

...............................................

Verser dans une casserole l’eau, le jus de citron, le jaune, la maïzena et le sirop d’agave. 

Remuer à froid puis faire chauffer sur feu doux environ 5 minutes jusqu’à ce que l’ensemble épaississe légèrement.

Ajouter ensuite l’huile de coco, puis la noix de coco râpée, une fois l’huile bien intégrée.

Transférer dans un récipient puis réserver au frais.

## Version vegan du crumble : 

- 1 c à soupe de pureé d’amandes ou de noisettes
- 3 c à soupe d’huile neutre ou parfumée ( coco, noisette, olive)
- 3 c à soupe de sucre de canne complet
- 175 g de farine de petit épeautre complet
- 4 c à soupe de lait d’amande

............................................

Dans un saladier, mélangez la purée d’amandes, l’huile et le sucre. 

Ajoutez la farine et mélangez. Ajoutez le lait et effriter pour bien mélanger tous les ingrédients. 

Disposez votre pâte effritée sur une plaque et enfournez 15 minutes au four à 160 degrés. 

### Dressage: 

Disposez des petits cuillères de lemon curd dans des coupelles puis par dessus du crumble. Saupoudrez de noix de coco râpée, de banane ou de mangue et de quelques zestes de citron restants.

**Petit plus**: 

Vous pouvez garder le lemon curd en pot et le servir avec des yaourts ou sur du bon pain grillé, voir des tranches de brioche. 

Agrémenté de quelques fruits frais émincés: fraises, pêches, poires, pommes etc… 