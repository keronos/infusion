---
author: Stéphanie Labadie
description: Soupe miso minute aux champignons, boulettes de pain au mais et fromage 
title: Soupe miso minute aux champignons, boulettes de pain au mais et fromage 
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 04/01/2022
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/soupe_miso_aux_champignons_boulettes_de_pain_sec_au_mais_et_au_fromage.jpeg
layout: recette
---


# Pour le bouillon miso : 

* 3 c à soupe de pate miso 
* 1 l d’eau 
* 1 botte de cébette
* 250 de champions de Paris ( coupés en 4 ) ou shitakés ( grossièrement émincés) 

.....................................

Faire fondre le miso dans de l’eau bouillante, et mélanger avec un fouet, laissez à petit frémissement pendant 5 minutes. 

Pendant ce temps, faites rissoler les champignons dans une grosse noisette de beurre et de l’huile d’olive. 

Laissez cuire, et ajoutez si besoin une pointe de jus de citron et/ou 2 c à soupe de soja, et un reste d’herbes fraîches émincées si vous en avez ( coriandre, persil ) .

## Pour les boulettes de pain: 

* 200 g à 250 g de pain rassis
* 50 cl de lait 
* 30 g beurre fondu
* 1 œufs battu
* 70 de mais grossièrement gâché
* 50 de mimolette râpée
* 1 oignon
* farine si besoin 
* Sel & poivre
* Un peu de muscade

.........................

Couper le pain sec en dés et le mettre dans un saladier.

Tremper le pain dans le lait, pendant 10 à 15 minutes, le temps que le pain ait absorbé le lait.

Pendant ce temps, émincer l’oignon finement.

Égoutter le pain puis le mélanger avec l’oignon, les œufs, le mais, la mimolette, le beurre, le poivre, le sel et la muscade. On obtient une pâte homogène et ferme qui ne doit pas être liquide (si c’est le cas, rajouter de la farine, de la semoule ou mieux de la chapelure)

Avec le creux des mains, former des boulettes.

Porter à ébullition un grand volume d’eau additionnée de bouillon. Y pocher les boulettes et les faire cuire 10 minutes à frémissement.

### Dressage : 

Pour le dressage, versez le bouillon de miso dans des bols, ajoutez les boulettes et les champignons et décorez de ciboule finement ciselée. 

Bon appétit ! 