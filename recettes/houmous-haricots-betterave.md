---
author: Bénédicte Lambert
description: Une recette simple à réaliser et qui permet de surprendre vos invités à l'apéro grâce à la couleur et au goût apporté par l'utilisation de la betterave.
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/houmous.jpg
title: Houmous
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 12/05/2017
nombre: 4 personnes
temps-cuisson: 60 min
temps-preparation: 10 min
temps-repos: 12h
layout: recette
---

## Ingrédients

* 250g de haricots blancs cuits
* 1 gousse d’ail
* 1 jus de citron
* 1 pincée de piment 
* 1 petite betterave
* Un peu d'eau

## Déroulé

Commencer par mixer la betterave.
Mixez tous les ingrédients et c'est prêt.   
