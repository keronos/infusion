---
author: Stéphanie Labadie
description: Tempura de radis noir, sauce betterave cajou coriandre   
title: Tempura de radis noir, sauce betterave cajou coriandre 
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tempura_de_radis_noir_sauce_betterave_cajou_coriandre.jpeg
layout: recette
---

## Pour la tempura:

* 3 radis noirs 
* 80 g de farine
* 30g de fécule de maïs
* 5g de levure chimique
* 130 g d’eau gazeuse
* 150 g d’huile de tournesol

Mélangez la farine, la fécule et la levure chimique, ajoutez l’eau gazeuse petit à petit en mélangeant au fouet. Assaisonnez et ajoutez des épices si besoin. Laissez l’appareil bien froid.

Coupez vos légumes en bâtonnets de 2 cm de large maximum. 

Chauffez l’huile à 180°C, tremper le radis dans l’appareil à tempura et frire deux minutes. Saupoudrer de fleur de sel et servir immédiatement.

## Pour la sauce cajou betterave :

* 100 g de noix de cajou 
* 1 c à café de moutarde ancienne 
* 200 g de betteraves cuites 
* Jus de citron vert 
* 1 c à café de miso blanc ( facultatif)
* Sel, poivre
* 1/3 botte de coriandre ou d’aneth ciselée 

Mixez tous les ingrédienst ensemble, ajoutez un peu d'eau tiède si bseoin ( si envie d'une consistance plus liquide, quelques cl pas plus ) et ajoutez les herbes ciselées à la fin. Réservez au frais jusqu’au moment de servir. 

**Astuce**: 

Vous pouvez utuiliser d'autres légumes si vous le souhaitez : carottes, patate douce, betterave ( mais coupés en bâtonnets un peu plus fins car plus long à cuire), courgettes, navets... Et varier les sauces : mayo aux anchoix et aux câpres, yaourt aux herbes etc... 