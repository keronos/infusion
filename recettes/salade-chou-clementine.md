---
author: Stéphanie Labadie
description: Salade gourmande de chou rouge, clémentines, fêta, noisettes et légumes rôtis.
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade-chou-orange.jpg
title: Salade gourmande chou rouge clémentines
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

## Ingrédients

- 1 petit chou rouge
- 2 gousse d’ail ciselées très finement
- 20 de beurre
- 4 clémentines
- 2 carottes multicolores
- 2 navets
- 1/2 c à café de graines de fenouil
- 40 de noisettes torréfiées et grossièrement hachées
- 50 g de féta
- quelques brins d’aneth

Pour la vinaigrette

- 3 c à soupe d’huile d’olive
- 1 c à soupe de soja
- 1 c à café de jus de citron
- 1 c à café de sucre complet

## Déroulé

Coupez les carottes en rondelles et en biseau, les mélanger avec 2 cuillères à soupe de sirop
d’érable, la moitié du jus d’une orange, et un généreux filet d’huile d’olive.

Ajoutez les graines de fenouil et enfournez pendant 25 minutes à 180 degrés.

Pendant ce temps faites fondre le beurre dans une grande sauteuse et quand il commence à
mousser, mettre le chou finement ciselé à cuire. Faites rissoler pendant 5 minutes et ajoutez
l’ail ciselée. Mélangez et couvrir à feu doux pendant 15 minutes.

Effritez la féta, ciselez l’aneth, préparez les noisettes torréfiées, la vinaigrette, et mélanger
tous les ingrédients une fois cuits et refroidis.

Petits plus: s’il reste de cette salade, elle peut se recycler en petits flans, en tarte ou en
tartinade pour l’apéritif.

**Coûts**
