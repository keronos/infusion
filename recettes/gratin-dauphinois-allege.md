---
author: Bénédicte Lambert
description: Gratin dauphinois allégé
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/gratin-dauphinois-allégé.jpg
title: Gratin dauphinois allégé
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 60 min
temps-preparation: 20 min
layout: recette
---


## Ingrédients  
 
* 2 pommes de terre
* 2 panais
* 1 gousse d'ail
* 1CS d'huile d'olive
* 400 ml de lait 1/2 écrémé
* 50g de fromage râpé
* sel, poivre



## Déroulé     

Préchauffer le four à 180°C.  
Éplucher les pommes de terre.  
Couper les légumes en fines rondelles.  
Graisser le moule avec l'huile puis frotter la gousse d'ail. 
Hacher le reste d'ail très finement. 
Mettre les pommes de terre et panais en couche, un peu d'ail saler, poivrer.    
Renouveler l'opération jusqu'à épuisement des ingrédients.  
Verser le lait et recouvrir de fromage.  
Enfourner pour 1h.     
 
**Astuces** 

Ajouter des oignons, des herbes aromatiques dans les couches.  
