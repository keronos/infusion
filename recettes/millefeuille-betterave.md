---
author: Bénédicte Lambert
description: mélange de saveur avec cette préparation à base de chutney de bettrave et de fromage de chèvre.
image_url: media/mille_feuille-betterave.png
title: Millefeuille de betterave
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 09/11/2018
nombre: 4 personnes
temps-cuisson: 60 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 1 ou 2 betteraves
* 1 chèvre frais
* 200g de chutney de betteraves
* huile d'olive
* sel, poivre

### Déroulé  

1. Faire cuire les betteraves au choix à la croute de sel au four, à la vapeur ou à bouillir dans l'eau
2. Couper des tranches.  
3. Poser une tranche, tartiner de chèvre, verser un peu de chutney puis recouvrir d'une tranche de betterave.  
4. Renouveler l'opération.  

### Astuces

Décorer de feuilles de cerfeuil.