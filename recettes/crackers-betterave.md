---
author: Bénédicte Lambert
description: crackers à base de graines et de betterave accompagnés d'un pesto également à la betterave
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/cracker_pesto-betterave.png
title: Crackers de betterave
licence: CC-by-sa
categories: accompagnement 
niveau: débutant
date: 09/11/2018
nombre: 4 personnes
temps-cuisson: 45 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 100g de graines de lin, tournesol et courge
* 100g betteraves crues finement râpées
* 1/2 oignon finement râpé
* 1 càs de sauce soja 
* épices au choix
* sel

### Déroulé  

1. Faire tremper les graines 2h dans de l'eau.  
2. Rincer puis mixer.
3. Râper finement la betterave et l'oignon.  
4. Mélanger tous les ingrédients et ajouter sel et épices.  
5. Étaler finement sur une plaque et pré-découper des carrés.  
6. Enfourner pour 10-15 min à 180°C.  

### Astuces

Faire déshydrater en cuisant à 42°C pendant 6-7h.