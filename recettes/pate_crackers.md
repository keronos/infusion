---
author: Stéphanie Labadie
description: Pâte à crackers
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/pate_crakers.jpg
title: Pâte à crackers
licence: CC-by-Sa
categories: entrée
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

* 100g de farine semi complète
* 50 g de farine de riz
* 50 de farine blanche
* 50ml d'huile d'olive
* 1 pincée de sel
* eau pour ramassez la pâte
* 1 cuillère à café de baies roses écrasées au mortier
* 1 cuillère à soupe de graines de sésame
* 1 blanc d’oeuf
* 1 cas de sucre en poudre.

## Déroulé

Préchauffer le four à 180 degrés.

Amalgamer les farines, le sel, le sucre, et l’huile d’olive pour former une boule de pâte. Réserver 1
heure au frigo.

Etaler au rouleau à pâtisserie, et couper des triangles de pâtes, les badigeonner de blanc d’oeuf, et
saupoudrer de graines de sésame et de baies roses.

Enfourner pour 20 minutes. Servir froid avec la tartinade et les pickles.

Servir la tartinade sur des crackers avec les pickles, un régal !!!