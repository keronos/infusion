---
author: Stéphanie Labadie
description: chaussons de pâte brisée fourrée aux restes du frigo
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/empanadas.jpg
title: Empanadas recycladas
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 40 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

**Pour la pâte:**

- 150 g de farine semi complète
- 100 g de farine de lentilles
- 150 g de beurre en dés pommade
- 50 g de parmesan
- 1 c à café de curry ou de romarin au choix
- 2 jaune d’oeuf
- 1/2 c à café de sel
- 4 cl d’eau tiède
- 5 cl de lait

**Pour les restes:**

- Une escalope de poulet
- 3 tranches de jambon
- 100 g de fromage râpé
- 50 g d’olives
- 150 de petits pois ( écrasés en purée)
- 2 poireaux
- 3 carottes
- 1 fromage de chèvre frais
- Carottes râpées revenues à la poêle

### Déroulé

1. Mélanger du bout des doigts les farines et le beurre pour obtenir un mélange sableux.
2. Mélanger avec le parmesan, puis le curry ou le romarin. Ajouter un jaune d’oeuf et l’eau, puis mélanger afin d’obtenir une pâte homogène.
3. Sur une surface farinée, étaler des ronds de pâtes avec le rouleaux à pâtisserie et un cercle.
4. Laisser reposer la pâte.
5. Pendant ce temps, faire revenir les poireaux émincés dans une noisette de beurre.
6. Assaisonner.
7. Râper les carottes et les faire rissoler dans une poêle, rajouter une pincée de cumin et de sel.
8. Couper le jambon et le fromage de chèvre en cubes. Réserver.

Sur une surface farinée, étaler des ronds de pâtes avec le rouleaux à pâtisserie et un cercle. Y
disposer sur la moitié les ingrédients souhaités, puis rabaisser l’autre bord par dessus afin
d’obtenir de petits chaussons.

Battre le jaune d’oeuf restant avec le lait, et badigeonner chaque empanada avec un pinceau. Enfourner à 180 degrés pendant 25 minutes.

Servir avec une salade aux herbes fraîches et un coleslaw au yaourt, miel et raisins, ou une soupe pour un repas complet.
