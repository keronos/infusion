---
author: Stéphanie Labadie
description: version végan du lemon curd
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/gateau-carottes.jpg
title: Lemon curd à la noix de coco et son crumble vegan
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 12/10/2021
nombre: 6 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

- 60 g d’eau
- 110 g de jus de citron
- 1 jaune d’oeuf
- 70 g de sirop d’agave
- 10 g de maizena
- 40 g d’huile de coco
- 50 de noix de coco râpée

### Pâte à tarte végan

- 1 c à soupe de pureé d’amandes ou de noisettes
- 3 c à soupe d’huile neutre ou parfumée ( coco, noisette, olive)
- 3 c à soupe de sucre de canne complet
- 175 g de farine de petit épeautre complet
- 4 c à soupe de lait d’amande


## Déroulé

Verser dans une casserole, l’eau, le jus de citron, le jaune, la maïzena et le sirop d’agave.

Remuer à froid puis faire chauffer sur feu doux environ 5 minutes jusqu’à ce que l’ensemble
épaississe légèrement.

Ajouter ensuite l’huile de coco, puis la noix de coco râpée, une fois l’huile bien intégrée.

Transférer dans un récipient puis réserver au frais.

Dans un saladier, mélangez la purée d’amandes, l’huile et le sucre. Ajoutez la farine et mélangez.

Ajoutez le lait et effriter pour bien mélanger tous les ingrédients. Disposez votre pâte à comble sur
une plaque et enfournez 15 minutes au four à 160 degrés.

Dressage: disposez des petits cuillères de lemon curd dans des coupelles puis par dessus du crumble.

Saupoudrez de noix de coco râpée, et de quelques zestes de citron restants ou de pistache concassée.
Vous pouvez vous amuser à mettre de la mangue ou de la banane coupe en petits cubes.

Petit plus: Vous pouvez garder le lemon curd en pot et le servir avec des yaourts ou sur du bon pain
grillé, voir des tranches de brioche. Agrémenté de quelques fruits frais émincés: fraises, pêches,
poires, pommes etc…