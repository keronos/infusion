---
author: Stéphanie Labadie
description: Chakchouka hivernale (et verte)
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/chakchouka2.jpg
title: Chakchouka hivernale (et verte)
licence: CC-by-sa
categories: plat
niveau: débutant
date: 30/11/2021
nombre: 4 à 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
layout: recette
---

### Ingrédients: 

* 4 oeufs 
* 2 poireaux 
* 4 tiges de blettes ou trois bonnes poignées d’épinards frais 
* 1 oignon
* 1 gousse d’ail 
* 3 c à soupe d’huile d’olive 
* 1 c à café d’épices de votre choix 

facultatif : 1 c à soupe ou de mélasse de grenade, ou de sirop d'érable. 

**Pâte d’herbes :** 

* 1/2 botte de persil plat 
* 1/2 botte de coriandre 
* 4 abricots secs 
* 1 gousse d’ail émincée 
* 2 c à soupe d’huile d’olive 
* 1 c à café de sucre 
* 1 c à café d'épice ( facultatif) type massalé, ou paprika fumé
* 1 c à café de sel 

**Mélange boulgour yaourt :** 

* 4 c à soupe de yaourt grec 
* 1 c à soupe de boulgour fin 
* Le jus d’un citron 
* Huile d’olive 
* sel, poivre 

**Service :** 

* Feuille de menthe 
* Feta 
* Huile d’olive 

## Déroulé : 

**Pour les légumes** : 

Epluchez et émincez l'oignon, réservez. 

Lavez les poireaux, enlever le vert ( qui vous servira pour un bouillon de légumes, une mayonnaise ou une huile parfumée), puis coupez des tranches d'1 cm d'épaisseur maximum. Réservez.

Lavez les blettes, et émincez les tiges en morceaux d'1 cm d'épaisseur, et les feuilles de 3 cm (pour garder un peu de mâche). Réservez. 

Dans une grande poele à bords hauts, chauffer l’huile d’olive, et ajouter l’oignon, laissez dorer 5 minutes puis ajoutez les poireaux et les blettes émincés, l'ail, l’épice et le sel. Cuire 5 à 10 minutes. Ajoutez la mélasse de grenade, ou le sirop d'étable, et laissez mijoter à feu doux 10 minutes encore. Rectifiez l'assaisonnement si besoin avec du sel / poivre, ou un trait de jus de citron. 

**Pour le boulgour aux herbes:**

Pendant ce temps, mélangez le boulgour avec le yaourt, le jus de citron, un filet d’huile d’olive, et le sel dans un bol. Salez et poivrez. Réservez pendant 15 minutes, le temps que le boulgour soit bien humidifié. 

Mixez le persil, la coriandre, les abricots, l’ail, 2 c à soupe d’huile d’olive et du sel. 

Mélangez la pâte d’herbes et la préparation au yaourt- boulgour. 

Ajoutez en couche ce mélange sur les légumes dans la pole. 

Baissez le feu et faire 4 petits puits à l’aide d’une cuillère. Cassez un oeuf dans chaque puit et couvrir. Laissez cuire 6 à 8 minutes le temps que les oeufs soient cuits. 

## Pour le dressage: ##

Au moment de servir, parsemez de menthe fraîche, de feta émiettée, de cranberries, d’huile d’olive et de poivre du moulin. 

A déguster avec une baguette fraîche ou du pain libanais grillé au four, ou tel quel, avec des pommes de terre rôties c'est bien aussi! 