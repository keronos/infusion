---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: asseemblage de légumes d'été et d'oeufs simple à réaliser.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tortilla-tapenade.jpg
title: gâteau d'omelettes aux légumes
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
---

### Ingrédients

- 6 oignons nouveaux émincés
- 3 cs d'huile d'olive
- 12 gros oeufs
- 15 cl de lait
- 2 poivrons rouges grillés, pelés et hachés
- 4 gousses d'ail nouveau
- 3 petites courgettes rapées
- 1 brin de romarin
- sel, poivre

### Préparation

1. faites fondre les oignons à la poêle pendant 3, 4 min dans une cs d'huile d'olive
2. Salez, poivrez
3. Fouttez 4 oeufs avec 3 cs de lait, sel et poivre
4. versez sur les oignons
5. Dès que l'omelette est prise, faites la glisser dans un moule à manqué huilé à sa taille
6. Faites revenir les poivrons pendant 3 min dans une cs d'huile d'olive, avrc 2 gousses d'ail émincées
7. versez dessus 4 oeufs fouettés avec 3 cs de lait, salez, poivrez. Faites glisser cette omelette sur la première
8. Faites cuire les courgettes pendant 2 min dans 1 cs d'huile d'olive avec un peu de romarin et de gousses d'ail émincées. Salez, poivrez
9. versez dessus les oeufs restants, battus, mêlés au lait et assaisonnés
10. Faites glisser cette omelette sur les autres
11. Enfournez le moule dans un four à th. 6 (180°c) pour 15 min
12. Démoulez et servez froid
