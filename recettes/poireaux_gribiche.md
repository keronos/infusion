---
author: Stéphanie Labadie
description: Poireaux sauce gribiche légère
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/.jpg
title: Poireaux sauce gribiche légère
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/10/2021
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
temps-repos: 30 min
layout: recette
---


## Ingrédients  
 
### Pour 4 personnes

* 2 poireaux
* 1 cuillère à soupe de câpres hachées
* 4 cornichons hachés
* 15 cl d’huile de tournesol
* 1 c à café de moutarde
* 1 oeuf
* 1 oeuf dur
* 1 poignée de persil/aneth/ estragon/coriandre hachée
* 1 cuillère à soupe de vinaigre de cidre,
* sel et poivre.

## Déroulé  
 
### Gnocci

Coupez les poireaux en rondelles de 2 cm et le faire cuire à l abatteur pendant 15 minutes, veillez à
ce qu’il ne cuise pas trop, et garde de sa texture.

Mélangez votre jaune d’oeuf avec la moutarde, faites prendre votre mayonnaise avec un mince filet
d’huile, puis terminez avec le vinaigre ( ou du jus de citron), et du sel.

Ajoutez les câpres, les cornichons et les herbes hâchées.

Coupez votre pomme en allumettes, c’est à dire en bâtonnets très fins.

Hâchez votre oeuf dur.

Avant de servir, mélangez le poireaux avec la sauve gribiche, puis avec les pommes.

Disposez l’oeuf émietté par dessus, avec quelques herbes friches ciselées.

Bon appétit !