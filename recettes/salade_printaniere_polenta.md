---
author: Stéphanie Labadie
description: Salade printanière aux boulettes de polenta grillée, sauce au yaourt
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/salade_printaniere_polenta.jpg
title: Salade printanière aux boulettes de polenta grillée
licence: CC-by-sa
categories: plat
niveau: débutant
date: 12/10/21
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 5 min
layout: recette
---

## Ingrédients

### Pour les quenelles

* 25 cl de lait
* 100 g de beurre demi-sel
* 100 g de farine
* 50 g de semoule de blé fine
* 3 oeufs
* 100 g de polenta extra fine
* 10 g de fleur de sel
* Le zeste très fin d’un citron non traité
* 40 g de parmesan

### Pour la salade

* 400 g de feuilles d’épinard ( ou de l batavia ou feuille de chêne )
* 1 pomme taillée an allumettes
* 1 orange
* 1 carotte
* 4 abricots secs coupés en lamelles

### Pour la sauce yaourt

* 1 yaourt
* 2 c à soupe de sauce soja
* 1/2 citron confit
* 1/2 bouquet de coriandre fraîche

## Déroulé

Dans une casserole, faites chauffer le lait avec le beurre coupé en dés. Salez et poivrez.

Remuez de temps en temps, et une fois l’ébullition atteinte, retirez du feu et ajoutez la farine
d’un coup. 

Mélangez rapidement avec une cuillère en bois, puis mettre la casserole sur le feu
sans cesser de remuer pour dessécher la pâte. Quand la pâte se détache de la casserole,
ajoutez la polenta et continuez de remuer jusqu’à l’obtention d’une boule. 

Hors du feu, ajoutez les oeufs un à un en ménageant entre chaque oeuf. Terminez avec le fromage et les
zestes de citron. 

Quand la pâte est homogène, la mettre dans un récipient et couvrir de film.
Laissez au réfrigérateur 2 heures minimum.

Façonnez les quenelles à l’aide d’une cuillère à soupe.

Dans une casserole d’eau bouillante et salée avec un cube de bouillon, plongez les quenelles
de 5 à 6 minutes si elles sont petites, 10 à 12 minutes si elles sont plus grosses. Quand elles
sont cuites, elles remontent à la surface, les égoutter avec une écumoire et les poser sur du
papier absorbant.

Coupez les en tronçons de 2 cm d’épaisseur. Les faire dorer dans une poele avec de l’huile
d’olive, réservez au chaud.

Astuce //// préparer la pâte à quenelles la veille et la laisser une nuit au frigo pour qu’elle soit
plus ferme.

Mélangez le yaourt avec la sauce soja, et le citron confit coupé en tout petit dés, et la
coriandre ciselée. Salez et poivrez.

Pour la salade, levez les suprêmes d’orange, coupez les en deux. Coupez les carottes et la
pomme en fines allumettes, et ciselez finement l’abricot sec.

Ajoutez les quenelles encore tièdes, et versez un peu de sauce au yaourt par dessus, avec
quelques feuilles de coriandre s’il vous en reste encore.