---
author: Stéphanie Labadie
description: Glace banane choco amandes minute et madeleines aux zestes d’orange
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/.jpg
title: Glace banane choco amandes
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 40 min
temps-preparation: 14 min
layout: recette
---

## Ingrédients

### Madeleine aux zestes d’orange
* 5 oeufs
* 100 g de sucre en poudre
* 1 zeste râpé de citron ou d’orange
* 1/2 c. à café de vanille
* 150 g de farine
* 2 c. à soupe de levure
* 50 g de beurre fondu, tiède
* sucre glace

### Pour la glace bananes minute :

* 300 g de bananes bien mûres
* 85 g de purée de noisettes
* 3 c à soupe de cacao en poudre


## Déroulé

Préchauffez le four th.7 (200°C).

Dans un bol, battez le sucre et les oeufs jusqu’à ce que la préparation soit jaune pâle.

Ajoutez le zeste et la vanille. Tamisez la farine avec la levure.

Incorporez délicatement à la préparation précédente à l’aide d’une spatule.

Y mélanger le beurre fondu de la même façon.

Laissez reposer la pâte 10 min avant la cuisson.Versez la pâte dans des moules à madeleines enduits
de beurre fondu. (n’emplir les moules qu’aux trois quarts)

Enfournez pendant 10 min.

Démoulez et laissez refroidir sur une grille.

Saupoudrez de sucre glace.

### Glace minute

La veille, couper la banane en rondelle et les mettre à congeler.

Le jour même, placer les morceaux de bananes dans le bol du mixeur, et mélanger avec la purée de
noisettes et le cacao en plusieurs fois jusqu’à obtenir une crême homogène. Déguster sans tarder.

On peut accompagner ce dessert de chocolat fondu, d’une crême chantilly, de copeaux de chocolat
blanc, servis sur des petites gaufres à déguster pendant le gouter.

### ASTUCES///
Avec les suprêmes d’oranges, vous pouvez faire un carpaccio de rondelles, avec une pointe de fleur
d’oranger et quelques feuilles de menthe ciselées, voir une pointe d’huile d’olive
