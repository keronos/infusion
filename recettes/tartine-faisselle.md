---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: des tartines aux herbes.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tartine-faisselle.png
title: Tartines de faisselles aux herbes
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 0 min
temps-preparation: 15 min
temps-repos: 60 min
---

### Ingrédients

- faisselle
- 25cl de crème liquide
- 1/2 botte de ciboulette
- 1/2 botte de cerfeuil
- 1 citron
- 10cl d'huile d'olive

### Préparation

1. Sortez les faisselles de pots et laissez les s'égouter
2. Réservez les pots pour la suite de la recette
3. Lavez et essuyez le citron, rapez finement le zeste, pressez la moitié du jus
4. Démoulez les fromages dans un saladier, réservez les faisselles
5. fouettez le fromage pour le lisser, puis incorporer les herbes ciselées, la moitié de l'huile, le zeste, le jus de citron, du sel et du poivre
6. Fouettez la crême très froide en chantilly et incorporez-la à la préparation
7. Rectifier l'assaisonnement en sel, poivre et jus de citron
8. remplissez les faisselles du mélange, gardez les pour accompagner les crudités
9. Mettez au frais au moins une heure, afin que les faisselles soient très égoutées
10. Au moment de servir, faites griller le pain, puis arrosez le avec le reste de l'huile. Démoulez les faisselles et tartinez-en les tranches de pain Servez avec une salade de jeunes pousses et des radis coupés en fines rondelles.
