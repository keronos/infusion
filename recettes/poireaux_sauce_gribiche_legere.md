---
author: Stéphanie Labadie
description: Poireaux sauce gribiche légère  
title: Poireaux sauce gribiche légère 
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 20 min
temps-preparation: 30 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/poireaux_sauce_gribiche_legere.jpeg
layout: recette
---

# Ingrédients: 

* 2 poireaux
* 1 cuillère à soupe de câpres hachées 
* 4 cornichons hachés 
* 4 c à soupe d’huile de tournesol ( ou de colza) 
* 2 c à soupe de vinaigre de cidre ou de jus de citron 
* 1 c à café de moutarde 
* 3 oeufs durs 
* 1 poignée de persil/aneth/ estragon/coriandre hachée
* sel et poivre. 

## Déroulé: 

Coupez les poireaux en rondelles de 2 cm et les faire cuire à la vapeur pendant 15 minutes, veillez à ce qu’ils ne cuisent pas trop, et garde de leur texture.

Mettez dans un petit bol mixeur l’huile, le vinaigre ou jus de citron, 2 œufs (vous pouvez si vous le souhaitez gardez un jaune pour la décoration), les cornichons, les herbes fraîches, les cornichons, le sel et le poivre. 

Mixez juste assez pour garder la texture des aliments ( que ce ne soit pas liquide) 

Coupez votre pomme en allumettes, c’est à dire en bâtonnets très fins. 

Hâchez votre oeuf dur. 

Avant de servir, mélangez le poireaux avec la sauve gribiche, puis avec les pommes. Disposez l’oeuf émietté par dessus, avec quelques herbes friches ciselées. Bon appétit !

**ASTUCES**

Pour un repas plus consistant, vous pouvez ajouter des pommes de terres cuites à la vapeur, et même quelques patates douces. 