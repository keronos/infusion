---
author: Stéphanie Labadie
description: Tarte tatin aux légumes confits et parfumée au parmesan et romarin
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/tarte_tatin_legumes.jpg
title: Tarte tatin aux légumes confits
licence: CC-by-Sa
categories: plat
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

### Pour la pâte:
* 150 g de farine semi complète
* 100 g de farine blanche
* 50 g de parmesan
* 150 g de beurre en dés pommade
* 1 c à café de curry ou de romarin au choix
* 1 jaune d’oeuf
* 1/2 c à café de sel
* eau tiède

### Pour les légumes :

* 4 carottes taillées en tronçons de 10 cm, et en biseau
* 2 panais taillés en tronçons de 10 cm, et en biseau
* 1 betterave coupée en gros quartiers
* 2 oignons rouges coupés en gros quartiers
* 4 gousses d’ail écrasées
* 2 branches de romarin
* 2 c à soupe de miel
* 2 grosses pincées de sel
* 2 grosses noisettes de beurre 2 c à soupe d’huile d’olive

## Déroulé

### Pour la pâte
Mélanger du bout des doigts les farines et le beurre pour obtenir un mélange sableux. Mélanger avec
le parmesan, puis le curry ou le romarin. Ajouter un jaune d’oeuf et l’eau, puis mélangée afin
d’obtenir une pâte homogène.

Laisser reposer la pâte.

### Pour les légumes

Disposez vos légumes dans deux grandes poéles, répartissez la moitié des ingrédients dans chacun.
Faites fondre le beurre et chauffer l’huile d’olive. Ajoutez vos légumes, avec les épices , le sel et le miel,

Laissez pénétrez et mouillez avec 2 cl d’eau dans chacune des poéles.

Couvrez et faites cuire à l’étouffée avec une branche de romarin dans chacune des poêles pendant 15
minutes, le temps de faire cuire et confire un peu.

Pendant ce remps, disposez au fond d’un moule à gâteau rond ou d’un plat à tarte quelques petits
bouts de beurre, 4 feuilles des sauges, et 2 c à soupe de sucre.

Disposez joliment dessus vos légumes. Etalez la pâte et disposez là par dessus les légumes. Enfournez
pendant 20 à 25 minutes à 160 degrés. Laissez tiédir avant de déguster, bon appétit !!!
