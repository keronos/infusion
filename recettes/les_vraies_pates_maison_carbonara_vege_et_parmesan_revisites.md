---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/les_vraies_pates_maison_carbonara_vege_et_parmesan_revisites.jpeg
author: Stéphanie Labadie
description: Les vraies pâtes maison, sauce carbonara végétarienne et parmesan revisité 
title: Les vraies pâtes maison, sauce carbonara végétarienne et parmesan revisité 
licence: CC-by-sa
categories: plat
niveau: moyen
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 30 min
layout: recette
---

# Pour les pâtes :

* 200 g de semoule de blé dur
* 150 g de farine de blé ( ou de sarrasin pour varier les plaisirs!)
* 2 œufs
* 1 pincée de sel
* un peu d’eau

......................................

Versez la semoule de blé sur un plan de travail ou un grand saladier, et formez un puit.

Cassez les œufs et placez-les dans le puits. Ajoutez le sel.

Commencez à mélangez du bout des doigts de l’intérieur vers l’extérieur.

Une fois la semoule et les œufs bien mélangés, si vous ne parvenez pas à former une boule parce que la pâte est trop sèche ajoutez de l’eau petit à petit tout en mélangeant jusqu’à obtenir une boule.

Farinez votre plan de travail (propre, c’est mieux) et pétrissez la pâte une dizaine de minutes.

Il vous faut obtenir une pâte souple, sèche au toucher et non collante.

Emballez la pâte dans un film alimentaire ou placez-la dans un saladier couvert d’un torchon humide et laissez-la reposer pendant 30 minutes au frigo.

......................................

Au bout des 30 minutes

Sortez votre machine à pâtes, fixez-la au plan de travail.

Placez la molette de réglage de l’épaisseur de pâte sur la position la plus grande.

Coupez votre pâte en 3 pâtons. Aplatissez-les entre vos mains (ou au rouleau à pâtisserie) puis passez le premier pâton entre les deux rouleaux de la machine. Répétez l’opération jusqu’à obtenir un beau rectangle de pâte.

Réduisez au fur et à mesure l’épaisseur entre les deux rouleaux jusqu’à ce que obteniez l’épaisseur de pâtes désirée.

Pour faire des tagliatelles, passez vos bandes de pâtes légèrement farinées (pour que les tagliatelles ne collent pas entre elles) dans le laminoir pour les découper.

Faites sécher vos pâtes à l’air libre pendant 1 heure minimum. Vous pouvez les faire sécher sur un séchoir à pâtes ou simplement étendues sur un linge propre.

**POUR INFO**

Conservation

Les pâtes fraiches peuvent être conservées **1 à 2 jours** au réfrigérateur si elles ne sont pas cuites, et **3 à 4 jours** si elles sont cuites et jusqu’à **2 mois** au congélateur.

**Cuisson des pâtes**:

Les pâtes fraiches se cuisent en 5 minutes dans une grande quantité d’eau bouillante salée (1 litres pour 100 gr de pâtes).

Elles flottent à la surface lors de la cuisson, mélangez-les délicatement de temps en temps pour une cuisson homogène.

## Pour la sauce carbonara végétarienne:

2 oeufs entiers 

4 jaunes d’oeufs

120 g de pecorino ou parmesan

Sel, poivre 

150 g de tofu fumé 

......................................

Faites rissoler le tofu coupé en de très fines tranches, griller le ou pas son vos envies. 

Faites cuire les pâtes al dente dans un grand volume d'eau bouillante salée pendant 5 minutes. 

Pendant ce temps, battez les oeufs entiers et les jaunes avec la moitié du mélange pecorino-parmesan. Poivrez.

Rajoutez une louche d’eau de cuisson des pâte. Mélangez à nouveau. 

Quand vos pâtes fraîches remontent à la surface, égouttez les et reversez les dans la marmite chaude, ajoutez le tofu fumé puis le mélange oeuf-fromage et remuez délicatement. 

Avec la chaleur, les oeufs et l'eau de cuisson vont former une sauce onctueuse.

Ne reste plus qu’à servir avec du parmesan .. revisité ! 

### Pour le parmesan revisité :

- 100 g d’amandes torréfiées et grossièrement hachées 

- 200 g de chapelure (au mieux du pain sec mixé) 

- 1 c à café de romarin sec 

- 2 pincées de sel

  ......................................

Faites fondre du beurre dans une poele, et y faire dorer votre chapelure, rajoutez les amandes, 2 pincées de sel, le romarin. 

Une bonne odeur devrait s’exhaler de voter poele! 

Retirer du feu, et ajoutez si l’envie vous tente 2 pincées de curry, ou de paprika fumé. 

A servir en toute occasion. Parfait sur des pâtes évidemment, mais aussi sur des salades, des légumes rôtos ou cuits, un fromage frais, un poisson... un allié parfait! 