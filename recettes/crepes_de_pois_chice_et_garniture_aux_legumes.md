---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/crepes_de_pois_chice_et_garniture_aux_legumes.jpeg
author: Stéphanie Labadie
description: Crêpes de pois chiche et garniture aux légumes 
title: Crêpes de pois chiche et garniture aux légumes 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 16/01/2022
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
layout: recette
---

# Pour les crêpes ( 8 crêpes de taille moyenne à peu près): 

250 g de farine de pois chiche

entre 40 et 50cl d'eau 

1 gousse d’ail écrasée 

1 c à café d’épices de votre choix ( facultatif) 

3 càs d'huile d'olive 

sel et poivre

......................................

Préparer la pâte dans un saladier en mélangeant la farine puis l'eau au fur et à mesure jusqu'à l'obtention d'une consistance de pâte à crêpe épaisse, salez et poivrez, ajoutez l'huile d’olive, l’ail et les épices. 

Laissez reposer 30 minutes. Vous pouvez aussi la préparer la veille et la conserver couverte au frigo. 

Chauffez une crêpière, graissez-là avec l'huile d'olive, une fois bien chaude, versez une louche de pâte à crêpe (elle ne doit pas remplir la crêpière comme un pancake), lorsque des bulles apparaissent, retournez-là pour cuire l'autre face. 

Réservez ainsi sur une assiette toutes vos crêpes. Pensez à ajouter un peu d'huile à chaque fois.

Réservez vos crêpes au chaud. 

## Pour la farce : 

Des feuilles de chou ou de blettes, des poireaux, des oignons et/ou des champignons rissolés 

Du fromage type mimolette râpée ou féta émiettée 

Du tofu fumé et/ou un oeuf etc.. 

Préparez une farce dans une poêle : émincez tous vos légumes finement et faites revenir une dizaine de minutes (selon les légumes), assaisonnez et réservez. 

### Pour le dressage: 

Sur une plaque allant au four recouverte de papier sulfurisé, garnissez vos crêpes d'une bonne cuillère de poêlée de légumes, ajoutez le fromage râpé et repliez-là. 

Enfournez pour 6 à 8 minutes. Pendant ce temps, vous pouvez cuire vos œufs au plat. 

Au moment de servir, glissez l'œuf au plat dans chaque crêpe. A table! 

**Astuces**: 

Ces crêpes peuvent se déguster en petit format en entreé ou à l'apéro, avec du houmous, ou de la tapenade, une tartinade carottes cajou etc... 

