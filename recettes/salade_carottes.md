---
author: Stéphanie Labadie
description: Salade de carottes rôties sauce chamoy
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/.jpg
title: Salade de carottes
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 12/10/21
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 5 min
layout: recette
---

## Ingrédients

### Pour 4 personnes :
- 1 kg de carottes épluchées et coupées en bâtonnets de 8X1 cm
- 3 c à soupe d’huile d’olive
- 1 c 1/2 à soupe de sirop d’érable
- 10 g de feuille de coriandre
- 8 abricots secs en fines tranches
- 30 g d’amandes grillées et salées

### Sauce Chamoy :
- 80 g d’abricots secs
- 1 c à café de sirop d’érable
- 3 à 4 cl de jus de citron vert ( selon les gouts)
- 1 petite gousse d’ail
- 3 c à soupe d’huile d’olive
- Facultatif : 1 c à café de piment d’espelette

## Déroulé

Préchauffer le four à 240 degrés

Dans un grand récipient, mélangez les carottes avec l’huile d’olive, le sirop d’érable, 1 c à café de sel
et du poivre si besoin. Etalez les ensuite sur 2 plaques de cuisson tapissées de papier sulfurisé, et
enfournez pour 18 minutes, en le mélangeant à mi cuisson. Les carottes doivent être bien dorées,
mais encore un peu croquantes.

Pendant ce temps, passez tous les ingrédients de la sauce Chamoy au blender avec 1/4 de cuillère à
café de sel pour obtenir une pâte lisse.

Dés que les carottes sont cuites, transférez les dans un grand récipient avec la sauce Chamoy.

Mélangez bien et laissez reposer 20 minutes pour que les saveurs se développent.

Ajoutez sur les carottes les herbes et les abricots en tranches, garnissez d’amandes grillées et arrosez
d’huile d’olive et de citron vert. Servez sans attendre !