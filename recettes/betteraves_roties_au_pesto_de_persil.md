---
image_url: media/betteraves_roties_au_pesto_de_persil.jpeg
author: Stéphanie Labadie
description: Betteraves rôties et leur pesto de persil
title: Betteraves rôties et leur pesto de persil
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 40 min
temps-preparation: 20 min
layout: recette
---

# Pour les betteraves: 

1 kg de betteraves avec la peau, brossées 

.......................................

Préchauffer le four à 220 degrés 

Enveloppez chaque betterave de papier aluminium, puis enfournez les pendant 30 à 60 minutes selon la taille des betteraves. 

En fin de cuisson une lame doit pouvoir pénétrer la chair de la betterave sans résistance. 

Sortez les, laissez tiédir, pelez les et découpez les en rondelles de 0,5 cm d’épaisseur et réservez. 

## Pour le pesto de persil :

1 botte de persil 

1 gousse d’ail 

6 abricots secs 

1 poignée d’amandes ( torréfiées c’est encore mieux )

1 filet de jus de citron 

5 cl d’huile d’olive, selon la consistance choisie ( plus ou moins épaisse) ou faire la moitié avec de l’eau 

.......................................

Ciseler très grossièrement les herbes fraîches et les mixer avec le reste des ingrédients. 

A déguster sur des pommes de terre bouillies, avec des pâtes, une salade, une viande, un poisson, un fromage de chèvre frais, des légumes crus à l’apéro ( carottes, celeri, pomme, concombres etc..)

Cette recette peut se faire avec toutes les herbes fraîches, le pesto aura a chaque un gout différent et tout aussi délicieux: persil, aneth, ciboulette, cerfeuil etc…

### Pour le dressage : 

Disposez les tranches de betteraves sur un plat de service, ajoutez un filet d'huile d'olive, de la fleur de sel, et le pesto. 

Vous pouvez vous amuser à parsemer de quelques noisettes ou amandes torréfiées et concassées, ou des graines de tournesol et de courge grillées.  

