---
author: Stéphanie Labadie
description: Pudding à l’orge et au sésame, sirop à l’orange
title: Pudding à l’orge et au sésame, sirop à l’orange  
licence: CC-by-sa
categories: dessert 
niveau: débutant
date: 13/01/2022
nombre: pour 2 à 4 personnes
temps-cuisson: 30 min
temps-preparation: 30 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/Pudding_a_l_orge_et_au_sesame_sirop_a_l_orange.jpeg
---


# Pour le sirop à l’orange :

* Le jus de 2 oranges
* 40 g de sucre en poudre 
* 1/4 à café d’eau de fleur d’oranger 

Commencez par le sirop. Prélevez le zeste de l’orange, détaillez le en lanière, à mettre dans une casserole avec le sucre et 7 cl d’eau. Portez à ébullition et laissez cuire pendant 1 minute, en remuant sans cesse pour dissoudre le sucre. 

Laissez refroidir.

## Pour le pudding :

* 1/2 c à soupe de graines de sésame blanc grillées 
* 1/2 c à soupe de graines de sésame noir grillées 
* 1 c et demie de sucre muscovado 
* 125 g d’orge mondée trempée toute une nuit dans de l’eau froide, ou à défaut du riz jasmin 
* 50 cl de lait entier 
* 25 cl de lait de coco 
* 1/2 gousse de vanille fendue dans la longueur 
* 1/2 citron pour le zeste ( râpé) 
* 2 oranges dont le zeste de de la moitié d’une des deux( râpé) 
* 20 g de tahini ou de purée d’amandes 

Pelez les oranges à vif, puis prélevez les quartiers au dessus d’un bol pour garder le jus. Ajoutez les dans la casserole avec leur jus et l’eau de fleur d’oranger, réservez. 

Dans un mortier, écrasez vivement les graines de sésame à 1 c à café de sucre muscovado. Réservez. 

Egouttez et rincer l’orge ( idem pour le riz). Mettez le dans une casserole avec le reste de sucre muscovado, le lait, le lait de coco, la gousse de vanille, et les zestes d’agrumes. 

Portez à ébullition et laissez mijoter pendant 20 à 30 minutes selon la céréale choisie. Elle soit être encore un peu ferme, ajoutez un peu de lait si la préparation épaissit trop. 

Laissez refroidir pendant 5 minutes, avant de retire la gousse de vanille. Versez dans des bols et nappez d’1 à café de purée d’amandes diluée dans un peu d’eau ( ou de jus d’orange) . 

Ajoutez les quartiers d’orange et le sirop, parsemez de graines de sésame écrasées et servez! 

**Astuces**: 

Vous pouvez remplacer les oranges pour d'autres fruits de saisons, prunes, raisons, fraises, pêches, nectarines... ils iront à merveille avec ce dessert! 

