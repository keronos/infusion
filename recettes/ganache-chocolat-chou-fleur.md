---
author: Bénédicte Lambert
description: Crème de chocolat blanc au chou-fleur 
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/dessert-chou-fleur.jpg
title: Crème de chocolat blanc au chou-fleur
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 26/10/2018
nombre: 4 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

* 250g de chou-fleur
* 175ml de crème liquide
* 1 gousse de vanille
* 2CS de sucre
* 150g de chocolat noir

## Déroulé

1. Détailler les fleurs de chou-fleur.  
2. Mettre à cuire dans la crème liquide avec la gousse de vanille grattée.  
3. Quand le chou-fleur est tendre, retirer à l'aide d'un chinois.  
4. Mixer le chou-fleur avec 2CS de crème et le sucre.  
5. Chauffer le chocolat au bain-marie.  
6. Verser le reste de crème bouillante sur le chocolat en 3 fois afin d'obtenir une ganache lisse.  
7. Mélanger aussitôt la ganache avec la purée de chou-fleur.  
8. Mettre dans des ramequins et laisser refroidir.  

## Astuces

Faire un fond de biscuit avec des spéculoos et un peu d'huile de coco.  
Ajouter quelques amandes ou noisettes pour ajouter du croquant.  
