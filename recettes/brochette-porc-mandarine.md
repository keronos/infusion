---
author: Stéphanie Labadie
description: Brochettes d’oignons rouges, de poires et de porc mariné aux mandarines et au romarin
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/brochette-porc-orange.jpg
title: Brochettes marinées aux mandarines
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 15 min
temps-preparation: 60 min (plus marinade)
layout: recette
---

## Ingrédients

- 600 g d’échine de porc
- le jus de 8 mandarines
- 2 gousses d’ail écrasée
- 1 c à café de romarin
- 10 cl d’huile d’olive
- 3 c à soupe de soja
- 2 poires
- 2 oignons rouges

## Déroulé

La veille ou quelques heures avant:

Mélangez le jus des mandarines avec l’huile, le soja, le romarin et la gousse d’ail écrasée.

Y faire mariner les cubes d’échine de porc de 3 cm de largeur pendant 2 heures minimum.

Pour la confection des brochettes:

Alternez des cubes de poires et des quartiers d’oignons rouges avec la viande.

Faire chauffer de huile olive dans une poêle et y faire cuire les brochettes sur les 4 côtés.

Rajoutez un peu de la marinade pour attendrir la viande et donner plus de goût.

S’il reste de la marinade après la cuisson des brochettes, la laisser frémir et réduire à petit feu dans une casserole jusqu’à ce qu’elle devienne plus nappante.

Dresser les brochettes avec cette sauce sucrée salée, et quelques feuilles de coriandre ou du sésame pilé au mortier avec un peu de cumin ou de cannelle.

**Petit plus**: pour un repas complet, les accompagner d’une semoule semi complète, et de quelques légumes ( navets, carottes, betteraves) rôtis au four avec du miel, un trait de jus d’orange, de l’huile d’olive et de la fleur de sel.

**Coûts**
