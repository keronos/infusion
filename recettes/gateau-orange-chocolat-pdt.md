---
author: Bénédicte Lambert
description: Un gâteau original à base de pommes de terre
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/gateau-pdt.jpg
title: Gâteau de pommes de terre au chocolat et orange
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 50 min
temps-preparation: 20 min
layout: recette
---


## Ingrédients  
 
* 2 pommes de terre
* 3 oranges
* 150g de sucre complet
* 3 oeufs
* 90g d'huile végétale (olive, tournesol...)
* 100g d'amandes
* 3CS de cacao en poudre
* sel



## Déroulé

1. Faire cuire les pommes de terre à l'eau puis les réduire en purée.  
2. Zester une orange et réserver.  
3. Couper des lanières à confire avec les 2 autres oranges puis les couper en petits cubes. 
4. Presser les oranges. 
5. Les faire blanchir 3 fois en démarrant à l'eau froide et en changeant l'eau.    
6. Puis les faire cuire avec la moitié du sucre et le jus d'orange à feu doux pendant 20/45 min (suivant le feu). Il ne doit plus y avoir de liquide, cela doit faire des morceaux d'orange caramélisés.  
8. Préchauffer le four à 180°C.
9. Mélanger les oeufs avec le reste de sucre, la purée de pommes de terre, les zestes et l'huile.   
10. Mixer les amandes afin de les réduire en poudre et l'ajouter au mélange.     
11. Ajouter à la pâte les oranges confites coupées.  
12. Verser dans un moule huilé et enfourner pour 25/40 min suivant la taille du moule et le four. Plonger la lame d'un couteau ou un cure-dent, il doit ressortir sec.  
 
**Astuces** 

Remplacer 1 oeuf par une courgette râpée afin de réduire les oeufs mais surtout d'introduire des fibres qui ralentissent l'assimilation du sucre.   
Remplacer le cacao par du chocolat noir 70% min et le couper pour faire des pépites.  