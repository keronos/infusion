---
author: Bénédicte Lambert
description: une préparation originale pour une pizza gourmande et légère 
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/pizza-chou-fleur.jpg
title: Pizza à la pâte de chou-fleur
licence: CC-by-sa
categories: plat
niveau: débutant
date: 26/10/2018
nombre: 4 personnes
temps-cuisson: 35 min
temps-preparation: 20 min
layout: recette
---


## Ingrédients

* 500g de chou-fleur
* 2 oeufs
* 1cc d'herbes (origan, herbes de provence, sauge...)
* sel, poivre
* Garniture  
* 1 tomate coeur de boeuf
* 5 champignons
* 1 poivron
* 225g de sauce tomate
* 10 feuilles de basilic
* roquette ou salade fine

## Déroulé

1. Préchauffer le four à 180°C.  
2. Râper les fleurs de chou-fleur avec la grille la plus fine.  
3. Presser à l'aide d'un torchon jusqu'à ce qu'il n'y ait plus d'eau, en procédant par petits tas.  
4. Mélanger avec les oeufs et les herbes jusqu'à l'obtention d'une pâte, saler et poivrer.  
5. Étaler finement sur une plaque recouverte de papier sulfurisé. 
6. Enfourner pour 20 min.  
7. Pendant ce temps,  découper en morceaux les champignons, le poivron et la tomate.  
8. Étaler la sauce tomate sur la pâte et déposer les ingrédients par dessus.  
9. Enfourner à nouveau pour 10/12 min.  
10. Ajouter sur la pizza sortie du four des feuilles de basilic et la salade.  

## Astuces

Mettre seulement la sauce tomate et cuire puis ajouter tous les éléments crus.  
Garnir selon les envies avec de la mozzarella, des courgettes, des artichauts, des tomates cerises...