---
author: Bénédicte Lambert
description: une manière originale de préparer le brocolis
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/steack-brocolis.jpg
title: Steack de brocolis
licence: CC-by-sa
categories: plat
niveau: débutant
date: 26/10/2018
nombre: 4 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

* 250g de brocolis
* 1 oignon
* 1 gousse d'ail
* 50g de champignons
* 25g de poudre d'amandes
* 50g de flocons d'avoine
* 1cc d'épices (garam massala ou ras-el-hanout)
* 2CS d'huile d'olive
* sel, poivre

## Déroulé

1. Faire blanchir les fleurs de brocolis dans une casserole d'eau bouillante.  
2. Bien égoutter le brocolis.  
3. Éplucher ail et oignon et détailler grossièrement.  
4. Nettoyer les champignons.  
5. Mixer tous les ingrédients dans le bol du robot.  
6. Saler, poivrer.  
7. Faire chauffer l'huile d'olive dans une poêle à feu assez vif.  
8. Former les steak et les déposer dans la poêle.  
9. Faire cuire plus longtemps sur la 1ère face afin qu'ils ne s'écrasent pas.  
10. Servir aussitôt.  

## Astuce

Faire cuire au four en badigeonnant d'un peu d'huile pour un résultat moins gras.