---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: Thon façon tataki aux épices.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/thon-tataki.png
title: Bouchées de thon aux graines d'asie
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
---

### Ingrédients

vingtaine de bouchées

- 300g de thon coupé dans une tranche de 1.5 cm d'épaisseur
- 1 cs de graine de coriandre
- 1 cs de poivre du Sichuan
- 1 cc de fenugrec
- 3 cs de graine de sésame
- 1 cc de fleur de sel
- sauce soja

### Préparation

1. Concassez oriandre, poivre et fenugrec dans un mixer ou un moulin à poivre, ajoutez les graines de sésame et la fleur de sel.
2. Versez le mélange dans une assiette creuse
3. Découpez le thon en cube de 1,5 cm de côté, enrobezchaque morceau du mélange de graines en appuyant bien pour obtenir une couche régulière.
4. Faites chauffer 1 cuillère à soupe d'huile dans une poële antiadhésive et faites cuire rapidement les bouchées sur toutes leurs faces.
5. Servez chaud ou tiède, accompagné de sauce soja.
