---
author: Stéphanie Labadie
description: Smoothie à l’avocat  ( ou aux bananes) 
title: Smoothie à l’avocat  ( ou aux bananes) 
licence: CC-by-sa
categories: plat
niveau: dessert
date: 04/01/2022
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/smoothie_a_l_avocat.jpeg
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 5 min
layout: recette
---

# Ingrédients: 

* 2 avocat bien mûrs ou 3 bananes 
* 1 c à soupe de purée d’amandes ou 2 poignées d’amandes ( selon les possibilités, les 2 marchent !) 
* 3 c. à s. de sirop d’agave ou 5 dattes 
* Lait (lait d'amandes pour ma part mais soja, noisettes, riz ou avoine font tout aussi bien l’affaire!)

Vous pouvez y rajouter des dattes, des pruneaux ou des figues séchées , ou 1 c à café de cacao pour un côté dessert gourmand, ou même de l’eau de fleur d’oranger.  

## Déroulé: 

Coupez l'avocat en deux et dénoyautez-le. Épluchez-le et coupez-le en morceaux avant de le placez dans un blender ( idem pour les bananes) . 

Ajoutez les autres ingrédients. Lancez la machine le temps que les avocats soient réduits en purée. 

Donnez plusieurs pulsions de quelques secondes jusqu'à ce que le mélange soit bien lisse. Dégustez.