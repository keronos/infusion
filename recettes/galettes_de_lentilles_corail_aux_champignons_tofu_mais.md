---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/galettes_de_lentilles_corail_aux_champignons_tofu_mais.jpeg
author: Stéphanie Labadie
description: Galettes de lentilles corail aux champignons tofu et mais 
title: Galettes de lentilles corail aux champignons tofu et mais 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
---

# Ingrédients: 

200 g de lentilles corail 

1 bouillon cube de légumes 

2 cm de gingembre 

1 grosse gousse d’ail râpée 

1 pincée d’épice Colombo et 1 pincée de cannelle 

3 tige de ciboule ciselées ( ou à défaut une herbe fraîche)

200 de champignons coupés en gros morceaux 

150 de tofu fumé coupé en fines tranches 

150 de mais 

1 gousse d’ail écrasée

1 c à soupe de soja 

1 c à café de miel 

## Déroulé: 

Faites tremper vos lentilles pendant 24 heures dans un grand saladier d’eau ( elles doivent être immergées). 

Le jour de la préparation, faites fondre votre bouillon cube dans 50 cl d’eau bouillante. 

Egouttez vos lentilles, puis mélangez les dans un robot mixeur avec le gingembre et l’ail, les épices et 2 pincées de sel. Mixez vos lentilles encore crues ( mais encore gorgées d’eau), et rajoutez du bouillon de légumes pour obtenir la consistance d’une pâte à pancakes. Mélangez à la spatule la ciboule ciselée. 

Dans une autre poêle, faites chauffer de l’huile d’olive ( ou du beurre de coco)et faites cuire vos champignons avec la gousse d’ail. Rajoutez le tofu, et le mais. Puis terminez de cuire avec le soja et le miel. Réservez. 

Dans une poéle, faites chauffer de l’huile d’olive ( ou du beurre de coco), et faire cuire vos galettes comme des blinis ou des pancakes en prenant soin de creuser un petit trou au milieu, où vous déposerez une grosse cuillère du mélange champignons/ tofu/ mais. Faites cuire de l’autre côté, puis réservez au chaud. 

### Dressage : 

Vous pouvez manger ces galettes avec une yaourt brassé parfumé aux fines herbes, de la chair de tomates parfumée à la coriandre, fleur de sel et huile d’olive ou une sauce vierge de saison ( pomme, olives, céleri, échalotes, coriandre, oranges l'hiver; tomates, fraises, olives, céleri, échalote, estragon l'été ). 