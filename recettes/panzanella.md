---
author: Stéphanie Labadie
description: réutilisation du pain rassis pour une salade hivernale
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/panzanella.jpg
title: Panzanella hivernale
licence: CC-by-sa
categories: plat
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

### Ingrédients

- 200 g de pain de campagne rassis et tranché
- 1 fenouil
- 2 betteraves
- 3 gousses d’ail
- 2 poires
- 2 c à soupe de vinaigre de vin blanc
- 3 c à soupe d’huile d’olive ( de bonne qualité) + assaisonnement
- 2 pincées de fleur de sel
- 5 feuilles de basilic ciselées

### Déroulé

1. Disposer les betteraves en gros cubes et les carottes en rondelles dans un grand plat.
2. Mélanger avec les 3 gousses d’ail un filet d’huile d’olive généreux, une cuillère à café de miel, et 1 cuillère à soupe de romarin.
3. Y ajouter le pain préalablement frotté à 1 gousse d’ail et taillé en gros croûtons.
4. Enfourner 20 minutes à 180 degrés.
5. Pendant ce temps, ciseler le fenouil, et couper les poires en quartiers.
6. Ciseler le basilic.

Pour la vinaigrette, mélanger le jus d’une orange avec 3 cuillères à soupe d’huile d’olive, 1
cuillère à soupe de soja, et une cuillère à café de miel. Les mélanger avec le fenouil et les
poires.

Y ajouter les légumes rôtis et les croûtons refroidis, puis le basilic ciselé.

Déguster !

La panzanella peut se préparer l’été avec des tomates, des concombres, est pourquoi pas de la
pastèque et de la fêta.
