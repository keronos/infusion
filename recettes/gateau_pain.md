---
author: Stéphanie Labadie
description: Gâteau gourmand au pain rassis, à la pomme et aux (rhum) raisins
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gateau_pain.jpg
title: Gâteau gourmand au pain rassis
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

### Ingrédients

- 1 baguette de pain sec (ou équivalent)
- 50 cl de lait + eau
- 4 à 5 pommes
- 1 banane
- 1 verre d’amandes moulues
- 1 verre de raisin sec
- 250 g. de sucre semoule
- 1 cuillère à café de cannelle
- 3 cuillère à soupe de rhum
- 4 oeufs

### Déroulé

1. Tremper le pain sec dans le mélange eau/lait.
2. Pendant ce temps râper finement les pommes après les avoir épluchées et épépinées.
3. Essorer le pain, le mixer grossièrement et mélanger aux pommes râpées.
4. Ajouter les amandes moulues, les raisins secs ( trempées dans un fond d’eau tiède et le rhum), la banane écrasée, le sucre, la cannelle, le reste du rhum et les oeufs.
5. Mélanger, de manière à rendre la préparation homogène.

Pour la cuisson du gâteau: remplir un moule à gâteau de la préparation, puis disposer 60 g d’amandes concassées et 20 g de sucre par dessus afin de caraméliser le tout durant la cuisson à four moyen (180°C) pendant environ 30 minutes.
