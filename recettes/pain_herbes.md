---
author: Stéphanie Labadie
description: Petits pains aux fines herbes
image_url: https://github.com/bndct-lmbrt/mes-recettes/blob/master/medias/pain_herbes.jpg
title: Petits pains aux fines herbes et au seigle
licence: CC-By-SA
categories: entrée
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 30/40 min
temps-preparation: 20 min (fermentation 12h)
layout: recette
---

## Ingrédients

* 15 cl d’eau tiède
* 1 c à café de sucre de canne
* 1 c à café bombée de levure déshydratée
* 250 g d farine de blé complet
* 100 g de farine de seigle complet
* 3/4 de c à café de sel
* 3 gousses d’ail
* 2 c à soupe d’huile d’olive
* 1 c à café de persil haché
* 1 c à café de ciboulette hachée

## Déroulé

Dans un pichet, mélangez l’eau, le sucre et l levure, puis laisser reposer pendant 15 minutes.

Dans un saladier, mélangez les farines et le sel. Ajoutez le liquide et pétrir 5 minutes.

Formez une boule, la déposer dans le saladier, couvrir et laisser lever 1 heure.

Former 5 boules, puis les roulez en 5 pâtons fins.

Mélangez les gousses d’ail écrasées ou hachées avec l’huile et les herbes, en badigeonner les patins, les nouer au centre et rabattre les extrémités en dessous.

Les déposer sur une plaque couverte de papier cuisson et cuire au four à 180 degrés pendant 10 à 15 minutes.
