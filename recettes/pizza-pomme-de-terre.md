---
author: Bénédicte Lambert
description: Une pizza à base de pommes de terre
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/pizza-pdt.jpg
title: Pizza en croûte de pommes de terre
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
temps-repos: 30 min
layout: recette
---


## Ingrédients  
 
Pâte :  
* 3 pommes de terre
* 1/2 céleri rave
* 1 oeuf
* sel, poivre

Garniture :  
* sauce tomate
* légumes de saison
* salade fraîche
* herbes aromatiques (thym, persil, basilic, origan...)


## Déroulé  
 
1. Éplucher puis râper les pommes de terre.  
2. Retirer la peau du céleri et râper.    
3. Ajouter une pincée de sel et laisser dégorger 30 min.  
4. Essorer le mélange à l'aide d'un torchon.  
5. Dans un saladier, mélanger avec l'oeuf et poivrer.  
6. Préchauffer le four à 220°C.  
7. Étaler la pâte sur la plaque du four recouvert de papier cuisson de manière à faire des pâtes assez fines.     
8. Enfourner pour 20 min.  
9. Disposer la sauce tomate et enfourner à nouveau 10/15 min.  
10. Recouvrir des ingrédients de votre choix : poivrons, tomates, artichauts, courgettes, et en hiver poireaux, épinards, chou-rave, radis, champignons...  
11. Ajouter salade et herbes aromatiques pour plus de fraîcheur et de goût.  
 
**Astuces** 

Utiliser un cercle à pâtisser ou dessiner sur le papier cuisson pour faire des pizzas rondes et harmonieuses.  
Mélanger à la pâte de pizza de pommes de terre un légume tel que le céleri ou le panais lui apporte des fibres et lui permet de faire redescendre son indice glycémique assez haut.  