---
author: Bénédicte Lambert
description: soupe traditionnelle des pays de l'est avec ici comme variante l'utilisation de chou
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/soupe_brotsch_decoree.png
title: Soupe bortsch
licence: CC-by-sa
categories: plat
niveau: débutant
date: 09/11/2018
nombre: 4 personnes
temps-cuisson: 40 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 500g de betterave
* 1 oignon
* 2CS d'huile d'olive
* 5cm de gingembre frais
* 2 tomates
* 1 carotte
* 1/2 chou vert
* 1OO ml de vinaigre
* sel, poivre

Accompagnement  
* 100g de fromage blanc ou de crème fraîche	
* 1 botte d'aneth

### Déroulé  

1. Râper la betterave et hacher l’oignon finement. 
2. Faire chauffer l'huile et faire dorer l'oignon.  
3. Peler et hacher le gingembre, l'ajouter à l'oignon.  
4. Couper les tomates et les carottes en petits dès.  
5. Hacher le chou.  
6. Faire revenir tous les légumes.  
7. Déglacer au vinaigre.  
8. Ajouter le bouillon et l'eau à couvert.  
9. Laisser mijoter 30 min.  
10. Assaisonner.  
11. Servir accompagné de fromage blanc et d'aneth ciselé.
