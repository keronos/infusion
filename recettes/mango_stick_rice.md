---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/mango_stick_rice.jpeg
author: Stéphanie Labadie
description: Mango stick rice 
title: Mango stick rice
licence: CC-by-sa
categories: dessert 
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients: 

* 200 g de riz gluant 
* 20 cl de lait coco
* 1 pincée de sel 
* 100 g de sucre complet muscovado 
* 1 mangue fraîche ( ou une orange, ou un ananas, kiwi etc…)

Cuire le riz à la vapeur pendant 25 minutes (dans un rice cocker c’est l’idéal ). 

Dans une casserole, mettre le lait de coco à chauffer doucement avec le sucre et la petite pincée de sel. Coupez le feu dés les premier signes d’ébullition. 

Pelez et coupez la mangue en dés, réservez. 

Dés que le riz est cuit, le mettre dans un saladier. L’arroser avec les 2/3 du lait de coco en mélangeant délicatement. Réservez le reste de lait de coco. Le riz doit tout absorber. Laissez reposer 15 minutes à température ambiante. 

## Dressage: 

Répartissez le riz dans des contenants individuels, recouvrez avec les dés de mangue. Arrosez avec le reste de lait de coco. Servez à température ambiante. 

Vous pouvez aussi opter pour la version gros dessert à partager dans un grand plat à service.! 

**Astuce**: 

Vous pouvez troquer la mangue contre des prunes, oranges ou raisins, ou des fraises, des nectarines ou des abricots selon les saisons, ce sera aussi très bon ! 