---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade_de_carottes_roties_sauce_chamoy.jpeg
author: Stéphanie Labadie
description: Salade de carottes rôties sauce chamoy 
title: Salade de carottes rôties sauce chamoy
licence: CC-by-sA
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 20 min
temps-preparation: 20 min
layout: recette
---

# Pour les carottes: 

- 1 kg de carottes épluchées et coupées en bâtonnets de 8X1 cm
- 3 c à soupe d’huile d’olive 
- 1 c 1/2 à soupe de sirop d’érable
- 10 g de feuille de coriandre
- 8 abricots secs en fines tranches 
- 30 g d’amandes grillées et salées 

..........................................................

Préchauffer le four à 240 degrés 

Dans un grand récipient, mélangez les carottes avec l’huile d’olive, le sirop d’érable, 1 c à café de sel et du poivre si besoin. 

Etalez les ensuite sur 2 plaques de cuisson tapissées de papier sulfurisé, et enfournez pour 18 minutes, en le mélangeant à mi cuisson. 

Les carottes doivent être bien dorées, mais encore un peu croquantes. 

## Pour la sauce Chamoy : 

- 40 g d’abricots secs 
- 1 c à café de sirop d’érable 
- 4 cl de jus de citron vert 
- 1 petite gousse d’ail 
- 2 c à soupe d’huile d’olive 
- Facultatif : 1 c à café de piment d’espelette 

..........................................................

Pendant ce temps, passez tous les ingrédients de la sauce Chamoy au blender avec 1/4 de cuillère à café de sel pour obtenir une pâte lisse. 

Dés que les carottes sont cuites, transférez les dans un grand récipient avec la sauce Chamoy. Mélangez bien et laissez reposer 20 minutes pour que les saveurs se développent. 

Ajoutez sur les carottes les herbes et les abricots en tranches, garnissez d’amandes grillées et arrosez d’huile d’olive et de citron vert. Servez sans attendre ! 