---
author: Stéphanie Labadie
description: Tartelettes façon banoffe, banane mangue ou fraises/nectarines
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tartelette_banofee.jpg
title: Tartelettes façon banoffe
licence: CC-by-Sa
categories: dessert
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

### Pour la garniture :

* 2 bananes de la noix de coco râpée torréfiée

### Pour la crème fouettée:
* 1 boite de lait de coco
* 2 c à soupe de sucre de canne

### Pour 150 g de petits flocons d’avoine
* 4 dates finement hachées
* 20 g de chocolat rapé
* 3 c à soupe de sirop d’agave ( ou d’érable)
* 4 c à soupe de lait d’amande

## Déroulé

### Pour la crême fouettée

La veille, placez la boîte de lait de coco au frais. Récupérez la crème de coco figée sur le
dessus avec une cuillère, et et la fouetter avec le batteur électrique dans un grand bol avec le
sucre pour obtenir une crème onctueuse et épaisse. 

Versez dans une poche munie d’une douille et laissez au frais avant le dressage.

### Pour les tartelettes

Mélangez les ingrédients du fond de tarte. Laissez reposer 15 minutes. Huilez les moules à
tartelettes et y tasser le mélange. 

Cuire au four à 150 degrés pendant 20 minutes. Les fonds de tarte doivent être dorés et croustillants.

Pour le dressage : prenez vos tartelettes, garnissez les de crème de coco, disposez quelques rondelles de bananes et saupoudrez d’un peu de coco râpée, et dégustez !

ASTUCES /// vous pouvez utiliser des lamelles de mangue aussi, ou de fraises, ou des framboises, des nectarines ou des prunes, selon la saison .