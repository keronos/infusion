---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gnocchis_de_potimarron_au_beurre_miso_et_sa_tombée_d_epinards.jpeg
author: Stéphanie Labadie
description: Gnocchis de potimarron au beurre miso et sa tombée d’épinards  
title: Gnocchis de potimarron au beurre miso et sa tombée d’épinards  
licence: CC-by-sa
categories: plat
niveau: moyen
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 30 min
layout: recette
---

# Pour les gnocchis pour 6 personnes:  

* 800g Pommes de terre à purée
* 350g Farine
* 1Oeuf
* 200g Potimarron
* 2cuil. à soupe Huile d’olive
* 40g Parmesan
* 1 Branche de thym
* 20g de beurre

..............................

Lavez, épluchez et coupez les pommes de terre en gros cubes. Faites-les cuire à la vapeur pendant 20 à 30minutes.

Une fois cuites, passez-les au presse-purée.

Coupez la chair de potimarron en petits dès. Poêlez-les dans l’huile avec un peu de sel sur feu moyen/fort pendant une quinzaine de minutes. Ajoutez un peu d’eau en cours de cuisson si nécessaire. Ecrasez les dés de potimarron.

Dans un saladier, mélangez la purée de pommes de terre avec la purée de potimarron, l’œuf, la farine ainsi que du sel.

Pétrissez la pâte jusqu’à ce qu’elle devienne souple et lisse. Façonnez vos gnocchis. Farinez-les légèrement.

Faites fondre une grosse noisette de beurre dans une poele, et faites cuire et dorer vos gnocchis en plusieurs fois ( ils ne vont pas tous loger en même temps, l’idée est qu’ils soient bien dorés) . Réservez. 

## Pour la sauce au beurre miso : 

1 c à soupe de pâte de miso 

1 citron vert : 1 c a café de zeste et 2 c à café de jus

5 g de gingembre frais 

50 g de beurre doux 

2 oignon verts en fines lamelles 

1 c à café de graines de sésame blanc

............................................................

Dans une sauteuse, mélanger la sauce miso, le jus de citron vert, le gingembre et le beurre, posé fouettez pendant 3 minutes jusqu’à ce que le beurre fonde et que la sauce soit lisse et légèrement épaisse. 

Attention, elle ne soit pas bouillir ou tourner. Si tel est le cas, sortez du feu, ajoutez quelques gouttes d'eau et fouettez vivement pour retrouver une texture homogène. 

Retirez la sauteuse du feu et réservez. 

### Pour la tombée d’épinards : 

- 300 g d’épinards 
- huile d’olive 
- un filet de citron 
- Sel 

.................................................

Lavez vos feuilles d’épinards et séchez les. 

Faites chauffer de l’huile dans une sauteuse, et faire revenir les feuilles d’épinards pendant 2 minutes, le temps qu’elles cuisent mais pas trop, sortir du feu. 

Assaisonnez avec du sel, un filet de citron et pourquoi une pincée de curry ou de cumin. 

#### Dressage : 

Quand tous vos ingrédients sont prêts, mettez une cuillère de sauce dans le fond d'une assiette, disposez les gnocchis avec la tombée d’épinards, une autre cuillère de sauce pour napper. Vous pouvez ajouter quelques lamelles de parmesan ou des graines des sésame. 

Buen provecho !!! 