---
author: Bénédicte Lambert
description: du fruit, des graines, un four et la cuisine se transforme en confiserie
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/bonbon-lin-pomme.jpg
title: Bonbon à la pomme
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 30/01/2019
nombre: 50 formes
temps-cuisson: 5 heures
temps-preparation: 20 min
temps-repos: 2h
layout: recette
---

## Ingrédients

Pour 50 formes

- 100g de graines de lin
- 100g de graines de tournesol
- 100g de noix
- 1 pomme
- 1 pincée de cannelle
- 60g de sucre mascobado

## Déroulé

1. Faire tremper les graines de lin.
2. Faire tremper les graines de tournesol et les noix.
3. Rincer les graines de tournesol et les noix.
4. Mixer tous les ingrédients.
5. Étaler sur une plaque et faire cuire à 40°C pendant 5h ou à 180°C pendant Xmin. Ou faire des formes et les faire cuire sur la plaque du four.

**Coûts**

- lin = 0,74€
- tournesol = 0,60€
- noix = 2,07€
- pomme = 0,21€
- cannelle = 0,05€
- sucre = 0,60€

total = 4,27€ pour 50
soit 0,085€/pièce
