---
author: Pascal Romain
description: des laits végétaux à réaliser avec un extracteur ou un simple mixeur.
title: laits végétaux
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 12/05/2017
nombre: 4 personnes
temps-cuisson: sans
temps-preparation: 10 min
layout: recette
image_url: https://github.com/bndct-lmbrt/mes-recettes/blob/master/medias/lait-vegetal.jpg

---

# Laits végétaux


## Ingrédients

- 100 à 200g de fruits à coque : noisettes, amandes ou noix de cajou
- 1L d'eau

## Déroulé

1. Faire tremper les fruits à coque dans de l'eau pendant 1 nuit ou 6h minimum.
2. Vider l'eau de trempage.
3. Mixer avec l'eau à l'aide d'un plongeur mixer ou d'un blender.
4. Filtrer avec un torchon.

**Astuces**

1. Garder l'okara (la pulpe qui reste dans le torchon) pour mettre dans une soupe, sur un gratin, un dessert, une chapelure...
2. Aromatiser le lait avec du chocolat cru ou du cacao non sucré.

Si le lait doit être chauffé, faire tout doucement pour ne pas enlever les nutriments.

Cette boisson se garde 3/4 jours maximum au frigo.
