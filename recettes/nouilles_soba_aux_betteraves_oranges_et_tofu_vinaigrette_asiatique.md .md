---
author: Stéphanie Labadie
description: Nouilles soba aux betteraves, oranges et tofu, vinaigrette asiatique 
title: Nouilles soba aux betteraves, oranges et tofu, vinaigrette asiatique 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 5 min
temps-preparation: 20 min
layout: recette
images_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/nouilles_soba_aux_betteraves_oranges_et_tofu_vinaigrette_asiatique.jpg
---

# Pour la salade de nouilles :

* 400 g de betterave cuite épluchée 
* 2 oranges 
* 300 g de nouilles soba 
* 150 g de tofu fumé 
* 1 poignée de graines de courges et de tournesol torréfiées ( ou de cacahuètes grossièrement hâchées) 
* 1/3 botte de coriandre ciselée 

Mettre la betterave dans un mixer et donnez 3 impulsions, le temps de la hâcher très grossièrement. 

Levez les suprêmes d’orange. 

Emincez le tofu en de fines tranches.

Cuire les nouilles soba dans une eau à ébullition pendant 4 à 5 minutes. Egouttez et passez les sous l'eau froide en les séparant avec les doigts pour éviter qu'elle ne se collent entre elles. Réserver.

## Pour la sauce :

* Le jus d’une mandarine 
* 3 c à soupe de soja 
* 3 c à soupe de vinaigre de riz 
* 1 c à soupe de sirop d’agave 
* 2 c à soupe d’huile de sésame grillée
* 2 c à soupe d’huile d’olive 
* 1 filet de jus de citron

Mélangez tous les ingrédients de la sauce. 

### Pour le dressage : 

Juste avant de servir; mélangez les nouilles, la betterave, le tofu et les oranges avec la sauce. Disposez dans des assiettes en saupoudrant de graines de céréales, ou de cacahuètes hâchées, et de la coriandre. 

A déguster tout de suite, avec ou sans les baguettes! 

**Astuce**: 

Pour en faire un repas vraiment complet, vous pouvez ajouter un oeuf mollet, ou un peu de poisson fumé. Et troquer les betteraves contre des oignons, des carottes, des courgettes ou des tomates rôtis selon les saisons. Enfin, il est possible d'ajouter quelques feuilles de mâche ou d'épinards, tout est possible ou preque!  