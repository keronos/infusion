---
categories: info
layout: recette
---

# Modèle de fiche

Ajouter les champs ci-dessous précédés de trois tirets --- et terminés par trois tirets ---

- author: Le créateur ou la créatrice de la recette. Par exemple Matthieu Dechart
- description: une courte description de la recette. Par exemple "préparation du carrelet en filet accompagné des légumes de saison."
- image_url: l'adresse d'une image d'illustration de la recette. Dans la mesure du possible il est préconisé de n'associer que des images dont la largeur est égale ou inférieure à 500 px. Cette image peut être stockée dans le dossier des recettes de ce dépôt ou quelque part sur Internet. Par exemple "https://raw.githubusercontent.com/akakeronos/recette-gourmandignes/master/images/filet-carrelet.jpg"
- title: le titre de la recette. Par exemple "Filets de carrelet aux légumes de printemps"
- licence: la licence de publication de la recette. Par exemple "CC-by-sa"
- categories: la catégorie de la recette. Par exemple "poisson". Actuellement les catégories utilisées sont "poisson, légumes, tarte, dessert, entrée, salade"
- niveau: le niveau de difficulté d'exécution de la recette. Par exemple "débutant"
- date: la date de publication/création de la recette Par exemple "12/05/2017"
- nombre: lorsque les quantités sont spécifiées dans la recettes, il est possible d'indiquer en en-tête le nombre de personnes auxquelles correspondent ces quantités. Par exemple "8 personnes"
- temps-cuisson: le temps de cuisson si pertinent. Par exemple "30/40 min"
- temps-preparation: le temps de préparation. Par exemple "25 min"
- layout: recette

## Ingrédients

- ingrédient 1
- ingrédient 2

## Déroulé

1. étape 1
2. étape 2

**Astuces**
