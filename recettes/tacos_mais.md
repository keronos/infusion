---
author: Stéphanie Labadie
description: une recette de galette de maïs
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tacos_mais.jpg
title: Tacos maïs
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 15/04/2021
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 5 min
layout: recette
---

### Ingrédients

* 150 g farine de mais
* 20 cl d’eau bouillante

### Déroulé

Mettez la farine et une bonne pincée de sel dans un récipient et ajoutez les 25 cl d’eau
bouillante, en remuant avec une spatule jusqu’à ce que la pâte se rassemble. 

Quand la pâte à suffisamment refroidi, pétrissez là et formez une boule lisse ayant la texture d’une pâte à modeler.

Divisez là en 12 bâton d’environ 30 g chacun, que vous laisserez sous un torchon humide.

Huilez légèrement vos mains pour façonner chaque bâton en une boule lisse.

Faites chauffer une pole anti adhésive. Placez un morceau de pâte entre deux feuilles de papier sulfurisé, et à l’aide d’une poele à fond épais, pressez de façon uniforme pour l’abaisser
en un disque de 10/12 cm de diamètre. 

Retirez la feuille du dessus et gardez celle du dessous pour faire cuire la tortilla dans la poele chaude. 

Faites la cuire 1mn30 jusqu’à ce qu’elle soit bien dorée, et faites pareil de l’autre côté. Transférez la sur un torchon dont vous rabattrez les côtés pour bien la couvrir. 

Faites pareil avec le reste des pâtons.

Garnissez vos tacos d’une bonne cuillère d’avocats et ajoutez par dessus le mélange aux oignons; ajoutez quelques feuilles de coriandre et … bon appétit !!