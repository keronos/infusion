---
author: Stéphanie Labadie
description: Version vegan du lemon curd à la noix de coco et petits fond de tarte aux zestes de citron
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/lemon-curd.jpg
title: Lemon curd coco vegan
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 20 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

Pour le lemon curd:

- 60 g d’eau
- 110 g de jus de citron
- 2CS de graines de lin
- 70 g de sirop d’agave
- 10 g de maizena
- 40 g d’huile de coco
- 50 de noix de coco râpée

Fonds de tarte:

- 1 c à soupe de pureé d’amandes ou de noisettes
- 3 c à soupe d’huile neutre ou parfumée ( coco, noisette, olive)
- 3 c à soupe de sucre de canne complet
- 175 g de farine de petit épeautre complet
- 4 c à soupe de lait d’amande

## Déroulé

Lavez les citrons et prélevez les zestes au-dessus d'une casserole.

Faire tremper les graines de lin dans un bol avec 4 Cs d’eau.

Mixer les graines de lin à l’aide d’un plongeur ou d’un petit robot.

Verser dans une casserole, l’eau, le jus de citron, la pâte de graine de lin, la maïzena et le sirop d’agave.

Remuer à froid puis faire chauffer sur feu doux environ 5 minutes jusqu’à ce que l’ensemble épaississe légèrement.

Ajouter ensuite l’huile de coco, puis la noix de coco râpée, une fois l’huile bien intégrée.

Transférer dans un récipient puis réserver au frais.

Dans un saladier, mélangez la purée d’amandes, l’huile et le sucre. Ajoutez la farine et mélangez.

Ajoutez le lait et pétrir pour obtenir un boule de pâte.

La filmer et la placer pour 30 minutes au frais avant utilisation.

Cuisson à blanc: 15 minutes au four à 150 degrés.

**Dressage**: disposez des petits cuillères de lemon curd sur les fonds de tarte ou les biscuits cuits et refroidis. Saupoudrez de noix de coco râpée, et de un deux zestes de citron restants ou de pistache concassée.

**Petit plus**: Vous pouvez garder le lemon curd en pot et le servir avec des yaourts ou sur du bon pain grillé, voir des tranches de brioche. Agrémenté de quelques fruits frais émincés: fraises, pêches, poires, pommes etc…

**Variante** : existe en version [flexitarien](lemon-curd.html)
