---
author: Stéphanie Labadie
description: Gnocchis de potimarron et sa tombée d’épinards à la crême de mimolette
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/.jpg
title: Gnocchis de potimarron
licence: CC-by-sa
categories: plat
niveau: débutant
date: 08/10/2021
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
temps-repos: 30 min
layout: recette
---


## Ingrédients  
 
### Pour les gnocchis pour 6 personnes:

* 800g Pommes de terre à purée
* 350g Farine
* 1 Oeuf
* 200g Potimarron
* 2cuil. à soupe Huile d’olive
* 40g Parmesan
* 1Branche de thym
* 20g de beurre

### Pour la tombée d’épinards
- 500 g d’épinards
- huile d’olive
- un filet de citron
- 10 cl de crème
- 15 g de fromage a pâte dure type gouda ou comté
- Sel

## Déroulé  
 
### Gnocci

Lavez les pommes de terre puis faites-les cuire à la vapeur pendant une trentaine de minutes.

Pelez les pommes de terre encore chaude et passez-les au presse-purée.

Coupez la chair de potimarron en petits dès. Poêlez-les dans l’huile avec un peu de sel sur feu
moyen/fort pendant une quinzaine de minutes. Ajoutez un peu d’eau en cours de cuisson si
nécessaire. Ecrasez les dés de potimarron.

Dans un saladier, mélangez la purée de pommes de terre avec la purée de potimarron, l’oeuf, la
farine ainsi que du sel.

Pétrissez la pâte jusqu’à ce qu’elle devienne souple et lisse. Façonnez vos gnocchis. Farinez-les
légèrement.

Faites fondre une grosse noisette de beurre dans une poele, et faites cuire et dorer vos gnocchis en
plusieurs fois ( ils ne vont pas tous loger en même temps, l’idée est qu’ils soient bien dorés) .

Réservez.

### Tombée d'épinards

Lavez vos feuilles d’épinards et séchez les. Faites chauffer de l’huile dans une sauteuse, et faire
revenir les feuilles d’épinards pendant 2 minutes, le temps qu’elles cuisent mais pas trop, ajoutez la
crême, laissez les épinards s’imprégner, puis sortir du feu. Assaisonnez avec du sel, un filet de
citron et pourquoi une pincée de curry ou de cumin.

Quand tous vos ingrédients sont prêts, disposez les gnocchis dans une assiette, avec la tombée
d’épinards et la sauce pour napper. Vous pouvez ajouter quelques lamelles de parmesan ou des
graines des sésame. Bon appétit !