---
author: Bénédicte Lambert
description: Taboulé de chou fleur, pomme, céleri, cranberries, coriandre, sauce miel citron
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/semoule-chouFleur.jpg
title: Taboulé de chou fleur
licence: CC-by-sa
categories: plat
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

* 1/2 chou fleur
* 1 pommes coupée en cubes
* 1 branche de céleri coupée en cubes
* 30 g de cranberries
* 1 poignée d’amandes torréfiées et concassées
* 1/2 botte de coriandre émincée
* Le jus d’un citron
* 5 c à soupe d’huile d’olive
* 1 c à café de sucre

## Déroulé

Râpez le chou fleur, ajoutez la pomme et le céleri, puis les cranberries, la coriandre et les amandes.

Mélanger le jus de citron, l’huile d’olive et le sucre.

Mélanger les ingrédients avant de servir