---
author: Bénédicte Lambert
description: faire des rouleaux de printemps soi-même c'est facile
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/rouleaux-printemps.jpg
title: Rouleaux de printemps
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 22/07/2018
nombre: 4 personnes
temps-cuisson: 5 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

- 8 à 16 feuilles de riz
- 120g de vermicelles de riz
- 2 poignées de cacahuètes hachées
- germes de soja ou graines germées
- 5 branches de menthe
- 5 branches de coriandre
- Légumes au choix: mélange carotte, betterave, poivrons de différentes couleurs, carottes, concombre, choux, salade...
- Sauce soja ou sauce améliorée avec gingembre, vinaigre de riz, citron (cf recette vinaigrettes)

## Déroulé

1. Faire cuire les vermicelles.
2. Couper tous les ingrédients.
3. Faire tremper la feuille de riz dans un récipient d’eau chaude.
4. Déposer avec un torchon humide dessous pour ne pas coller.
5. Mettre les ingrédients dans le 1er tiers du haut et au centre. Disposer petit à petit en veillant à ne pas trop en mettre, sinon il ne fermera pas ou se déchirera.
6. Verser la sauce de préférence sur les vermicelles de riz pour assaisonner sans avoir besoin de tremper.
7. Plier les côtés et rouler.
8. S'ils sont préparés en avance, conserver dans un linge humide.

**Astuces**

Privilégier les légumes de saison et agrémenter selon vos envies.

Peut se faire en toutes saisons.
