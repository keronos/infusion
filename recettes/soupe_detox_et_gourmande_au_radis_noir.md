---
author: Stéphanie Labadie
description: Soupe detox et gourmande au radis noir 
title: Soupe detox et gourmande au radis noir 
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 04/01/2022
nombre: 4 à 6 personnes
temps-cuisson:  20 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/soupe_detox_et_gourmande_aux_radis_noir.jpeg
layout: recette
---

# Ingrédients: 

* 1 oignon 
* 1 gros radis noir
* 2 pommes de terre
* 12 cl de crème liquide à 4% de MG
* 1,5 l d’eau avec 1 cube de bouillon de volaille ou de légumes
* 1 c. à café de paprika fumé ou de cumin 
* poivre



## Déroulé: 

Epluchez et émincez l’oignon. 

Épluchez le radis noir à l'aide d'un couteau d'office puis lavez-le. Coupez-le ensuite en morceaux. Épluchez la pomme de terre puis lavez-la et coupez-la également en morceaux. Réservez.

Dans une marmite ou une cocotte, faites revenir une grosse noisette de beurre et faites y dorer l’oignon. Ajoutez l’eau et le bouillon. Porter à ébullition puis mettez les autres légumes et mélangez à l'aide d'une spatule ou une cuillère en bois. Couvrez la marmite puis laissez mijoter la soupe détox à feu doux pendant 30 min.

À la fin de la cuisson, mixez tous les légumes avec la crème liquide et l’épice de votre choix jusqu'à obtenir un velouté onctueux et homogène.

### Pour le dressage :

Vous pouvez remplacer le radis noir par des navets, ou faire un mélange des deux, ça marchera très bien aussi! 

Vous pouvez servir cette soupe avec les épluchures de pommes de terre rôties au four, ou quelques croûtons et brins d’aneth. 

Autre possibilité : l’accompagner avec du maquereau ou de la truite fumée. 

Ou juste quelques fines tranches de radis noir ou de radis restantes avec un filet d’huile d’olive et une pincée de fleur de sel. 
