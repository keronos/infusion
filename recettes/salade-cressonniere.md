---
author: Bénédicte Lambert
description: Une salade détox à base de cresson
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/salade-cressoniere.jpg
title: Salade de légumes sauce cressonnière
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/19
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 5 min
layout: recette
---

## Ingrédients

- 500g de légumes
- 2 oeufs
- 1 botte de cresson
- persil
- 2CS d'huile
- 1CS de vinaigre
- sel, poivre

## Déroulé

Faire une salade de légumes.
Cuire les oeufs durs.  
Hacher le cresson sans le cuire, ajouter un peu de persil, huile, vinaigre, sel et poivre.

**Astuces**
Sans cresson, on le remplace par le persil.
