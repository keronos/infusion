---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/flan_a_la_patate_douce_coulis_d_orange.jpeg
author: Stéphanie Labadie
description:  Flan à la patate douce, coulis d'orange 
title: Flan à la patate douce, coulis d'orange 
licence: CC-by-sa
categories: plat 
niveau: débutant 
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 40 min
layout: recette
---

# Pour la pâte brisée :

250 g de farine T45

140 g de beurre à température (sortir 1 heure avant)

25 g de poudre d’amandes

75 g de sucre glace

2 g de sel

1 oeuf

Travaillez le beurre pommade avec le sucre glace, ajoutez le sel, la poudre d'amandes, puis incorporez l'oeuf. 

Enfin, ajoutez la farine tamisée et mélangez de façon à obtenir une pâte homogène sans trop la travailler.

Filmez et bloquez au réfrigérateur jusqu'à ce que la pâte aie bien durcie.

Foncez votre moule à tarte et enfournez pour environ 25 minutes à 175°C

## Pour le flan :

 450 g de patates douces 

3 oeufs

10 cl de crème de coco 

10 cl de crème liquide 

100 g de sucre de canne 

1 gousse de vanille 

...............................................

Lavez les patates douces puis faites les cuire pendant 10 minutes au cuiseur vapeur.

Une fois cuites, épluchez-les et mettez-les dans le bol d'un mixeur. Ajoutez au bol la crème liquide, les oeufs, le lait de coco, le sucre. Faites tourner la préparation jusqu'à ce qu'elle soit bien homogène. 

Abaissez votre pâte dans un moule à tarte. Versez la préparation par dessus et enfournez à 160 degrés pendant 30 minutes. 

Laissez refroidir avant de déguster. 

### Pour le coulis d’orange: 

Le jus de 4 oranges 

1 c à soupe de miel 

2 graines de cardamome 

...............................................

Faire réduire votre jus, avec la cardamome et le miel dans une casserole à petit frémissement jusqu’à ce que sa consistance devienne sirupeuse. Laissez refroidir, votre sirop est prêt. 

#### Pour le dressage: 

Mettez 1 cuillère à soupe de coulis d'orange  dans une assiette et disposez un part de flan, puis arrosez là d'une cuillère de coulis d'orange à nouveau. 

