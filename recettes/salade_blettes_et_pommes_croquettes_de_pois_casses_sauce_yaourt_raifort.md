---
author: Stéphanie Labadie
description: Salade de blettes et pommes, croquettes de pois cassés, sauce au yaourt raifort
title: Salade de blettes et pommes, croquettes de pois cassés, sauce au yaourt raifort
licence: CC-by-sa
categories: plat
niveau: moyen
date: 16/01/2022
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade_blettes_et_pommes_croquettes_de_pois_cassées_sauce_yaourt_raifort.jpeg
nombre: 6 personnes
temps-cuisson: 40 à 50 min
temps-preparation: 20 min
layout: recette
---

# Pour les croquettes de pois cassés:

* 300 g de pois cassés 
* 1 bouquet garni 
* 1 blanc de poireau 
* 1 carotte 
* 2 oignons rouges ou blonds
* 1 c à soupe de sucre roux 
* 1 pointe de cumin 
* 1 à 2 c soupe de farine de riz 
* 4 c à soupe d’huile d’olive ou de courge 
* 1/2 botte de coriandre
* 1 pomme râpée
* 80 g de chapelure et/ou flocons d’avoine

........................................................

Faites cuire les pois cassés dans un litre de bouillon avec le bouquet garni pendant 30 minutes, jusqu’à ce que les pois casés soient cuits mais encore fermes. Egouttez et mixer les pois cassés en fine purée. 

Pendant que les pois cassés cuisent, coupez les légumes en petits morceaux et faites les cuire dans une poêle avec une bonne noisette de beurre, sucre, sel et poivre et une pointe de cumin. 

Réservez et laissez refroidir. 

Dans un saladier, mélangez la purée de pois cassés avec les légumes cuits, la farine de riz, la coriandre, la pomme râpée et l’huile d’olive. 

Formez des croquettes pas trop grosses, panez les dans la chapelure et faites les dorer dans du beurre à la poêle. 

Vous pouvez aussi les enfourner à 180 degrés pendant 20 minutes, en les arrosant avant d’un généreux filet d’huile d’olive. 

## Pour la salade :

* 500 g de blettes 
* 1 c à soupe de soja 
* 2 gousses d’ail
* 1 c à soupe de miel 
* 1 pommes
* 1 poignée de cranberries 
* 1 poignée de noix 
* 1/2 citron 

........................................................

Dans une casserole, faites cuire les blettes dans une grosse noisette de beurre avec les blettes coupées grossièrement, feuilles et tiges dénervées, puis ajoutez le jus de clémentines, le soja, le miel, salez et laissez cuire jusqu’à évaporation. 

Pendant ce temps taillez les pommes en de fines allumettes. Arrosez les d’un filet de jus de citron pour qu’elles ne noircissent pas. Réservez à part. 

### Pour la sauce yourt raifort :

20 cl de crème fraîche ( ou de yaourt pour une version plus light) 

1/2 c à café de raifort 

1 c à soupe de vinaigre balsamique 

1 c à café de sucre ou de sirop d’agave

1 pincée de sel 

1/2 botte de ciboulette ou coriandre ciselée

........................................................

Mélangez tous les ingrédients et réservez au frais.

#### Pour le dressage :

Mélangez les blettes, les pommes, les cranberries, les noix et la crème de raifort. 

Gardez un peu de sauce au yaourt. 

Disposez par dessus les croquettes, à nouveau un peu de crème de raifort, puis quelques feuilles de coriandre. A table!!! 

**Astuces**: 

Vous pouvez préparer les croquettes seules, avec une purée, une semoule, des légumes rôtis Idem pour la salade que vous pouvez préparer indépendamment des croquettes, avec la même sauce ou une autre! 

