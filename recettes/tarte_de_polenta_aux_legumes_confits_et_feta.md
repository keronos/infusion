---
author: Stéphanie Labadie
description: Tarte de polenta aux légumes confits et fêta  
title: Tarte de polenta aux légumes confits et fêta   
licence: CC-by-sa
categories: plat
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tarte_de_polenta_aux_legumes_confits_et_feta.jpeg
layout: recette
---

# Base pour pâte à la polenta :

- 1 grande tasse de polenta 
- 4 grandes tasses de liquide ( moitié lait/moitié eau) / bouillon cube de légumes )
- 50 g de parmesan 

  ...............................

Une fois le liquide arrivé à ébullition, mélanger avec la polenta au fouet et laisser cuire doucement, le temps qu’elle gonfle. Ajouter le parmesan et quelques herbes fraîches qui traineraient dans le frigo, ou des olives ou des tomates séchées etc… et tapisser le fond d’un moule à tarte de cette préparation. 

Verser un filet d’huile d’olive épar dessus et mettre à griller au four pendant 20 minutes à 180 degrés . 

Votre fond de tarte est prêt! 

## Pour les légumes rôtis : 

- patates douce, potimarrin, oignons rouges, carottes, navets ... l'hiver 
- tomates cerises, courgettes, oignons, aubergines, fenouil..  l'été 

Coupez grossièrement les légumes et mettez les à rôtir sur une plaque à 180 degrés avec 3 gousses d'ail écrasées, 1 c à soupe de sucre, 2 pincées de sel, l'épice de votre choix, de la sauge du thym ou du romarin, et un bon filet d'huile d'olive. 

### Dressage : 

Une fois cuits, disposez les sur le fond de tarte à la polenta, avec de la feta émiettée, et quelques graines de tournesol, refaites griller 10 minutes à 180 degrés, et c'est prêt! 

