---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/magret_de_canard_sauce_clementine_gingembre_cannelle_puree_patates_douces.jpeg
author: Stéphanie Labadie
description: Magret de canard sauce clémentine, gingembre, cannelle, purée de patates douces rôties  
title: Magret de canard sauce clémentine, gingembre, cannelle, purée de patates douces rôties  
licence: CC-by-sa
categories: plat
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 40 à 50 min
temps-preparation: 30 min
layout: recette
---

# Pour le canard et 4 personnes :

* 2 magrets de canards 
* le jus de 500 g de clémentines 
* 1 citron 
* 10 g de miel 
* 10 g de gingembre 
* 1 gousse d’ail écrasée 
* 1/2 c à café de cannelle 
* 1 c à soupe de vinaigre baslamique 
* Sel et poivre 

**Pour la marinade :** 

Prélever les zestes et presser les clémentines et le citron, râper le gingembre et l’ajouter aux jus de fruit avec le miel, et la cannelle. saler et poivrer.

Faire réduire cette marinade dans une casserole jusqu'à ce qu'elle obtienne une consistance nappante. 

**Pour la cuisson du magret :** 

Dans une poêle bien chaude, déposez les magrets côté peau pendant 4 mn, retournez les et saisir le dessous des magrets.

Mettez les ensuite dans le four à 180°c pendant 10 à 15 mn selon la taille des magrets. Laissez les reposer 5 à 10 mn avant de les repasser au four 5 mn.

## Pour la purée de patates douces au romarin: 

300 g de pommes de terre  

500 g de patates douces 

4 gousses d’ail écrasées 

Huile d’olive 

Sel et poivre 

Branches de romarin 

70 g de beurre fondu ( voire noisette )

10 cl de crème liquide

Feuilles de sauge 

..........................................

Préchauffez le four à 180 degrés. 

Pelez les pommes de terre et les patates douces, coupez les en cubes de 3 cm de côté. Disposez les dans deux plats différents, chacune ayant son propre temps de cuisson. 

Versez un filet généreux d’huile d’olive, salez, mélangez avec le romarin et la sauge et les gousses d’ail écrasées.  Enfournez pour 25 à 30 minutes. 

A la sortie du four, écrasez les deux variétés de pommes de terre ensemble avec le beurre, la crème et le sel / poivre. Rectifiez l’assaisonnement si besoin. 

### Dressage: 

Disposez une cuillère de sauce, vos tranches de magret par dessus, et une autre cuillère de sauce, avec une portion de purée à côté. 

