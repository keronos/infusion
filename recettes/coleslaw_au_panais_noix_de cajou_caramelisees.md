---
author: Stéphanie Labadie
description: Coleslaw au panais, noix de cajou caramélisées 
title: Coleslaw au panais, noix de cajou caramélisées
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/coleslaw_au_panais_noix_de_cajou_caramelisees.jpeg
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson:  10 min
temps-preparation: 30  min
layout: recette
---

## Pour les noix de cajou :

* 2 c soupe de sucre brun 
* 2 c à café d’huile d’olive
* 3/4 de c à café de curcuma moulu 
* 200 g de noix de cajou grillées et salées 
* 2 c à café de graines de cumin 
* 1 pincée de sel 

Préparez les noix de cajou. Mettez dans une cette casserole le sucre, l’huile, le curcuma, et 2 c à soupe d’eau. 

Portez à ébullition en remuant souvent et ajoutez les noix de cajou et le cumin. Laissez cuire 3 à 4 minutes en remuant sans cesse jusqu’à ce que les graines et les noix de cajou soient enveloppées d’un glaçage collant. 

Etalez sur une plaque de cuisson en les répartissant bien, saupoudrez de la pincée de sel, et enfournez pour 15 minutes à 180 degrés jusqu’à ce qu’elles soient dorées. Laissez refroidir. 

## Pour la vinaigrette :

* le jus de 2 citron verts
* 2 c à soupe de miel ( ou 3 c à soupe de sirop d’érable) 
* 1 c à café de moutarde ancienne  
* 5 c à soupe d’huile d’olive 

Diluez le sel dans le jus d citron et émulsionner avec les autres ingrédients jusqu’à obtenir une vinaigrette nappante.

### Pour la salade :

* 1/2 chou blanc 
* 1 à 2 panais râpé 
* 2 carottes taillées en julienne 
* 1 petit oignon rouge finement émincé 
* 1 poignée de cranberries 

Dans un saladier, mélangez le chou blanc émincé finement à la mandoline, le panais râpé, les carottes, l’oignon rouge, les cranberries, puis ajoutez la sauce. Mélangez bien et laissez reposer pour qu’elle imprègne bien les légumes.

Puis, au moment de servir, incorporez la coriandre, puis transférer sur un plat de service. 

Saupoudrez d’une généreuse poignée de noix de cajou, et servir le reste à part dans un bol, pour accompagner ou pour la gourmandise ! 