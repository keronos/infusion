---
author: Bénédicte Lambert
description: des crocodiles ou des nounours, 100% naturels!
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/bonbons-agar-agar.jpg
title: Bonbons à l'agar-agar
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 30/01/2019
nombre: 50 bonbons
temps-cuisson: 4 min
temps-preparation: 5 min
layout: recette
---

## Ingrédients

- 200g de jus de fruits
- 300g de miel
- 35g de fécule
- 5g d'agar agar

## Déroulé

1. Mélanger les ingrédients.
2. Faire bouillir 4 min.
3. Laisser refroidir puis couler dans les moules ou sur une plaque. Laisser refroidir puis rouler dans le sucre.

**Coûts**

- fruits = 1€
- miel = 6,15€
- fécule = 0,43€
- agar-agar = 0,50€

coût total = 8€

coût pièce= 0,16€
