---
author: Stéphanie Labadie
description: Frites de polenta aux olives et ketchup maison
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/frites_de_polenta_aux_olives_et_ketchup_maison.jpeg
title: Frites de polenta
licence: CC-by-Sa
categories: plat
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 20 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

### Pour les frites de polenta :
* 50 g de polenta
* 100 g d’eau et 100 g de lait à bouillir avec un bouillon cube et une pincée de gros sel
* 1 poignée d’olives noires dénoyautées et grossièrement hachées
* 50 g de parmesan

### Pour le ketchup :
* 150 g de de concentré de tomates
* 2 càs de sucre de canne roux
* 2 à 3 càs de vinaigre de cidre
* 100 à 150 ml d’eau (et plus si nécessaire)
* Sel


## Déroulé

Faites bouillir l’eau, le lait et le bouillon cube. Baissez le feu et ajoutez la polenta, mélangez
avec un fouet pendant que la polenta cuit progressivement.

Ajoutez le parmesan en fin de cuisson, et étaler le mélange dans un plat carré ou
rectangulaire, de sorte que votre polenta arrive à 2 cm de hauteur.

Laissez la polenta refroidir et quand elle est bien solidifiée, coupez des bâtonnets de polenta.

Disposez les sur une plaque de cuisson, badigeonnez au besoin vos frites de polenta avec un
pinceau et de l’huile d’olive, saupoudrez de bleu de sel, et enfournez 20 à 25 minutes à 170
degrés.

A déguster avec une sauce ketchup maison, du tzatziki ou tout simplement, au naturel !

### Ketchup

Dans un poêlon, déposer le concentré de tomate, l’eau, le sucre et le vinaigre.

Cuire dix minutes à petits bouillons, saler. Remuer régulièrement.

Si le ketchup devient trop dense, rajouter un peu d’eau. Goûter et rectifier l’assaisonnement en
sucre, sel , vinaigre. Il faut un bon équilibre entre les saveurs. On peut agrémenter ce ketchup
d’épices, comme piment fumé, paprika, curry etc.