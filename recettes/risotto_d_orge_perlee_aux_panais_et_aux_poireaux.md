---
author: Stéphanie Labadie
description: Risotto d'orge perlée aux panais et aux poireaux, crême de parmesan 
title: Risotto d'orge perlée aux panais et aux poireaux, crême de parmesan 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 30 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/risotto_d_orge_perlee_aux_panais_et_aux_poireaux_creme_de_parmesan.jpeg
---

# Ingrédients: 

* 300 g d'orge perlée trempée la veille dans un grand volume d’eau 
* 1 grosse échalote ciselée 
* 1 petite betterave crue ou cuite
* 1 gros panais coupé en brunoise ( petits cubes) 
* 1 litre de bouillon de légumes ou de volaille chaud 
* 50 g de parmesan 
* 15 cl de crème 
* 10 cl de vin blanc 
* 2 petits poireaux 
* Le jeu de 2 clémentines 
* 2 c à soupe de soja sucré 
* 30 g de beurre 

## Déroulé : 

Lancer le bouillon de légumes et le garder chaud. 

Laver et couper les poireaux en deux dans le sens de la longueur, puis les tailler en biseau. Faire rissoler la moitié du beurre dans une poêle, et y faire cuire les poireaux sur la face intérieure. Couvrir et laisser cuire 5 minutes à feu doux. Une fois grillés de ce côté, ajouter le jus de clémentines et le soja, puis terminer la cuisson. Réserver au chaud. 

Dans une cocotte, faire chauffer l’autre moitié du beurre et une cuillère à soupe d’huile d’olive. Ajouter l’ échalote émincée et laisser cuire doucement pendant 5 minutes.

A feu vif, ajouter le panais et l'orge perlée, et mélanger pendant 2 minutes. Saler peu, poivrer et mouiller avec le vin blanc et laisser évaporer en remuant. 

Ajouter une louche de bouillon et poursuivez la cuisson à feu moyen pendant 20 à 30 minutes (selon que vous aimiez al dente ou non!) en ajoutant le bouillon au fur et à mesure de son absorption. Coupez le feu et ajoutez le parmesan râpé et la crème en mélangeant rapidement. 

Rectifier l’assaisonnement si besoin. Laissez reposer 5 minutes.

Servir dans des assiettes creuses, en disposant le risotto d’orge aux panais, puis le poireaux par dessus, avec son jus de cuisson. 

Peaufiner le dressage avec un peu de parmesan râpé, quelques noisettes torréfiées concassées, un peu de chapelure grillée, 1 pincée d’épice de votre choix, ou quelques feuilles de coriandre… ou tout à la fois!!!  



