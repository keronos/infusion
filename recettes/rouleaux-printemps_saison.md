---
author: Stéphanie Labadie
description: faire des rouleaux de printemps soi-même c'est facile
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/rouleaux-printemps.jpg
title: Rouleaux de printemps de saison
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 22/10/2021
nombre: 4 personnes
temps-cuisson: 5 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

### Pour les 10 rouleaux de printemps :
* 10 feuilles de riz
* 100 g de tofu
* 1 c à soupe de noix de coco râpée
* 2 carottes râpées avec du jus de citron et du sel
* 1 radis noir
* 1 pomme taillée en allumettes
* 1/2 chou ciselé ( mélangé avec ce mélange : 1 c à café de miso et 1 c l de jus de pomme)
* 1 concombre coupé en bâtonnets ( ou en pickles : 1 mesure de sucre, 2 de vinaigre et 3 d’eau, faites bouillir, versez sur vos pickles et laissez refroidir )
* 1 poignée de noix de cajou hachée
* 1/2 botte de coriandre
* des feuilles de menthe
* facultatif: des vermicelles chinois

### Pour le tofu : 

* Couper le en fines lamelles, faites revenir avec 1 gousse d’ail hâchée, 1 c à soupe de noix
de coco râpée, 1/2 c à café de l’épice de votre choix ( curry par exemple ) et un filet de jus de citron.
* Réservez.

### Pour la sauce :

* 1 c à soupe de miso blanc
* 1 c à soupe de soja
* 2 c à soupe de vinaigre de riz
* 1 c à café de sucre
* Le jus d’1/2 orange

## Déroulé

Tremper les feuilles de riz dans de l’eau tiède. Quand elles se ramolissent, les disposez sur un plan de
travail et disposer dessus les ingrédients souhaités sur un rectangle. 

Puis roulez le rouleau comme un boudin et rabattant les bord vers l’intérieur. 

Réservez au frais dans du cellophane.
