---
author: Bénédicte Lambert
description: pain de mie sans gluten à faire soi-même.
title: Pain de mie sans gluten
licence: CC-By-0
categories: accompagnement
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/painDeMie-sans-gluten.jpg
niveau: débutant
date: 12/05/2018
nombre: 2 personnes
temps-cuisson: 60 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

- 400g de mélange de farine sans gluten
- 275ml de lait végétal
- 80ml d'eau
- 25ml d'huile
- 10g de levure
- 5g de gomme de guar
- 1cc de sucre (coco ou autre)
- 2CS de graines (lin, tournesol...)
- sel

## Déroulé

1. Mélanger les farines s'il y en a plusieurs.
2. Faire chauffer le lait et l'eau.
3. Mélanger les ingrédients petit à petit.
4. Faire reposer 1h à température ambiante.
5. Dégazer la pâte et laisser à nouveau reposer dans le moule légèrement huilé.
6. Badigeonner de jaune d'oeuf.
7. Enfourner pour 30 min à 210°C.
