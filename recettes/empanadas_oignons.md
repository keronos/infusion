---
author: Stéphanie Labadie
description: Empanadas aux oignons rissolés, olives noires, pruneaux et fêta
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/masterhttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/empanadas_oignons.jpg
title: Empanadas 
licence: CC-by-Sa
categories: entrée
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

Pour 10 petites empanadas :

### Pour la pâte:

* 250 g de farine
* 125 g de beurre
* 1 jaune d’oeuf
* 1 c à café de cumin/cannelle/ curry , au choix!

### Pour la farce

- 2 oignons blancs
- 1 c à soupe de sucre
- 1 poignée d’olives noires grossièrement hachées
- 5 pruneaux grossièrement émincés
- 80 g de feta
- 1 citron

## Déroulé

Faites suer les oignons finement émincés dans une poêlé avec une grosse noisette de beurre, et une
cuillère de sucre, laissez les confire, puis sortir du feu et réservez. Mélanger aux olives et aux
pruneaux, rajoutez un filet de jus de citron si besoin.

Pré chauffez le four à 180 degrés.

Pour la confection des empanadas, étalez la pâtes en petits cercle de10 cm de diamètres.

Déposez un peu du mélange oignons olives pruneaux et émiettez un peu de fêta par dessus, puis
rabattez le cercle sur lui même pour former une demie lune.

Enfournez les empanadas 25 minutes, puis laissez tiédir avant de déguster!