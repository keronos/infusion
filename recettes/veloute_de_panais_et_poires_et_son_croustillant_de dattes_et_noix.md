---
author: Stéphanie Labadie
description: Velouté de panais et poires, et son croutsillant de dattes et noix 
title: Velouté de panais et poires, et son croutsillant de dattes et noix 
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 20 min
temps-preparation: 15 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/Veloute_de_panais_et_poires_et_son_croustillant_de_dattes_et_noix.jpeg
layout: recette
---

# Pour la soupe :

* 3 beaux panais 
* 1 oignon 
* 1 grosse pomme de terre 
* 1 filet d’huile d’olive 
* 10 cl de crême liquide
* 2 poires 
* Fleur de sel et poivre 

Dans une casserole, faire dorer l’oignon émincé dans un filet d’huile d’olive puis ajouter les panais coupés tranches de 3 cm à peu près. Faire rissoler les légumes pendant 5 minutes, puis recouvrer d’eau ou de bouillon. 

Faire cuire pendant 20 à 30 minutes, Ajouter ensuite les poires épluchées et la crême, mixer dans un blender ou d’un mixeur plongeant jusqu’à obtenir une texture bien lisse. 

## Pour le croustillant de dattes et noisettes :

* Une poignée de dattes 
* Une poignée de noisettes grossièrement concassées 

Dans une poêle, faire revenir les dattes découpées en 4 avec les noisettes, et faire caraméliser le tout avec 1 c à soupe d’huile d’olive. Réserver. 

### Dressage :

Servir le velouté dans des bols avec le croustillant de dattes et de noisettes par dessus, et une pincée de massalé, un peu de coriandre ciselée ou un filet d’huile de noisettes. Et … bon appétit ! 