---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/crumble_de_potimarron_et_patates_douces_a_la_creme_de_cajou.jpeg
author: Stéphanie Labadie
description: Crumble de potimarron et de patates douces à la crême de cajou 
title: Crumble de potimarron et de patates douces à la crême de cajou
licence: CC-by-sa
categories: plat
niveau: débutant
Date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 40 à 50 min
temps-preparation: 30 min
layout: recette
---

# Pour les légumes: 

* 1 petit potimarron 
* 250 g de patates douces 
* 3 oignons 
* 2 c à soupe de beurre de coco
* 2 c à soupe de purée de cajou 
* 20 cl d'eau
* 5 gousse d’ail écrasées
* Huile d’olive 
* Épices type massalé, ou cannelle ou coriandre 

## Pour la pâte : 

* 70 g de beurre 
* 70 de farine semi complète 
* 70 de poudre d’amande, ou de farine d’épeautre ou de sarrasin ( ou celle que vous avez sous la main!) 
* 2 pincée de sel 

Confectionnez la pâte en effritant le beurre et les farine, avec le sel et des épices. Vous devez obtenir un mélange sableux.

### Déroulé : 

Coupez les potimarrons et les patates douces en morceaux de 3 cm de côté, peler et écraser 3 gousses d’ail, et enrober le tout de 3 c à soupe d’huile d’olive, de fleur de sel, d’1 c à café d’épice. Mettre à rôtir au four pendant 30 minutes à 180 degrés. 

Pendant ce temps, émincez les oignons et les faire rissoler dans 1 c à soupe de beurre de coco. Salez et ajoutez une cuillère à café de miel, et laisser cuire jusqu’à ce que oignons soient transparents et fondants. Réservez. 

Dans un saladier, mélangez la purée de cajou avec l'eau, du sel, un c à café d’épices, et un gousse d’ail écrasée. Mélangez au fouet.

Quand les légumes sont fondants, sortez le plat du four.

Ajoutez les oignons cuits aux légumes rôtis, puis versez la préparation de cajou par dessus, mélangez. 

Saupoudrez par dessus de la pâte à crumble. 

Remettre au four et laissez cuire encore 20 minutes à 160 degrés. 

**Astuce**: 

Ce crumble se mange avec une bonne salade, et peut aussi s’agrémenter des légumes que vous avez sous la main : des poireaux, des carottes, des navets etc… ou se compltéer avec du boulgour du petit épeautre ou des lentilles dans le fond du plat à gratin pour un plat plus complet nutritionnellement. 