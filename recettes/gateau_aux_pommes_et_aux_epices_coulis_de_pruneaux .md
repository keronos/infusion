---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gateau_aux_pommes_et_aux_epices_coulis_de_pruneaux.jpeg
author: Stéphanie Labadie
description: Gateau aux pommes et aux épices, coulis de pruneaux    
title: Gateau aux pommes et aux épices, coulis de pruneaux  
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
---

# Pour le coulis : 

* 150 g de pruneaux 
* 20 cl de jus de pommes 

.......................................

Faites bouillir le jus de pommes et versez le sur les pruneaux. Couvrez puis 15 minutes après, passez au mixeur pour obtenir un coulis nappant ( n'utilisez pas forécment tout le jus de pomme, selon la texture souhaitée, nappante ou liquide). 

 ## Pour la garniture aux pommes : 

80 g de cassonade 

1 c à soupe de mélange 4 épices 

3 grosses pommes sucrées et acides 

.......................................

Dans un bol, mélangez les pommes coupées en quartiers de 1,5cm de large avec le 4 épices et le sucre. 

### Pour le gâteau : 

130 g de beurre doux à température ambiante coupé en cubes

150 g de sucre semoule 

3 gros oeufs 

1 gousse de vanille 

300 g de farine d’épeautre ( ou de blé ) 

1 c à café de levure 

1/2 c à café de sel 

200 g de crème aigre ou fraîche 

.......................................

Préchauffez le four à 150 degrés. Beurrez un moule. 

Dans un grand saladier, battre au fouet le beurre et le sucre. 

Ajoutez la vanille et les oeufs. 

Puis la farine, la levure et le sel. 

Terminez avec la crême. Dés que vous obtenez un appareil homogène, versez le dans le moule. 

Déposez les pommes au sucre sur la pâte. 

Enfournez pour 40 minutes approximativement, jusqu’à ce que le gâteau soit doré, ferme et croustillant sur le dessus. 

Vous pouvez secouer le moule et stopper la cuisson dés que le gâteau cesse de trembloter en surface. 

Sortez du four et laisser le tiédir environ 30 minutes. 

#### Dressage : 

Dans chaque assiette, versez un fond de coulis avant de déposer une part de gâteau. 

Vous pouvez aussi troquer le coulis de pruneaux contre un coulis d'abricots secs ( même technique de fabrication), ou un caramel au beurre salé... c'est possible aussi! 