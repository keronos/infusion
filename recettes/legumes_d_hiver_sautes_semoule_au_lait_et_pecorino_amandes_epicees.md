---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/legumes_d_hiver_sautes_semoule_au_lait_et_pecorino_amandes_epicees.jpeg
author: Stéphanie Labadie
description:  Légumes d'hiver sautés, semoule au lait et pecorino, amandes épicées 
title: Légumes d'hiver sautés, semoule au lait et pecorino, amandes épicées 
licence: CC-by-sa
categories: plat
niveau: débutant 
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 30 min
layout: recette
---

# Pour les légumes et les amandes : 

300 g de choux de Bruxelles 

2 carottes taillées en allumettes (ou tous légumes que vous aurez sous la main)

2 clémentines ou 1 orange 

2 gousses d’ail  

optionnel: 4 feuilles de sauge, un branche de romarin etc.. 

1 c à soupe de sucre de canne ( ou de sirop d’érable, ou d’agave ) 

1 pincée de sel 

Et tous les légumes que vous aurez sous la main 

...............................................

Faites chauffer votre poele avec une grosse noisette de beurre, ajoutez les carottes, faites saisir et ajoutez le jus des clémentines ( en garder un peu en réserve au cas où), le sucre, le sel, les feuilles de sauge, le romarin etc… couvrir 5 à 10 minutes, puis ajoutez les choux de Brucelles taillés en fines rondelles.  

Couvrez à nouveau, remettez un peu de jus de clémentine et ouvrez à nouveau 5 à 10 minutes. Vos légumes doivent avoir gardé un peu de croquant, et le jus avoir été totalement absorbé ( si besoin terminez la cuisson sans couvercle). Réservez. 

Dans une autre poele, faire rissoler une bonne noisette de beurre, et faites dorer vos amandes effilées, avec une cuillère à café de paprika (ou de votre épice préféré) et une pincée de sel. Laissez dorer puis réservez. 

## Pour la semoule au lait: 

600 g de lait entier 

1 bouillon cube 

200 g de semoule 

80 g de pecorino 

...............................................

Dans une casserole de taille moyenne, mettez le lait à chauffer avec 60 cl d’eau et 3/4 de c à café de sel, 1 boisson cube, et du poivre si besoin. 

A ébullition, ajoutez la semoule, et fouettez sans discontinuer pendant 3 à 4 minutes, jusqu’à obtenir l’onctuosité d’un porridge. Retirez du feu et incorporez 80 g de pecorino. 

Répartissez la semoule au lait dans des coupelles, dresser les légumes part dessus, parsemez d’amandes au épices, et dégustez! 

**Astuce**: 

Vous pouvez varier les gouts et les couleurs avec les légume de votre choix pour accompagner cette semoule. 