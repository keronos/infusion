---
author: Stéphanie Labadie
description: Gâteau aux agrumes et sa compote d’abricots minute
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gateau_amandes.jpeg
title: Gâteau aux agrumes et sa compote d’abricots minute
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 12/10/2021
nombre: 6 personnes
temps-cuisson: 60 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients
* 2 oranges bios
* 6 oeufs
* 170 g de sucre de canne complet ou moitié complet et moitié blond
* 250 g de poudre d'amande
* 2 càc de poudre à lever
* 1/2 càc sel fin

Pour le streusel en option : 25 g de farine / 15 g de flocons d'avoine / 50 g de sucre / 50 g de beurre
froid / 25 g de noisettes ou noix de pécan

### Pour la comptée d’abricots
* 200 g d’abricots secs
* 10 cl de jus de pomme

## Déroulé

La veille ou au moins 2h avant de faire le gâteau, cuisez vos agrumes entiers avec leur peau dans une
casserole d'eau pendant 1h30 puis égouttez-les et enlevez les pépins.

Préchauffez votre four à 180°.

Préparez le streusel : mixer tous les ingrédients et placez au frais.

Mixez les agrumes entiers (inutile de laver le mixer de la préparation streusel précédente)

Fouettez les oeufs entiers avec le sucre, ajoutez la poudre d'amande et la poudre à lever, la pincée de
sel, mélangez afin d'obtenir une pâte homogène. Versez la préparation dans deux petits moules
(18cm) beurrés ou un grand moule à manqué de 26cm et parsemez du Streusel.

Enfournez pour 1h jusque la lame d'un couteau ressorte sèche. Dégustez tiède ou froid pour le
goûter.

Faire chauffer le jus de pommes, le verser sur les abricots secs. 
Couvrir et attendre le refroidissement de cette préparation. 

Mixer grossièrement en enlevant un peu de jus de pomme si besoin.

Servir cette tartinade avec le gâteau aux agrumes, accompagnés pourquoi pas d’une grosse cuillère de crème fraiche?!

/// ASTUCE :

Cette tartinade peut être servie sur une fromage blanc, un yaourt, avec une compote, ou sur les
tartines du petit déjeuner tout simplement !