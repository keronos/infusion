---
author: Bénédicte Lambert
description: de l'énergie pour la parade
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/energyball-carnaval.jpg
title: Energy balls du carnaval
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 11/01/2019
nombre: 50 energy balls
temps-cuisson: 0 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

- 150g de graines de sarrasin
- 120g d'oléagineux (noix, noisettes, amandes, pistaches, courge, tournesol)
- 60g de pruneaux
- 120g de pâte de figues
- 8cc d'eau
- 90g de purée d'amandes
  enrobage
- Cacao
- Noix de coco
- sésame noir et blanc
- pistaches
- fleurs séchées

## Déroulé

1. Écraser grossièrement les graines de sarrasin.
2. Couper en morceaux les oléagineux.
3. Couper les pruneaux en petits morceaux.
4. Mixer la pâte de figues avec l'eau.
5. Mélanger tous les ingrédients en malaxant avec les mains.
6. Faire des boules et rouler dans le produit d'enrobage au choix.

**Coûts**

- sarrasin = 0,95€
- oléagineux = 3,6€
- purée d'amandes = 2,77€
- enrobage = 0,66€

coût total = 7,98€

coût pièce = 0,16€
