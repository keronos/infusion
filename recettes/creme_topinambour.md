---
author: Stéphanie Labadie
description: Crème de topinambours aux poireaux braisés et aux céréales torréfiées
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/creme_topinambours.jpeg
title: Crème de topinambours
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 15/10/2021
nombre: 4 personnes
temps-cuisson: 40 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

* 800 g. de topinanbours
* 2 c à soupe de crème d’amande
* 1/4 cc de noix de muscade râpée
* Sel et poivre
* 2 poireaux
* 2 c à soupe de soja
* 2 c à soupe de sirop d’érable
* 1 grosse noisette de beurre + huile d’olive
* 1 cs. de graine de courge
* 1 cs. de graines de tournesol
* 1 cs. d’amande effilées
* 1 cc. de piment en flocons

### Déroulé

Pour la crème :

Pelez les topinambours et coupez-les grossièrement

Portez un grand volume d’eau salée à ébullition et plongez-y les topinambours pour 20 minutes de cuisson

Egouttez-les et mixe-les finement avec la crème d’amande et la muscade. Assaisonnez à votre convenance

Pour les poireaux :

Rincez vos poireaux, et les couper en biseau, par tronçons de 10 cm. Coupez les au milieu.

Faites chauffer une poêle, laisser rissoler la noisette de beurre, et disposez vos moitié de poireaux côté centre. Laisser griller, baisser un peu le feu, ajouter 2 cl de jus de pomme (ou d’eau), et couvrir pendant 5 minutes de cuisson.

Arrivés en fin de cuisson, levez le couvercle, retournez vos poireaux, ajoutez le soja et le sirop d’érable, laissez évaporez, et réservez.

Pour les graines :

Fais griller les graines et les amandes avec le piment dans une poêle chaude sans matière grasse pour 2-3 minutes.

Servir la crême dans des assiettes creuses avec les poireaux, quelques graine torréfiés (vous garderez
le reste dans un pot hermétique, idéal pour vos salades!), et pourquoi pas quelques cranberrries ou
lamelles d’abricots secs.

Bon appétit!