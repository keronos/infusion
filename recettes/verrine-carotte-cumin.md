---
author: Bénédicte Lambert
description: une verrine simple et fraîche
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/carottes-cumin.jpg
title: Verrine carottes cumin
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 12/05/2017
nombre: 4 personnes
temps-cuisson: 30 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

- 1/2 oignon
- 5 petites carottes botte
- 1 cuiller à café de cumin entier
- 1 cuiller à soupe d’huile d’olive
- sel, poivre

_pour la déco_:

- chips de jambon
- tomates cerises
- morceaux de pistaches

## Déroulé

1. Faire revenir l’oignon émincé puis les carottes coupées en rondelles dans l’huile.
2. Ajouter les grains de cumin et faire revenir pour que leur parfum se révèle en chauffant.
3. Couvrir d’eau et laisser cuire 20 à 30 minutes, piquer les carottes pour vérifier leur cuisson.
4. Mixer finement la préparation en veillant à ce que le mélange ne soit pas trop liquide.
5. Saler, poivrer.
6. Servir froid avec les éléments de décoration.
