---
author: Stéphanie Labadie
description: Chocolat liégeois vegan et froid et sa glace bananes minute
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/pamplemousses-confits.jpg
title: Chocolat liégeois vegan
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 12/10/2021
nombre: 4 personnes
temps-cuisson: 40 min
temps-preparation: 14 min
layout: recette
---

## Ingrédients

### Pour 4 verres

* 15 cl de lait coco
* 1 l de lait d’amandes
* 4 c à soupe de caco cru en poudre
* 4 c à soupe de sirop d’agave
* 6 c à soupe de farine de coco

### Pour la glace bananes minute :

* 300 g de bananes bien mûres
* 85 g de purée de noisettes
* 3 c à soupe de cacao en poudre

## Déroulé

### Pour le chocolat liégois

La veille mettez le lait de coco au frigidaire.

Prélevez un peu de la crème qui aura figée pour monter une chantilly de coco au batteur électrique.

Mixer tous les ingrédients au blender. Servir accompagnée chantilly de coco, de fruits frais ou de
copeaux de chocolat pour un milk shake encore plus gourmand!

### Pour la glace banane

La veille, couper la banane en rondelle et les mettre à congeler.

Le jour même, placer les morceaux de bananes dans le bol du mixeur, et mélanger avec la purée de
noisettes et le cacao en plusieurs fois jusqu’à obtenir une crême homogène. Déguster sans tarder.

On peut accompagner ce dessert de chocolat fondu, d’une crême chantilly, de copeaux de chocolat
blanc, servis sur des petites gaufres à déguster pendant le gouter.