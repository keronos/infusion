---
author: Stéphanie Labadie
description: Tartinades de fête anti- gaspi
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tartinade_fete.jpg
title: Tartinades de fête
licence: CC-by-sa
categories: plat
niveau: débutant
date: 26/10/2021
nombre: 4 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

### Crème aux côtes et feuilles de chou fleur
* les côtes et feuilles d’un chou fleur
* 2 quartier de potimarron
* 1 gousse d’ail
* 1 filet d’huile d’olive
* 2 c à soupe de fromage de chèvre frais
* Fleur de sel

### Sauce vierge d’hiver
- 1 orange
- 1 petite échalotes ciselée
- 1/2 pomme
- 1/2 branche de fenouil
- 30 g d’olives noires
- 1/2 botte de coriandre
- Quelques feuilles de menthe
- Le jus d’un demi citron
- 10 cl d’huile d’olive
- 1 pincée de sel

## Déroulé

Bien laver les légumes.

Couper grossièrement les côtes de chou fleur, ajouter les cubes de potimarron et la gousse d’ail. Faire cuire au cuit vapeur 15 minutes. Mixer au blender, avec le fromage frais, l’huile d’olive et le sel. Et pourquoi pas une pincée de curry ou de paprika fumé?!

A déguster sur une tartine de pain avec des herbes fraîches ciselées, des baies roses concassées, un filet d’huile d’olives, des tomates séchées émincées etc… Ou la sauce vierge d’hiver !

Lever les suprêmes d’orange, et les couper finement comme les olives, l’échalote, les pommes et le
fenouil, en tout petits cubes.

Juste avant de déguster, mélanger dans un bol tout ces ingrédients avec l’huile d’olive, le jus de citron et le sel.

A servir sur un poisson, une viande blanche, des légumes rôtis etc… c’est délicieux!