---
author: Stéphanie Labadie
description: Barre de céréales aux pommes, abricots et noix de coco
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/barre_cereales.jpg
title: Barre de céréales aux pommes
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 11/11/2021
nombre: 12 barres
temps-cuisson: 0 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

* 130 g de flocons d’avoine
* 100 g de pomme cuite écrasée
* 20 g de noix de coco râpée
* 60 g de noix de cajou
* 60 g d’abricots moelleux grossièrement coupés
* 20 g de dattes coupées grossièrement coupés
* 30 g d’huile de coco
* 35 g de miel
* 30 g de purée d’amandes

## Déroulé

Faites chauffer l’huile de coco avec le miel et la purée de cacahuètes.

Mixez grossièrement les abricots avec les noix de cajou.

Mélangez les flocons d’avoine avec la pomme cuite et la poudre cajou abricots. Incorporez le mélange fondu avec le miel.

Disposez la pâte obtenue dans les empreintes d’un moule en silicone tassant bien, sinon dans un moule rectangulaire ou sur une feuille de papier sulfurisée ( vous couperez au couteau les cubes ou les barres à la taille de votre choix !).

Faites cuire les barres de céréales 35 minutes dans un four préchauffé à 140 degrés.

Sortez les du four et laissez les bien refroidir avant de démouler.

ASTUCES : vous pouvez agrémenter ces barres de céréales des gourmandises qui vous font
envie : des pépites de chocolat noir, des amandes ou des noix de cajou, des figues séchées ou
des pruneaux, ajouter de la cannelle ou une pincée de gingembre, à votre guise!