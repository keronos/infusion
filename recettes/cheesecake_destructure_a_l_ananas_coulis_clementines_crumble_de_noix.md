---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/cheesecake_destructure_a_l_ananas_coulis_clementines_crumble_de_noix.jpeg
author: Stéphanie Labadie
description: Cheesecake destructuré à l'ananas, coulis de clémentines, crumble de noix    
title:  Cheesecake destructuré à l'ananas, coulis de clémentines, crumble de noix 
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 30 min
layout: recette
---

# Pour le crumble: 

40 g de farine semi complète 

40 de farine d’épeautre 

50 g de beurre doux coupé en dés de 2 cm 

50 g de cassonade 

1 pincée de sel 

100 g de noix concassées grossièrement 

.......................................

Réunissez les deux farines dans un grand saladier avec le beurre, le sucre et le sel. 

Avec les doigts, formez une pâte friable, puis incorporez les noix. 

Répartissez la pâte sableuse sur une plaque tapissée de papier sulfurisé et enfournez à 180 degrés pendant une quinzaine de minutes. 

Sortez et laissez refroidir jusqu’au moment du dressage. 

## Pour les fruits :

1 petit ananas 

2 c à soupe de sirop d’érable 

2 c à soupe de sucre 

40 g de beurre 

1 branche de thym 

.......................................

Coupez l’ananas en quartiers, puis en tranches de 2 cm d’épaisseur. 

Faites rissoler votre beurre dans une poêle, puis ajouter l’ananas, le sucre et le sirop d’érable, et enfin le thym. 

Laisser cuire à four moyen pour que l’ananas caramélise et prenne une jolie couleur dorée. Laissez tiédir.

### Pour la crème :

15 cl de crême fraîche épaisse 

100 g de yaourt grec 

100 de mascarpone 

2 c à soupe de sucre 

1/2 c à café d’essence de vanille 

1/2 c à café de cardamome en poudre 

.......................................

Réunissez tous les ingrédients dans un saladier et fouettez jusqu’à ce que le mélange forme des pics fermes, en prenant soin de ne pas trop battre, de risque de la faire tourner ( 30 sec devrait suffire) . 

#### Pour le dressage :

Dans le fond d’une assiette creuse, disposez deux c à soupe d’ananas caramélisé, par dessus une bon cuillère de crème, puis enfin saupoudrez de crumble. 

Rajoutez trois morceaux d’ananas en guise de déco, du jus sucrée de cuisson, et un peu de pistaches concassées.

A comer ! 