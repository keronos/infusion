---
author: Stéphanie Labadie
description: Tuiles aux amandes salées 
title: Tuiles aux amandes salées  
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tuiles_aux_amandes_salees.jpeg
layout: recette
---

# Ingrédients: 

* 3 bancs d’oeufs
* 75 g de parmesan 
* 50 g d’amandes effilées 
* 75 g de beurre 
* 75 g de farine 

## Déroulé : 

Préchauffez le four th.6 (180°C).

Faites fondre le beurre dans une casserole et laissez le tiédir.

Battez les blancs à l'aide d'un fouet jusqu'à ce que le mélange mousse. Versez le beurre, puis la farine, le parmesan, les amandes, le piment. Mélangez jusqu’à l'obtention d’une pâte homogène.

Recouvrez la plaque du four de papier sulfurisé puis y déposez des petits tas de pâte (bien espacés). Écrasez chaque petit dôme avec le dos d'une cuillère. 

Enfournez la plaque durant 10 minutes

Sortez la plaque puis décollez les tuiles avec la lame d'un couteau. Posez les soit bien à plat soit sur un rouleau à pâtisserie pour leur donner une forme arrondie. Laissez les refroidir avant de servir.