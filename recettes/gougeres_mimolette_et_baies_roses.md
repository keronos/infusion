---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gougeres_mimolette_et_baies_roses.jpeg
author: Stéphanie Labadie
description: Gougères mimolettes et baies roses 
title: Gougères mimolettes et baies roses 
licence: CC-by-sa
categories: plat
niveau: débutant
Date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 20 à 30 min
temps-preparation: 20 min
layout: recette
---

# Ingrédients: 

4 oeufs 

1 jaune d'oeuf

150 g de farine tamisée

100 g de mimolette

1 pincée de muscade

1 c à café de baies roses moulues au mortier 

80 g de beurre

sel, poivre blanc

## Déroulé: 

Préchauffez le four th.6/7 (200°C).

Râpez la mimolette. 

Mettez le beurre en parcelles dans une casserole avec 25 cl d'eau froide et 1 c. à café de sel. 

Chauffez à feu doux. Dès que le beurre est fondu et que l'ébullition commence, retirez la casserole du feu.

Ajoutez la farine d'un sel coup et mélangez vivement avec une cuillère en bois jusqu'à ce que la pâte soit ferme et se détache des parois de la casserole. 

Faites la dessécher 1 min à feu doux, toujours en la travaillant. Laissez tiédir quelques instants puis incorporez les oeufs l'un après l'autre en mélangeant vigoureusement.

Ajoutez alors le fromage râpé. Relevez de poivre, d'une pincée de muscade et des baies roses. 

Mettez cette réparation dans une poche à douille avec une douille lisse assez grosse et sur une plaque à pâtisserie légèrement beurrée. 

Vous pouvez aussi confectionner vos gougères avec 2 cuillères à café, et les déposer sur une plaque à pâtisserie. 

Laissez cuire et gonfler ( sans ouvrir le four) pendant 20 à 30 minutes. Vos gougères doivent être bien doreés et cuites à l'intérieur. 

**Astuce**: 

A servir à l'apéro ou pour une version repas ( en en forme de quenelles) avec une belle salade verte!