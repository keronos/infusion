---
author: Stéphanie Labadie
description: Boulettes de lentilles, sucré salées aux épices, espuma poireau citronnelle et chapelure aux herbes fraîches
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/boulette_lentilles.jpg
title: Boulette de lentilles
licence: CC-by-sa
categories: plat
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

### Ingrédients

10 à 15 boulettes

**Pour les boulettes:**

- 150 g de lentilles corail
- 70 g de pain rassis
- 1/2 botte de coriandre
- 50 cl de lait d’épeautre ( lait végétal ou lait de vache au choix)
- 40 g de farine de pois chiche ou de fécule de mais
- 2 gousses d’ail
- 1 oignon
- 70 g de feta
- 6 dattes
- 1 c. à café de curry
- 1 c. à café de cumin
- 200 g de pain sec mixé ( = chapelure)
- 50 de parmesan

**Pour l’espuma:**

- 1 poireau
- 50 cl de crème fraîche liquide
- 1 tranche de de lard fumé
- 2 échalotes ciselées
- 1/2 citron

**Pour la chapelure ( pour un pot de 250 g)**

- 200 g de pain sec
- 1 gousse d’ail
- 1/2 bouquet de persil
- 1/2 botte de ciboulette
- Quelques feuilles de menthe
- 3 c à soupe d’huile d’olive

### Déroulé

1. Mettre le pain sec coupé en morceaux à tremper dans le lait d’épeautre.
2. Pendant ce temps, ciseler finement l’oignon, le faire fondre dans une casserole avec une noisette de beurre et un filet d’huile d’olive.
3. Y ajouter les épices, puis les lentilles ( préalablement rincées) et les faire cuire dans deux fois leur volume d’eau avec une feuille de laurier pendant 20 minutes.
4. Passer à la passoire pour enlever l’excédent d’eau, laisser tiédir.
5. Mélanger avec le pain ( essoré), la fêta émietté, les dattes émincés, l’oeuf, la coriandre ciselée et les gousses d’ail hâchées.
6. Rajouter la farine de pois chiche si le mélange semble trop liquide.
7. Former des boulettes, les rouler dans le mélange chapelure /parmesan, et les faire cuire à la poêle.

Pour une version plus légère, huiler un plat à gratin. Placer les boulettes dans le plat et enfourner à 180 °C pendant 20 mn.

Dressage: dans une assiette à soupe, disposer un fond de crème au poireaux avec le siphon, puis les boulettes saupoudrées de chapelure aux fines herbes.

Espuma :

1. Faire revenir les échalotes dans une grosse noisette de beurre.
2. Ajouter le lard fumé et les poireaux taillés en petits tronçons. Laisser fondre, assaisonner avec du sel et du poivre.
3. Puis verser la crême fraîche liquide, attendre l’ébullition puis laisser infuser 10 minutes.
4. Mixer au robot plongeur ou au blender, gouter et relever au besoin avec une pointe de jus de citron.
5. Passer au chinois et verser dans le siphon. Le mettre au bain marie.

Chapelure :

1. Couper le pain en gros morceaux pour le mixer pour obtenir une chapelure grossière.
2. Faire revenir dans une poêle avec du beurre, ajouter 2 pincées de sel.
3. Mixer les herbes fraîches avec l’ail et les ajouter à la chapelure dorée une fois refroidie. Utiliser de suite ou mettre dans un pot et conserver au frais au maximum deux jours.

Dressage: dans une assiette à soupe, disposer un fond de crème au poireaux avec le siphon, puis les boulettes saupoudrées de chapelure aux fines herbes.
