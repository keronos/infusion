---
author: Stéphanie Labadie
description: Petits pains aux oignons rouges confits  
title: Petits pains aux oignons rouges confits  
licence: CC-by-sa
categories: accompagnement 
niveau: débutant
date: 16/01/2022
nombre: 4 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
image_url: petits_pains_aux_oignons_rouges_confits.jpeg
layout: recette
---

# Pour 2 petits pains ou 1 grand : 

* 250gr de farine d’épeautre ou de blé bio
* 4gr de levure déshydratée (ou 150gr de levain)
* 1/2 c à c de sucre de canne
* 200ml d’eau tiède (filtrée, c’est mieux)
* 1 c à c rase de sel
* 1 c à s d’huile de noisette (ou d’olive)
* 1 oignon rouge, ou un carotte, une betterave, un poireau, un poivron etc… 
* 1 c à s de pavot (facultatif)
* une pincée de fleur de sel

....................................................

Commencez par mélanger vivement l’eau avec la levure ou le levain et le sucre, ça doit mousser.

Dans un saladier, versez la farine, le sel et l’huile de noisette et mélange.

Ajoutez l’eau additionnée de levure et sucre, et termine de mélanger.Vous pouvez ajouter un peu de romarin, de thym, origan etc… 

Vous devez obtenir une pâte liquide, souple et légèrement collante.

Laissez-la reposer dans un endroit bien chaud (sur le chauffage) recouverte d’un linge propre pendant environ 30 minutes.

## Pour les oigons rouges confits: 

Coupez des rondelles épaisses à faire rôtir au four pendant 20 minutes à 180 dégrés, avec un filet d’huile d’olive, une cuillère à soupe de sucre de canne, une pincée de sel, quelques feuilles de sauge etc… 

### Pour la confection des pains: 

Allumez le four à 180°.

Préparez une plaque à four chemisée avec la pâte divisée en 2 pâtons, c’est assez liquide, pas d’affolement, tout est normal.

(vous pouvez aussi mettre toute la pâte dans un moule à gâteau ) 

Enfoncez délicatement les rondelles d'oignons, saupoudrez de pavot et de fleur de sel et enfournez pour 30 à 35 minutes, selon la taille des pains.

(vérifiez la cuisson en tapotant le dos du pain, s’il sonne creux, c’est cuit!)

**Astuce**

A la sortie du four, étalez un peu d’huile de noisette ou d’olive au pinceau sur toute la surface du pain, ça va redonner un peu de vie aux légumes ternis par la cuisson et faire joliment briller votre pain..