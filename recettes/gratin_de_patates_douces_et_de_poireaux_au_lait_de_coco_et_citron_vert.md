---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/gratin_de_patates_douces_et_de_poireaux_au_lait_de_coco_et_citron_vert.jpeg
author: Stéphanie Labadie
description: Gratin de patates douces et de poireaux au lait de coco, et citron vert  
title: Gratin de patates douces et de poireaux au lait de coco, et citron vert
licence: CC-by-sa
categories: plat
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson:  40 min
temps-preparation: 30 min
layout: recette
---

# Pour le gratin: 

5 échalotes coupées en tranches de 3 mm d’épaisseur

1 gousses d’ail écrasée

2 c à soupe d’huile d’olive

500 g de petites patates douces coupées en tranches de 3 mm d’épaisseur

500 g de pommes de terre coupées en tranches de 3 mm d’épaisseur

2 petits poireaux finement ciselé

100 g de crème de coco fondue 

3 citrons verts : 1 à café de zeste et 6 cl de jus

20 cl de bouillon de volaille 

## Pour les aromates croustillantes : 

15 cl d’huile d’olive 

3 gousses d’ail ciselées 

5 g de gingembre 

4 oignons verts ciselés 

........................

Préchauffez le four à 180 degrés. Mélangez dans un plat allant au four les échalotes, l’ail, l’huile d’olive et 1 c à café de sel. Laissez revenir 5 minutes. Réservez dans un grand saladier et gardez le plat pour la suite sans le laver. 

Ajoutez dans le saladier les pommes de terre, les patates douces et les poireaux, avec la crème de coco, le jus, 2 c à café de sel, et du poivre, puis mélangez en prenant soin de ne pas briser les tranches de pommes de terre. 

Versez cette préparation dans le plat, en disposant bien les pommes de terre sur le dessus ( en forme de rosace). Versez le bouillon, couvrez bien avec du papier aluminium, et enfournez pour 30 à 40 minutes. 

..................................................

Préparez les aromates croustillantes : faites chauffez l’huile à feu moyen dans une poêle pour y faire revenir l’ail et le gingembre jusqu’à ce que l’ail soit bien doré. 

Avec une écumoire, transférez les aromates sur une assiette avec du sopalin ( pour absorber l’excèdent d’huile). Ajoutez le oignons verts dans la poêle pour les faire frire jusqu’à ce qu’ils deviennent croustillants, en remuant pour qu’ils ne s’agglomèrent pas. 

Transférez dans l’assiette et étalez bien tous les aromates avant de les saupoudrez de sel. 

..................................................

Retirez le papier d’aluminium et arrosez le gratin de manière uniforme avec le restant d’huile ayant servi à frire les aromates. Puis remettez au four et terminez la cuisson à découvert pendant 30 minutes, en montant jusqu’à 200 degrés les 5 dernières minutes pour que le dessus soit doré et croustillant. 

Laissez le gratin refroidir 10 minutes puis garnissez le avec les aromates frits, le zeste de citron et du sel. 