---
author: Bénédicte Lambert
description: de la pâte de fruit réduite au four
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/cuir-fruit.jpg
title: Cuirs de fruit
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 30/01/2019
nombre: 1 ou 2 plaques
temps-cuisson: 1 h
temps-preparation: 10 min
layout: recette
---

## Ingrédients

- 400g de purée de fruits
- 40g de miel
- 60g de purée d'amandes, de sésame ou de noisettes

## Déroulé

1. Mixer les ingrédients.
2. Déposer sur une plaque
3. Enfourner à 100°C pendant 1h en retournant à mi cuisson.
4. Découper des lanières et les rouler puis re-découper.

**Coûts**

- poires = 1,46€
- miel = 0,82€
- sésame = 0,97
- charbon = 0,05€

coût = 3,30

coût par unité = 0,07€
