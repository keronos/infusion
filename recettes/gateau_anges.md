---
author: Stéphanie Labadie
description: Gâteau des anges aux agrumes et sa crème anglaise à la menthe
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/gateau_anges.jpg
title: Gâteau des anges aux agrumes
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 08/10/2021
nombre: 4 personnes
temps-cuisson: 50 min
temps-preparation: 20 min
layout: recette
---


## Ingrédients  
 
### Pour un gâteau léger comme un nuage :
* 11 blancs d’oeufs
* 220 g de sucre
* 30 g de maïzena
* 100 de farine
* 1 c à café de maizena
* Le zeste d’une orange ou d’un citron
* 1 c à soupe d’eau de fleurs d’oranger

### Pour la crême anglaise :
* 1 petit bouquet de menthe (ou la moitié d'un gros)
* 40 cl de lait
* 4 jaunes d’oeufs
* 60 g de sucre en poudre


## Déroulé

Allumez votre four à 200 degrés.

Battez les blancs d’oeufs jusqu’à ce qu’ils prennent la consistance de blancs en neige. Rajoutez à ce
moment là le sucre en plusieurs étapes, et vous allez obtenir la texture d’une meringue.

A l’aide d’un tamis, passez au chinois la farine, maïzena et levure, et ajoutez les au mélange en 3 fois.

Mélangez délicatement entre chaque pour bien l’incorporer, puis terminez avec 1 c à soupe d’eau de fleurs d’oranger, et les zestes d’orange. Vous pouvez aussi continuer à utiliser le batteur électrique!

Verser la pâte dans un moule à cheminée beurré et fariné en 3 étapes. Tapotez le moule pour éviter les bulles d’air et une fois toute la préparation versée, passez une baguette dans le mélange.

Enfournez pour 25 à 30 min. Le gâteau est cuit lorsque le dessus reprend sa forme quand on le touche du bout du doigt.

Laisser refroidir la tête en bas, et démoulez délicatement une fois le gâteau à température ambiante.

### Crême anlaise

Équeutez les tiges de menthe et lavez les feuilles.Faites bouillir le lait dans une casserole. Retirez du
feu, ajoutez le menthe et laissez-la infuser 5 min.

Récupérez les feuilles de menthe et mixez-les finement avec un peu de lait. Puis ajoutez le lait restant et mixez à nouveau.

Dans un saladier, fouettez les jaunes d’oeufs avec le sucre. Incorporez peu à peu le lait à la menthe.

Reportez le mélange sur feu doux-moyen et faites cuire doucement pour que la crème épaississe. La
crème ne doit surtout pas bouillir. Remuez avec une cuillère en bois pendant la cuisson et surveillez la consistance : la crème doit napper la cuillère.

Laissez bien refroidir et servez cette crème anglaise à la menthe avec un gâteau au chocolat.