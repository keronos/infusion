---
author: Bénédicte Lambert
description: Une salade sympa à base de chou-fleur
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/semoule-chouFleur.jpg
title: Salade de taboulé de chou-fleur
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 12/05/2017
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 10 min
layout: recette
---

## Ingrédients

- 1 chou-fleur
- 1 poivron rouge
- 3 CS d'huile d'olive
- 1/2 botte de menthe
- 1/2 botte de persil
- 1 citron
- sel, poivre

## Déroulé

1. Râper le chou-fleur avec le plus petit trou de la râpe.
2. Découper le poivron rouge en petits dés.
3. Hacher les herbes.
4. Presser le citron.
5. Dans un saladier, mélanger tous les ingrédients et arroser d'huile au fur et à mesure, assaisonner.

## Astuces

Ajouter des radis, des tomates cerises ou des poivrons de différentes couleurs.
