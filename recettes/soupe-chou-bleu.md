---
author: Bénédicte Lambert
description: soupe au chou et au tofu
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/soupe-chou-vert.jpg
title: Soupe de chou vert et tartine de bleu
licence: CC-by-sa
categories: plat
niveau: débutant
date: 26/10/2018
nombre: 4 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

- 1 oignon
- 1CS d’huile d'olive
- 100g de tofu fumé
- 100 ml de vin blanc sec
- 1 petit chou vert frisé
- 2 carottes
- tartines de pain
- 1 fromage roquefort ou gorgonzola
  sel, poivre

## Déroulé

1. Couper l’oignon en lamelles.
2. Faire chauffer l’huile à feu vif dans une sauteuse ou un faitout.
3. Quand l’oignon est bien doré, ajouter le tofu coupé en cubes, faire revenir 1 minute.
4. Déglacer au vin blanc.
5. Laisser réduire 2 minutes.
6. Enlever les premières feuilles du chou et détacher le reste des feuilles, les couper si elles sont trop grandes.
7. Faire chauffer une casserole d’eau bouillante afin de le blanchir pendant 5 minutes.
8. Égoutter le chou et réserver.
9. Couper les carottes en petits cubes.
10. Déposer le chou et les carottes dans le faitout.
11. Verser 1200ml d’eau bouillante.
12. Laisser cuire à feu doux pendant 15 minutes avec un couvercle.
13. Découper 4 tranches de pain.
14. Étaler le fromage et réserver les tartines.
15. Saler et poivrer la soupe.
16. Dans des assiettes à soupe, déposer la tartine et recouvrir de soupe au chou.

## Astuces

Ajouter des légumes tels que poireaux, navets et panais pour en faire un plat complet et savoureux.  
Mettre de la livèche pour parfumer le bouillon de la soupe.
