---
author: Stéphanie Labadie
description: Tzatziki frais frais!  
title: Tzatziki frais frais!  
licence: CC-by-sa
categories: entrée 
niveau: débutant
date: 17/01/2022
nombre: 4 à  personnes
temps-cuisson: 0 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/Tzatziki_frais_frais.jpeg
layout: recette
---

# Ingrédients: 

* 1 concombre 
* 1 pomme 
* 1 échalote ciselée 
* 1/2 botte de menthe
* 1/2 botte de coriandre 
* 4 cornichons aigre doux 
* Un jus de filet de citron 
* 2 yaourts bulgare ( ou 1 yaourt et 15 cl de crême fraîche pour une version plus gourmande!)

## Déroulé: 

Râpez le concombre, et laissez le rendre son jus dans une passoire pendant 20 minutes. 

L’essorer entre vos mains, puis le mélanger dans un saladier avec le yaourt, la pommes taillée en allumette, l’échalote et les herbes ciselées, les cornichons coupées en petits dés. 

Assaisonnez à votre convenance avec du sel, une pointe d’huile d’olive, du jus de citron. Laissez au frais avant de servir.

**Astuces**: 

Ce Tzatziki peut se déguster avec des crackers ou quelques tranches de pain à l'apéro, ou en condiments, en accompagnement de légumes bouillis ou rôtis, d'un poisson blanc, ou même en entrée avec quelques croûtons de pain grillés disposés dessus, c'est parfait l'été!  