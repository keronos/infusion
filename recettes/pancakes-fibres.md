---
author: Pascal Romain
description: des pankakes avec les fribres extraites des jus de fruits ou légumes.
title: Pancakes aux fibres
licence: non définie
categories: accompagnement
niveau: débutant
date: 12/05/2017
nombre: 11 personnes
temps-cuisson: 30/40 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/pancakes-fibres.jpg
temps-preparation: 20 min
layout: recette
---

pour 36 pancakes

### Ingrédients

- 450 g de pulpe de jus d'orange, carotte et pomme
- 450 g de farine : 1 €
- 3 cuillère à café de poudre à lever
- 3 oeufs: 1€
- 100g de sucre: 0,5 €
- 75 cl de lait : 1 €
- 3 cuillère à soupe d'huile de tournesol: 0,5 €

### coût

4 €

### Ustensiles

- poêle
- cul de poule
- cuillière
- fouet

### Recette

1. Mélanger dans un saladier la farine, la pulpe, la farine, la poudre à lever et le sucre
2. Faire un puit, cassez les oeufs et ajouter le lait et l'huile
3. Mélanger bien puis incorporer la pulpe de manière à obtenir une pâte homogène
4. Faire chauffer la poêle huilée et faire cuire des cuillères de pâte à feu moyen
5. Retourner les pancakes au bout de quelques minutes
