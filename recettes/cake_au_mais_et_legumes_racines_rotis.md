---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/Cake_au_mais_et_legumes_racines_rotis.jpeg
author: Stéphanie Labadie
description: Cake au mais et légumes racines rôtis
title: Cake au mais et légumes racines rôtis
licence: CC-by-sa
categories: plat
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 40 à 50 min
temps-preparation: 30 min
layout: recette
---

# Pour les légumes :

1 petite betterave crue

1/2 potimarron 

1 patate douce 

1 oignon rouge 

4 gousses d’ail écrasées

1 c à soupe de sucre de canne 

1 c café d’épices ( cumin, cannelle, massalé, paprika etc…)

4 c à soupe d’huile d’olive 

Préchauffez le four à 180 degrés. 

Epluchez l’oignon et les patates douces, puis coupez tous les légumes en cubes irréguliers de 2 cm de côté ( pas trop gros pas trop petits). Ecrasez les gousses d’ail, et mélangez tous les ingrédients dans un plat à gratin, en prenant soin de bien masser les légumes avec l’huile d’olive, le sel, le sucre et les épices.

Enfournez pendant 30 à 40 minutes, le temps que les légumes deviennent cuits à coeur et fondants. Sortez les et laissez les tiédir.

## Pour l’appareil à cake :

3 oeufs 

10 cl d’huile d’olive 

1 yaourt brassé

1 boîte moyenne de mais 

60 de farine de sarrasin 

60 g de farine semi-complète 

60 g de farine de riz 

1 c à café de levure chimique

50 g de mimolette râpée 

Mettre le mais dans le mixer et donnez 3 impulsions, le temps de hâcher grossièrement les grains. 

Battre au fouet les oeufs, l’huile d’olive, le yaourt, les farines, la levure et le fromage. Assaisonnez et ajoutez à la toute fin le mais. 

Incorporez les légumes à l’appareil à cake, versez la préparation dans un moule à cake ou à gâteau, et enfournez pour environ 45 minutes. Ne vous fiez pas trop à la pointe du couteau qui ressortira humide avec le mais. Laissez tiédir avant de déguster avec une belle salade! 

**astuce**: 

Vous pouvez réaliser la même recette avec des légumes d'été rôtis : courgettes jaunes ou vertes, tomates cerises, oignons etc... et rajoutez un peu de basilic ou d'estragon, ça devrait être pas mal aussi! 

