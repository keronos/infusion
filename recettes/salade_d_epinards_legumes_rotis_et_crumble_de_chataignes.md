---
author: Stéphanie Labadie
description: Salade d'épinards, crumble de chataignes, sauce cajou 
title: Salade d'épinards, crumble de chataignes, sauce cajou   
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 40 à 50 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade_d_epinards_crumble_de_chataignes_sauce_cajou.jpeg
layout: recette
---

# Pour les légumes rôtis :

* 1 botte d’épinards 
* 1 céleri rave 
* 700 g de topinambours 
* 2 grosses carottes 
* 2 oignons rouges 
* 2 c à soupe de miel 
* 2 c à soupe de soja 
* 4 c à soupe d’huile d’olive 
* 1 c à café d’épices de votre choix

........................................................

Préchauffez le four à 180 degrés. 

Passez les épinards à l’eau pour enlever l’excédent de terre. Essorez et réservez. 

Pelez le céleri rave, les topinambours, les carottes. Coupez les en morceaux de 2cm de côté. 

Disposez les légumes sur une plaque de cuisson, et malaxez avec le mélange de miel, huile d’olive et soja. 

Enfournez pendant 30 minutes, jusqu’à ce que les légumes soient rôtis et fondants. 

## Pour le crumble épeautre/châtaigne :

* 70 g de beurre 
* 70 de farine d'épeautre 
* 40 g de poudre d’amandes 
* 40 de farine de châtaignes 
* 1 pincée de sel

........................................................

Préchauffez le four à 180 degrés. 

Frictionner le beurre avec les farines jusqu'à l'obtention d'une consistance sableuse. 

Dispersez cette préparation sur une plaque de cuisson, et enfournez pendant 20 minutes. 

Le crumble va dorer, sortez le et laissez le tiédir. 

### Pour la crème de cajou  : 

* 100 g tasse de noix de cajou 
* 5 cl d’huile de tournesol / d’olive
* 3 c soupe de vinaigre de riz
* 2 petite c à soupe de nuoc man 
* 2 petite c à soupe de miel 
* 1/2 gousse d’ail pelée et dégermée 
* 15 à 20 cl d’eau chaude 

........................................................

Mixer tous les ingrédients et aboyer l’eau chaude à la fin, jusqu’à l’obtention d’une consistance crémeuse, lisse et assez liquide pour servir de sauce.

**OU ( 2e option ! )**:  

* 100 g de noix de cajou crues trempée dans l’eau pendant une nuit 
* 20 ml d’huile d’olive 
* 80 ml d’eau froide 
* 1 c à café de paprika fumé 
* 1 pincée de sel 
* Une petite gousse d’ail si l’envie vous tente ! 

........................................................

Mixer tous les ingrédients et conservez au frigo

#### Pour le dressage :

Mélangez vos feuilles d’épinards avec la moitié de la crème de cajou. 

Disposez les légumes par dessus, versez par dessus le reste de crème de cajou, puis le crumble de châtaignes. 

Bonne dégustation !!! 