---
author: Stéphanie Labadie
description: Moelleux au chocolat, noix et patates douces  
title: Moelleux au chocolat, noix et patates douces  
licence: CC-by-sa
categories: dessert 
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 20 à 30 min
temps-preparation: 20 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/moelleux_au_chocolat_noix_et_patates_douces.jpeg
---

# Ingrédients: 

400 g de patate douce cuite 

250 g de chocolat pâtissier 

40 g d’huile de coco 

1 oeuf ( ou une banane) 

30 g de cacao amer non sucré 

80 de farine de petit épeautre 

50 g de noix 

## Déroulé : 

Déposez la patate douce et le chocolat dans une casserole.

Faites chauffer à feu doux jusqu’à ce que le chocolat soit fondu.

Ajoutez l’huile de coco et laissez-la fondre, puis l’oeuf ou la banane et mixez l’ensemble.

Intégrez ensuite les la farine et le cacao et mélangez à la maryse.

Terminez par les noix.

Versez la préparation dans un moule carré de 20 cm, préalablement huilé et chemisé.

Enfournez pour 15 minutes à 200°C.

Laissez le gâteau totalement refroidir avant de démouler. 

A servir avec une crême anglaise parfumée aux zestes d'oranges, ou seul c'est bon aussi ! 