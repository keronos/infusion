---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: Tortilla à la tomate.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tortilla-tomates.jpg
title: Omelette à la tomate
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 6 min
temps-preparation: 15 min
---

### Ingrédients

- 3 oeufs
- 3 cuillère à soupe de lait
- 3 cs d'huile d'olive
- 3 tomates
- 2 gousses d'ail
- 1 bouquet de basilic
- 3 cs de sésame
- sel, poivre
- pique en bois

### Préparation

1. Dans un bol, mélangez les oeufs et le lait. Salez, poivrez
2. Dans une pôele antiadhésive, faites chauffer une cs d'huile d'olive et versez-y la moitié du mélange d'oeufs
3. Bougez la poêlepour que la préparation couvre tout le fond
4. Quand le dessus de cette omelette est sec, faites la glisser sur un plat et laissez tiédir
5. faites cuire une seconde omelette avec le reste de la préparation dans une cs d'huile d'olive
6. Coupez la tomate en 2, épépinez-les, puis taillez-les en cubes
7. faits-les revenir 2 min à la casserolle dans le reste d'huile chaude avec l'ail pelé et haché. Salez, poivrez
8. Sur les 2 omelettes, répartissez la tomate, parsemez de balilic haché, puis roulez-les pour obtenir 2 cyclindres
9. Tronconnez-les
10. Sur chaque morceau, déroulez 3 cm d'omelette que vous maintenez en l'air avec une pique en bois.
11. Parsemez de sésame
