---
author: Bénédicte Lambert
description: Pancake de betteraves
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/pancake-betterave.jpg
title: Pancake de betteraves
licence: CC-by-sa
categories: entrée 
niveau: débutant
date: 09/11/2018
nombre: 4 personnes
temps-cuisson: 5 min
temps-preparation: 10 min
layout: recette
---

### Ingrédients  

* 2 œufs 
* 140 ml de lait d'avoine bio
* 125g de mix de farine sans gluten
* 1/2 cuillère à café de poudre à lever
* 20 g de sucre roux (en version sucrée)
* 40g de betterave crue
* sel

### Déroulé  

1. Séparer les blancs des jaunes.  
2. Mélanger le lait, les jaunes d'aouts et la farine.  
3. Ajouter la levure, le sel et le sucre.  
4. Verser la batterie râpée finement ou le résidu du jus de betterave.  
5. Battre les blancs en neige et les incorporer délicatement.  
6. Laisser reposer 30 min.  
7. Dans une poêle faire chauffer un peu d'huile.  
8. Déposer des petits tas de pâte et retourner quand des trous apparaissent à la surface.  
9. Cuire l'autre face et servir.  

### Astuces

Changer les farine et mettre de la farine de pois chiches.   
Remplacer le lait par de l'eau