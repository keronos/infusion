---
author: Bénédicte Lambert
description: un classique de la cuisine avec des légumes
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/gateau-carottes.jpg
title: Gâteau à la carotte
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 12/05/2017
nombre: 6 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

- 2 oeufs
- 125g de farine
- 1/2 sachet de levure
- 120g de sucre roux
- 1cc de cannelle
- 100ml d'huile de tournesol
- 125g de carottes fraiches râpées
- 25g de noix hachées et quelques cerneaux pour la déco
- 25g de noix de coco râpée
- sel

**Glaçage** :
- 50g de beurre 1/2 sel ramolli
- 50g de fromage blanc
- 225g de sucre glace

## Déroulé

1. Préchauffer le four à 180°C.
2. Mélanger les oeufs avec la farine, la levure, le sucre, la cannelle et l'huile.
3. Râper les carottes et ajouter au mélange.
4. Hacher les noix, mélanger.
5. Ajouter la noix de coco, saler.
6. Huiler le moule et verser la préparation.
7. Enfourner pour 20/25 min.
8. Préparer le glaçage en mélangeant les ingrédients jusqu'à l'obtention d'une crème lisse et onctueuse.
9. Dresser avec le glaçage et les cerneaux de noix lorsque le gâteau est refroidi.

Crédit photo : Joseph Gonzalez
