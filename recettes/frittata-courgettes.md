---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: omellette aux courgettes facile à réaliser.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/frittata-courgette.png
title: frittata de courgettes
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 4 personnes
temps-cuisson: 10 min
temps-preparation: 15 min
---

### Ingrédients

- 600 g de petites courgettes
- 2 gousses d'ail
- 8 oeufs
- 4 cs d'huile d'olive
- 1 pincée de piment
- sel

### Préparation

1. Lavez et essuyez les courgettes, coupez-les en très fines rondelles après avoir éliminé les extrémités
2. Pelez et hachez l'ail
3. Cassez les oeufs dans une jatte (petit saladier), salez et battez-les un peu à la fourchette
4. Faites chauffer l'huile dans une grande poële antiadhésive, metttez l'ail à dorer 1 min. en remuant, puis ajoutez les courgettes et remuez dans cesse l'ensemble, 5 min. à feu vif, pour que les courgettes colorent un peu.
5. Salez, relevez de piment, mélangez et verser les oeufs
6. Laissez prendre comme une omelette, sur feu moyen. Au début, ramenez les bords vers le centre avec une spatule.
7. Dès que l'omelette commence à prendre, faites la glisser sur une assiette, recouvrez l'assiette avec la poêle et retournez l'ensemble pour faire dorer quelques minutes l'autre face de la frittata.
8. Servez la frittata très chaude, coupée en parts.
