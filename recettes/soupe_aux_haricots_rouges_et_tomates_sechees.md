---
author: Stéphanie Labadie
description: Soupe aux haricots rouges et tomates séchées   
title: Soupe aux haricots rouges et tomates séchées   
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/soupe_aux_haricots_rouges_et_tomates_sechees.jpeg
temps-preparation: 20 min
layout: recette
---


# Pour la salsa: 

* 1 petit oignon rouge haché finement 
* 1 c à soupe d’huile de tournesol
* 100 g de grains de mais doux 
* 1 trait de citron vert 
* 1 petit bouquet de coriandre 
* Un peu de crême aigre ou yaourt pour servir

Faites fondre l’oignon 5 minutes dans l’huile de tournesol jusqu’à ce qu’il commence à se colorer. Augmentez le feu et ajoutez le mais doux en remuant jusqu’à ce que les grains de mais commencent à griller sur les bords. Hors du feu, ajoutez du sel, du poivre et le jus de citron vert. 

## Pour la soupe: 

* 1 c à soupe d’huile de tournesol 
* 2 oignons rouges hachés 
* 3 gousses d’ail hachées 
* 1 c à café de curry rouge 
* 1 c à café d’origan frais ou séché 
* 1 c à soupe de graine de cumin moulu au mortier 
* 12 moitiés de tomate séchées 
* 700 g de haricots noirs cuits
* 75 cl de bouillon de légumes 
* 1 filet de jus de citron vert

Faites chauffer l’huile dans une grande cocotte. Faites y fondre les oignons rouges à feu doux ( il ne faut pas qu’ils dorent). Ajoutez l’ail, la pâte de curry, l’origan et les graines de cumin. Poursuivez la cuisson 5 minutes jusqu’à ce que le cumin embaume. 

Ajoutez les tomates, les haricots et le bouillon de légumes. 

Portez à ébullition et laissez frémir 20 à 30 minutes. 

Ajoutez du sel, du poivre et du jus de citron. Versez le tout dans un blender et mixez jusqu’à l’obtention d’un mélange lisse. 

Goutez, rectifiez l’assaisonnement si besoin. 

### Pour le dressage :

Répartissez la soupe dans des bols chauds. Finissez avec une cuillère de crême aigre, une cuillère de salsa, et saupoudrez de quelques feuille de coriandre. 

**Astuce**: 

Pour varier les gouts, vous pouvez troquer la crême aigre contre de la crème de coco, ou ajoutez un oeuf mollet dans chaque bol, ou des boulettes de céréales pour un repas complet. 

Vous pouvez aussi boire cette soupe froide l’été, en ajoutant quelques feuilles de basilic pour la fraîcheur. 