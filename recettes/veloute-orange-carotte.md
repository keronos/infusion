---
author: Stéphanie Labadie
description: Velouté d’orange carottes coco et dukkha maison, servi froid au chaud!
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/veloute-orange-carotte.jpg
title: Velouté oranges et carottes coco
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 08/02/2019
nombre: 4 personnes
temps-cuisson: 20 min
temps-preparation: 20 min
layout: recette
---

## Ingrédients

Pour la soupe:

- 1 kg de carottes
- 3 oranges
- 2 oignons
- 2 gousses d’ail
- 1 c à soupe de sucre complet
- 2 à 3 cm de racine de gingembre frais, pelé et coupé en petits dés
- 80 cl de bouillon de légumes
- 30 cl de lait de coco
- 1/2 botte de coriandre

Pour le dukkha:

- 150 d’amandes
- 50 g de noisettes
- 50 g de graines de courge
- 40 g de cranberries
- 3 cuillères à soupe de graines de cumin
- 4 cuillères à soupe de graines de coriandre
- 1 cuillère à café de fleur de sel

## Déroulé

Dans une grande casserole, faites revenir les oignons ciselés dans un filet d’huile d’olive.

Lorsqu’ils deviennent translucides, rajoutez les gousses d’ail écrasées, le gingembre et le sucre, puis les carottes coupées en rondelles. Laissez rissoler puis mouiller les ingrédients avec le bouillon de légumes. Faites cuire doucement pendant 20 minutes, jusqu’à ce que les carottes soient cuites. Puis au moment de mixer, ajoutez le jus des oranges et le lait de coco.

Salez à votre convenance et ajouter un filet de jus de citron au besoin.

Dresser les assiettes de soupe tiède avec la coriandre ciselée et le dukkha.

**Petit plus**: pour une soupe hivernale plus consistante, ajoutez une patate douce à votre soupe, cela lui donnera plus de velouté, de consistance… et de gourmandise!

La servir l’été avec du melon blanc et du jambon en petits cubes, et l’hiver avec des croûtons ailés, une cuillère de crème et un soupçon de curcuma.

Faites torréfier les amandes et les noisettes dans une poêle sèche. Lorsqu’une bonne odeur de grillé se dégage, débarrasser et laisser refroidir sur une assiette avant de les hâcher grossièrement.
Renouveler l’opération avec les graines de courge, en les gardant entières. Puis avec la même poêle, faites griller les épices, retirez du feu lorsque vous commencer à sentir l’odeur des épices. Pilez au mortier les graines de cumin et de coriandre.

Mélanger tous les ingrédients avec les cranberries, et réserver dans un récipient hermétique pendant 6 à 8 semaines.

**Petit plus** : pour une version plus rapide, vous pouvez faire torréfier les amandes et les
noisettes, puis ajouter dans la même poêle les épices en poudre, avec un très léger filet
d’huile d’olive et une fois refroidi, de la fleur de sel puis le fruit sec de votre choix pour une
version sucrée salée.

**Coûts**
