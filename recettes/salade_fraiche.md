---
author: Stéphanie Labadie
description: Salade fraîche de céleri, panais et pruneaux, sauce miso et jus de pomme, avec ses tuiles aux amandes et parmesan
image_url: https://gitlab.com/keronos/infusion/raw/master/recetteshttps://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade_fraiche.jpg
title: Salade fraîche de céleri
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 08/10/2021
nombre: 4 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

## Ingrédients

Pour 4 personnes :

### pour la salade :
* 250 de celeri rave
* 70 g d’amandes concassées
* 2 panais
* 1 petit pomme
* 4 pruneaux
* 3 c à café de miso
* 7 cl de jus de pomme
* 1 pointe de jus de citron

### Pour les tuiles aux amandes :

* 3 bancs d’oeufs
* 75 g de parmesan
* 50 g d’amandes effilées
* 75 g de beurre
* 75 g de farine

## Déroulé

Pelez le céleri rave et râpée à la grosse grille . Faites de même avec le panais, et la pomme.

Mélanger le miso avec le jus de pomme, et ke jus de citron si besoin.

Emincez grossièrement le pruneau.

Mélangez tous les ingrédients ensemble avec quelques feuille de coriandre si l’appétit vous en
dit!

Préchauffez le four th.6 (180°C).

Faites fondre le beurre dans une casserole et laissez le tiédir.

Battez les blancs à l'aide d'un fouet jusqu'à ce que le mélange mousse. Versez le beurre, puis la farine,
le parmesan, les amandes, le piment. Mélangez jusqu’à l'obtention d’une pâte homogène.

Recouvrez la plaque du four de papier sulfurisé puis y déposez des petits tas de pâte (bien espacés).

Écrasez chaque petit dôme avec le dos d'une cuillère.

Enfournez la plaque durant 10 minutes

Sortez la plaque puis décollez les tuiles avec la lame d'un couteau. Posez les soit bien à plat soit sur un rouleau à pâtisserie pour leur donner une forme arrondie. Laissez les refroidir avant de servir.