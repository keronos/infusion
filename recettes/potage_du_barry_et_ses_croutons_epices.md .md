---
author: Stéphanie Labadie
description: Potage du Barry, et ses croûtons épicés 
title: Potage du Barry, et ses croûtons épicés  
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/potage_du_Barry_et_ses_croutons_epices.jpeg
---

# Pour la soupe : 

– 1 chou fleur (soit 700g une fois les feuilles ôtées)
– 2 pommes de terre
– 50 cl de lait
– 25 cl d’eau
– 1 bouillon cube de volaille
– 10cl de crème entière
– 2 échalotes 

............................

Laver et couper en petits morceaux le chou-fleur.
Laver et peler les pommes de terre. Les couper en rondelle.

Peler les échalotes et les émincer grossièrement. 

Dans une cocotte, faire fondre le beurre, et rissoler les échalottes. Ajouter le chou fleur, les pommes de terre, puis le lait, l’eau et le bouillon.

Cuire environ 20 minutes et mixer soigneusement. 
Pour un résultat plus fin, vous pouvez passer la soupe obtenue au chinois.

Ajouter la crème à la soupe et émulsionner au mixer. Vous pouvez également ajouter la crème préalablement montée en chantilly.

## Pour les croûtons: 

* 500 g de pain sec
* épices
* huile d'olive 
* sel 

..............................

Couper le pain en petits cubes, le mélanger avec un filet d’huile et une pincée de fleur de sel, 1 c à café de cumin. 

Faire griller au four à 160 degrés pendant 5 à 10 minutes. 

Laisser refroidir avant de déguster, pour retrouver du croquant, et ne pas se brûler la langue! 

Les disposer sur la soupe avec quelques brins de cerfeuil. ET… bon appétit ! 

**ASTUCE**:

Vous pouvez remplacer les épices une autre saveur, frotter le pain avec une gousse d’ail part exemple avant de le découper en cubes et le mélanger à l’huile d’olive. 