---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/rillettes_de_lentilles_au_maquereau_et_leurs_crackers.jpeg
author: Stéphanie Labadie
description: Rillettes de lentilles au maquereau et leus crackers  
title: Rillettes de lentilles au maquereau et leus crackers   
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette 
---

# Pour les rillettes : 

400 g de lentilles cuites 

2 filets de maquereau fumés émiettés 

1 yaourt grec ( ou 20 g de crême fraîche pour une version moins light!) 

1 petite pomme acidulée coupée en brunoise ( tout petit cubes) 

1 branche de céleri coupée en brunoise ( tout petit cubes), ou à défaut 2 petits navets 

1/2 botte de coriandre / aneth ciselée 

1 échalote ciselée finement 

......................................

Dans un faitout mélangez tous les ingrédients. Le mélange doit être assez consistant pour pouvoir former des quenelles. 

Servez avec une salade verte, une vinaigrette soja miel ( 4 c à soupe d’huile d’olive, 2 c à soupe de soja, 1 c à soupe de miel ) avec de quelques graines de courge et tournesol torréfiées 

## Pour les crackers : 

200 g de farine de blé semi complet 

65 g de beurre 

1 c à soupe de cassonade 

1/2 c à café de sel 

65 g de beurre 

4 c à soupe d’eau 

1 c à café de coriandre et 1 c à café de baies roses moulues au mortier

2 c à soupe de sésame

......................................

Frictionnez le beurre avec tous les autres ingrédients pour obtenir une consistance sableuse, et ajoutez l’eau à la fin pour former une boule de pâte. 

Etaler très finement sur une feuille de papier sulfurisé, saupoudrez de fleur de sel et de sésame (ou graines de tournesol, ou de courges …) . 

Prédécouper vos crackers à l’aide d’un couteau sur la pâte avant de la mettre cuire 20 minutes à 180 degrés ( plus ou moins selon la puissance du four). 

Laissez refroidir puis presser vos crackers dans une assiette. 

A servir avec de la tapenade, du guacamole, des rillettes de lentilles, un perso d’herbes fraiches ou de tomates séchées …. 

### Pour le dressage : 

Disposez des rillettes de maquereau sur les crakers, et servez aussitôt! 