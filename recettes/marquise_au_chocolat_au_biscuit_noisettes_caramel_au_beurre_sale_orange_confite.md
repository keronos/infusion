---
author: Stéphanie Labadie
description: Marquise au chocolat au biscuit noisettes, caramel au beurre salé, orange confite
title: Marquise au chocolat au biscuit noisettes, caramel au beurre salé, orange confite
licence: CC-by-sa
categories: dessert 
niveau: débutant
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/marquise_au_chocolat_au_biscuit_noisettes_caramel_au_beurre_sale_orange_confite.jpeg
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 40 à 50 min
temps-preparation: 60 min
layout: recette
---

# Pour le crumble de noisettes: 

50 g de farine d’épeautre 

50 g de poudre d’amande

40 g de noisettes concassées 

60 g de sucre complet 

70 g de beurre 

Préchauffez le four à 180 degrés. 

Versez les noisettes concassées, le beurre, la poudre d’amande, le sel et le sucre dans un cul de poule, et travaillez la pâte du bout des doigts pour obtenir une consistance sableuse, puis étalez le crumble sur une plaque à pâtisserie et laissez dorer 12 minutes au four. 

## Pour la marquise: 

250 g de chocolat 

150 g de beurre 

5 oeufs 

100 g de sucre 

1 pincée de sel 

Cassez le chocolat en morceaux, coupez le beurre en cube, et faites les fondre au bain marie. 

Séparez délicatement le blanc des jaunes. Battez les jaunes avec le sucre, jusqu’à ce que le mélange blanchisse. Ajoutez ce mélange au chocolat. 

Tapissez le fond du moule à terrine d’une feuille de papier sulfurisé. Déposez une bonne couche de crumble, puis versez par dessus l’appareil à marquise. Laissez prendre la préparation au congélateur au moins 6 heures. 

### Pour le caramel au beurre salé: 

 160 g de sucre 

80 g de beurre demi sel à température ambiante

20 cl de crème liquide à température ambiante 

Mettez dans une poêle le sucre et faites chauffer sur feu moyen pour obtenir un caramel à sec. 

Le sucre va commencer à se dissoudre, devenir liquide puis se transformer en un liquide ambré : ça prend à peu près 3 ou 4 minutes. NE PAS MELANGER LE SUCRE, il va caraméliser tout seul ! 

Dès que le sucre est transformé en caramel, retirez la casserole du feu et ajoutez délicatement une petite partie de la crème. Attention aux projections (le caramel brûle…).  

Remuez vivement et incorporez le reste petit à petit. 

Quand il n'y a plus de bouillons, ajoutez le beurre et remuez à nouveau jusqu'à consistance d'une crème moyennement liquide. 

Si la crème ne vous semble pas assez épaisse, remettez la casserole à feu doux et remuez jusqu'à ce que vous obteniez la consistance souhaitée.

#### Pour l’orange confite : 

2 oranges non traitées

300 g de sucre en poudre 

300 g d’eau 

Commencer par préparer tous les ingrédients. Laver soigneusement les oranges et les trancher en rondelles régulières de 3 à 4 mm d’épaisseur.

Porter à ébullition l'eau et le sucre en poudre.

Lorsque le sucre est bien fondu, ajouter les tranches d'oranges dans le sirop frémissant.

Cuire à frémissement pendant 15 minutes.

Une fois les 15 minutes écoulées, stopper la cuisson et laisser refroidir dans le sirop. Renouveler les étapes 4 et 5, 5 fois, jusqu'à ce qu'il n'y ait pratiquement plus de sirop et que les oranges soient confites, légèrement translucides.

Égoutter les tranches d'oranges, les disposer sur un papier cuisson le temps qu'elles refroidissent. Réservez.

##### Pour le dressage :

Mettre une cuillère à soupe de caramel au beurre salé dans le fond d’une assiette, puis une tranche de marquise au chocolat, un quartier d’orange confite, et pourquoi pas une pincée de pollen. Bonne dégustation! 

**Astuce**: 

Vous pouvez aussi réaliser la marquise sans crumble de noisettes, elle sera délicieuse aussi, et la servir avec une crême anglaise parfumée au basilic ou à la menthe! 