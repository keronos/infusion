---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/fraisier_leger_comme_un_nuage.jpeg
author: Stéphanie Labadie
description: Fraisier léger comme un nuage 
title: Fraisier léger comme un nuage 
licence: CC-by-sa
categories: dessert
niveau: moyen
date: 17/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 50 min
layout: recette
---

# Pour la génoise : 

1 gousse de vanille

120 g de sucre

100 g de farine

50 g de beurre

4 oeufs

........................................

Réalisez la génoise : séparez les blancs des jaunes et battez les blancs bien fermes

Ajoutez un tiers du sucre aux blancs quand ils sont fermes, et battez encore 1 minute.

Fouettez les jaunes avec le restant de sucre et la vanille

Ajoutez le beurre fondu, puis la moitié de la farine tamisée

Pour détendre la pâte, ajoutez 2 c. à soupe de blanc d’oeufs

Puis ajoutez le reste de la farine

Enfin, ajoutez les blancs d'oeufs délicatement en soulevant la pâte pour ne pas casser les bulles d'air !

Versez la pâte dans deux cercles (ou sur une plaque, vous pourrez découper les cercles après cuisson…)

Enfournez à four chaud 10 minutes à 190°C. Sortez et laissez refroidir. 

## **Pour la crème :** 

20 cl de crème liquide entière

25 cl de lait demi écrémé

2 jaunes d’oeuf

40 g de maizéna

50 g de sucre

1 g d'agar agar ou 2 feuilles de gélatine

1 gousse de vanille ou 1 sachet de sucre vanillé

........................................

Réalisez la crème en chauffant le lait avec la gousse de vanille fendue (ou l'extrait de vanille ou sucre vanillé, au choix )

Fouettez les jaunes avec le sucre, ajoutez la maizéna ensuite, progressivement

Ajoutez l'agar agar dans le lait et laissez bouillir 1 minute.

Versez le lait bouillant en commençant par une cuillère, puis deux, puis le reste  ( pour ne pas cuire les jaunes),  mélangez et remettez sur le feu moyen, fouettez constamment, la crème pâtissière va s’épaissir

Reversez la crème dans un bol propre, couvrez-la au contact avec du film alimentaire et laissez refroidir.

Au moins une heure après, fouettez la crème liquide bien ferme (mettez-la au congélateur 15 minutes avant, ca aide !!)

Ajoutez la crème fouettée à la crème pâtissière froide, délicatement, et mettez dans une poche à douille au frais.

### Pour le montage : 

Procédez au montage du fraisier : disposez un cercle ou coupez votre génoise selon la forme souhaitée. 

Avec une poche à douille, déposez une première couche de crème, 

Coupez les fraises en deux dans le sens de la hauteur et face tranchée vers l’extérieur

Plaquez bien les fraises contre le bord avec la crème, et complétez l'intérieur avec la crème et des morceaux de fraises

Recouvrez avec le 2e cercle de génoise, et décorez avec une poche à douille et de jolies petites montagnes de crème avec des fraises en guise de décoration, et de la menthe ciselée . 

Placez au frais entre 2 heures et une nuit avant de démouler et de servir.

**Astuce**: 

Ce dessert peut être servi avec des fruist de saison : abricots, pêches, framboises ou raisins, ananas, oranges ... faites vous plaisir ! 