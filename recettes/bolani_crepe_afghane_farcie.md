---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/bolani_crepe_afghane_farcie.jpeg
author: Stéphanie Labadie
description:  Bolani, crêpe afghane farcie 
title: Bolani, crêpe afghane farcie 
licence: CC-by-sa
categories: entrée 
niveau: moyen
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 30 min
layout: recette
---

# Pour la pâte :

- 350 g Farine de blé
- 1/2 Sachet Levure Boulangère
- Sel
- 12 cl Eau

...............................................

Dans un saladier : mélanger farine, levure et sel
Ajouter l'eau chaude progressivement et pétrir jusqu'à ce que la pâte se décolle
Couvrir d'un linge et réserver 2 heures à température ambiante pour faire lever la pâte

## Pour la farce: 

- 1 poireau
- 1 ciboule 
- 1/ botte de coriandre 
- 1/2 oignon
- 1 c à café Graines de coriandres moulu
- 1 c à café Garam Massala
- Sel
- Poivre
- Huile végétale

...............................................

Nettoyer et émincer le poireau, idem avec la ciboule et la coriandre 
Mixer par brèves impulsions pour garder de la mâche / texture 

Les faire suer à la poêle dans un peu d'huile d'olive avec sel, poivre et épices

Séparer la pâte en 5 boules

Les étaler en cercles et les garnir de farce

Replier la pâte en soudant bien les bords (utiliser un peu d'eau pour humidifier les bords)

Dorer à la poêle, 2 à 3 minutes de chaque côté

Servir avec la sauce au yaourt

### Pour la sauce au yaourt:

- 1 yaourt ou fromage blanc ou yaourt grec
- quelques feuilles de menthe fraiche ciselée 
- Une petite gousse d’ail hachée 
- Un peu de jus de citron Selon Goût
- 1 c à cafe de vinaigre balsamique ou de miel 
- Sel et poivre 

...............................................

Incorporez les ingrédients au Yaourt et mélanger

**ASTUCE**: 

Vous pouvez remplacer la farce par un reste de sauce bolognaise, ou un reste de gratin de légumes, ou d’autres herbes fatiguées au fond du frigo, selon votre inspiration !  