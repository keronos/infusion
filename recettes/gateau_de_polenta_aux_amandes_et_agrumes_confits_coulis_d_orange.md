---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/Gateau_de_polenta_aux_amandes_et_agrumes_confits_coulis_d_orange.jpeg
author: Stéphanie Labadie
description: Gateau de polenta aux amandes et agrumes confits, coulis d'oranges 
title: Gateau de polenta aux amandes et agrumes confits, coulis d'oranges  
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 16/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
layout: recette
---

# Ingrédients : 

- 90gr de poudre d’amande
- 145gr de polenta
- 100gr de sucre
- 1 sachet de Backing Powder ou 2 c à s de levure
- 1 pincée de sel
- 1 c à s de pavot
- 60ml d’huile d’olive
- 3 oeufs
- le zeste de 2 oranges et le jus de 3 oranges + une pour la garniture (ou d’un tout autre agrume, mandarine, orange sanguine, citron…)

........................................................

Allumez le four à 180°.

Mélangez la polenta, la poudre d’amandes, le sucre et la poudre à lever.

Ajoutez les zestes d’oranges, et les graines de pavot.

Pressez les fruits et incorporez le jus à la pâte. Ajoutez les jaunes d’oeufs et l’huile d’olive. 

Montez les blancs en neige puis incorporez les délicatement à l’appareil à gâteau.

Lavez les oranges et à l’aide d’un couteau qui coupe bien, coupez de très fines tranches (2mm d’épaisseur pas plus).

Dans le fond de votre moule, disposez de petits noisettes de beurre, 2 c à soupe de sucre de canne, puis disposez les rondelles d’orange en rosace. 

Versez 1 c à soupe de sucre par dessus, puis versez la pâte à gâteau par dessus et enfournez 30 à 40 minutes. 

## Pour le coulis d’orange: 

* Le jus de 4 oranges 
* 6 c à soupe de sucre 
* 2 graines de cardamome 

........................................................

Faire réduire votre jus, avec la cardamome et le miel dans une casserole à petit frémissement jusqu’à ce que sa consistance devienne sirupeuse. Laissez refroidir, votre sirop est prêt.

Pour dresser vos assiettes, mettez une belle cuillère de sirop d’orange dans l’assiette, puis disposez par dessus un part de gâteau. Et voilà, à table! 