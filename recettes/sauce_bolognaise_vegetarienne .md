---
author: Stéphanie Labadie
description:  Sauce Bolognaise végétarienne 
title: Sauce Bolognaise végétarienne  
licence: CC-by-sa
categories: plat
niveau: débutant 
date: 13/01/2022
nombre: 6 personnes
temps-cuisson: 45 min
temps-preparation: 30 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/sauce_bolognaise_vegetarienne.jpeg
layout: recette
---

# Ingrédients: 

* 3 carottes épluchées et coupées en gros morceaux 
* 2 oignon pelés et coupés en gros morceaux 
* 300 g de pleurotes
* 60 g de cèpes séchés 
* 4 gousses d’ail 
* 4 tomates pelées en conserves 
* 12 cl d’huile d’olive 
* 70 g de miso blanc 
* 4 c à soupe de concentré de tomates 
* 9 cl de sauce soja
* 2 c à café de graines de cumin 
* 180 g de lentilles brunes ou vertes séchées
* 100 g d’orge perlée 
* 1 l de bouillon de légumes ou volaille
* 160 de crème de coco 

## Déroulé: 

Préchauffez le four à 190 degrés 

Mettez les 6 premiers ingrédients dans un robot de cuisine et hachez les en donnant de brèves impulsions 

Sur une plaque de cuisson anti-adhésive, mélangez les légumes hachés avec l’huile, le miso, le concentré de tomates, la sauce soja et les graines de cumin. Enfournez pour 40 minutes, en remuant à mi cuisson, jusqu’à ce que les légumes soient titrés sur les bords et que le jus bouillonne. 

Réduisez le four à 180 degrés

Ajoutez tous les autres ingrédients, avec 25 cl d’eau et 1/3 de c à café de sel. 

Remuez en raclant les côtés et le fond à l’aide d’une spatule. Couvrez avec du papier aluminium et enfournez de nouveau pour 40 minutes. 

Une fois la cuisson terminée, sortez du four, retirez le papier aluminium, et poursuivez la cuisson pendant 5 minutes. Laissez reposer 15 minutes avant de servir, pour que les ingrédients absorbent encore le jus de cuisson. 

**Astuce**: 

Vous pouvez servir ces sauce bolognaise avec des pâtes évidemment, en accompagnement d'une volaille ou d'un poisson, ou avec du riz, de la semoule, des boulettes de polenta etc... Aussi, elle sera toujours meilleure le lendemain ! 