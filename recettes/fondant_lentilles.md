---
author: Stéphanie Labadie
description: Fondant aux lentilles et au citron
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/fondant-lentilles.jpg
title: Fondant au citron
licence: CC-by-sa
categories: plat
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 25 min
layout: recette
---

### Ingrédients

- 160 g de lentilles corail
- 4 oeufs
- 120 grammes de sucre roux
- 2 oranges
- 1 banane
- 100 g de beurre
- 1 gousse de vanille
- 40 g de maizena
- 80 de sucre ( pour le sirop)

### Déroulé

1. Cuire les lentilles corail en les couvrant d’eau jusqu'à ébullition et pendant 10 minutes.
2. Bien les égoutter puis y ajouter le sucre, la vanille, la banane écrasée et le zeste d’une orange, puis fouetter le mélange.
3. Séparez les jaunes et les blancs d'oeufs.
4. Ajouter les jaunes d'oeufs, le beurre fondu, la maïzena, puis incorporer les blancs d'oeufs montés en neige.
5. Verser dans un moule, sur papier sulfurisé et huilé, pour avoir une épaisseur de 2 cm et cuire 15 à 20 min à 160°C.
6. Sortir le gâteau lorsque la lame d'un couteau ressort sèche.

On peut déguster ce gâteau avec un sirop réalisé avec le jus de 2 oranges (dont celle zestée) et quelques oranges confites. Pour ce faire, incisez la peau en 4 quartiers et épluchez-les. Vous obtenez 4 grosses écorces.

1. Coupez-les en lamelles et retirez au maximum la chair blanche qui est très amère.
2. Versez dans une grande casserole d’eau froide, et plongez les écorces. Portez le tout à ébullition et laissez frémir 5 minutes.
3. Retirez les écorces et plongez-les dans de l’eau froide. Renouveler l’opération 3 fois.
4. Enfin, les faire faire mijoter avec petit feu avec le jus des deux oranges, et 40 g de sucre complet.
5. Les retirer avant que le sirop ne caramélise.

**Astuce**
On peut aussi servir ce fondant avec du mascarpone au sucre vanillé battu avec avec un soupçon de fève tonka.
