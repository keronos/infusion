---
author: Stéphanie Labadie
description: Salade fraîche de céleri, panais et pruneaux, sauce miso et jus de pomme
title: Salade fraîche de céleri, panais et pruneaux, sauce miso et jus de pomme 
licence: CC-by-sa
categories: entrée
niveau: débutant
date: 17/01/2022
nombre: 4 personnes
temps-cuisson: 0 min
temps-preparation: 20 min
layout: recette
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/salade_de_celeri_panais_et_pruneaux_sauce_miso_et_jus_de_pomme.jpeg
---

# Pour la salade : 

* 250 de celeri rave 
* 70 g d’amandes concassées
* 2 panais 
* 1 petit pomme 
* 4 pruneaux 
* 3 c à café de miso 
* 7 cl de jus de pomme 
* 1 pointe de jus de citron 

..................................

Pelez le céleri rave et râpée à la grosse grille . Faites de même avec le panais, et la pomme. 

Mélanger le miso avec le jus de pomme, et le jus de citron si besoin. 

Emincez grossièrement le pruneau. 

Mélangez tous les ingrédients ensemble avec quelques feuille de coriandre, ou quelques noix /  amandes si l’appétit vous en dit! 

