---
author: Stéphanie Labadie
description: Tourin à l'ail 
title: Tourin à l'ail 
licence: CC-by-sa
categories: plat 
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/tourin_a_l_ail.jpeg
layout: recette
---

# Ingrédients: 

* 2 belles têtes d’ail rose 
* 1 œuf
* 1 litre d’eau
* 1 petite CC de vinaigre de vin ou balsamique
* Gros sel
* Poivre
* Graisse de canard
* Quelques croutons de pain complet

........................................................

Emincez 4 têtes d’ail et faites-les revenir à feu vif dans une cuillère à soupe de graisse de canard, ou à défaut de beurre. Remuez en permanence afin d’enlever l’amertume de l’ail. 

Ajoutez alors la farine en remuant vivement et dès que le mélange est homogène, ajoutez sans attendre l'eau, petit à petit et toujours en remuant pour éviter les grumeaux. Salez, poivrez et laissez bouillir vingt minutes. 

Une fois à ébullition, ajoutez un blanc d’œuf et remuez pour voir apparaître les filaments (gardez votre feu vif pour cette étape). 

Dans un bol, mélangez une cuillère à café de vinaigre et le jaune d’œuf restant. Ajoutez cela à la préparation précédente et laissez blanchir. 

Versez votre tourin blanchi à l’ail dans une soupière. Vous pouvez le servir avec des croutons de pain complet que vous aurez grillé. 

**Astuce**: 

Pour une présentation plus soignée et en faire profiter les enfants, vous troquer les vermicelles contre un mixage de soupe; pour obtenir un effet plus velouté! 

