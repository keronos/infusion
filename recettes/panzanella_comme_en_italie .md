---
author: Stéphanie Labadie
description: Panzanella comme en Italie   
title: Panzanella comme en Italie    
licence: CC-by-sa
categories: plat
niveau: débutant
date: 17/01/2022
nombre: 4 à 6 personnes
temps-cuisson: 0 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/panzanella_comme_en_Italie.jpeg
layout: recette
---

# Ingrédients:

* 4 tranches de campagne et au levain préalablement grillé, qui gardera sa structure une fois mouillé 
* 2 cuil. à soupe de vinaigre de vin rouge
* 6 cuil. à soupe d’huile d’olive
* 600 g de tomates
* 1 petit concombre
* 1 oignon rouge
* 1 bouquet de basilic
* 100 g d’olives noires (ou violettes) dénoyautées
* Sel, poivre

## Déroulé: 

Coupez le pain en gros cubes.

Mélangez l’huile, le vinaigre et 10 cl d’eau. Versez sur le pain et laissez gonfler 15 minutes.

Pendant ce temps, pelez et épépinez le concombre. Coupez les tomates, le concombre et l’oignon en cubes. Rincez et séchez les feuilles de basilic.

Essorez le pain et déposez la moitié dans un saladier.

Recouvrez de la moitié des légumes, ajoutez la moitié des olives.

Salez et poivrez.

Déposez le reste du pain et recouvrez du reste des légumes et des olives.

Salez, poivrez et laissez reposer 1 heure à température ambiante pour que les saveurs se mêlent.

Au moment de servir, ajoutez les feuilles de basilic, 2 dernières cuillères à soupe d’huile d’olive, et mélangez bien