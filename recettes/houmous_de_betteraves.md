---
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/houmous_de_betteraves.jpeg
author: Stéphanie Labadie
description: Houmous de betteraves 
title: Houmous de betteraves 
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 17/01/2022
nombre: 4 à 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
---

# Déroulé: 

Préparer un houmous classique ( cf recettes houmous à l'ail confit) et rajoutez y les ingrédients suivants en les mixant  : 

- 150 g de betterave cuite 
- 100 g d’aneth ( 70 g pour le houmous, 30 g pour la déco) 
- Quelques noix torréfiées et concassées 

Servir avec de la féta, des noix torréfies et de l’aneth 