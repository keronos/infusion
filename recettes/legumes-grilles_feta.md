---
categories: entrée
layout: recette
author: Bénédicte Lambert
description: Légumes grillés pour l'apéro.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/legumes-feta.jpg
title: Légumes grillés à la crême de feta
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 10 min
temps-preparation: 10 min
---

Pour 12 bouchées

### Ingrédients

- tranches de courgettes, poivrons, aubergines
- 200 g de feta
- 100g de fromage crémeux (type carré frais)
- 1 botte de basilic
- 2 cs d'huile d'olive
- pique en bois

### Préparation

1. Ecrasez la féta à la fourchette
2. Dans un grand bol mélangez-la avec le formage, le basilic hâché, l'huile d'olive. Salez si nécessaire, poivrez
3. Préparez les légumes au four
4. Laissez refroidir
5. Mettez une grosse noix de farce au formage sur chacun des légumes grillés, roulez et fermez avec une pique en bois.
6. Servez frais
