---
author: Stéphanie Labadie
description: Banana Bread  
title: Banana Bread  
licence: CC-by-sa
categories: plat
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 à 40 min
temps-preparation: 20 min
layout: recette
image_url: media/banana_bread.jpg
---

# Ingrédients: 

*4 bananes (un peu mûres c’est encore mieux…)
* 3 oeufs
* 330 gr de farine d’épeautre (ou de blé si pas d’épeautre!) 
* 150gr de beurre
* 1 sachet de Backing Powder (ou 10gr de levure)
* 1 à 2 poignées de noix, d’amandes, de noisettes, de graines de courge, sésame ou tournesol

## Déroulé : 

Allumer le four à 180°.

Faire fondre le beurre et le laisser tiédir dans un beau plat.

Ajouter les oeufs, et battre au fouet pour mélanger. 

Ecraser les bananes et mélanger. 

Incorporer la farine la en plusieurs fois à la pâte avec le Backing Powder et ajouter des noix et autres graines ( sésame, courge, tournesol etc.

Chemiser un moule cake, et enfourner pendant 40 à 50 minutes (vérifier la cuisson avec une pointe de couteau). 

Déguster tiède ou refroidi.  

**Astuces**: 

A servir avec une confiture d’abricots minutes ( 2 signées d’abricots secs ramollis dans 10 cl de jus de pomme chaud, puis mixés avec ). 

Ou avec du fromage ça marche aussi, ou les tartines du matin etc… ou tout seul, c’est parfait ! 