---
author: Bénédicte Lambert
description: alternative à la tapenade
image_url: https://github.com/bndct-lmbrt/recettes-yoga-cuisine/raw/master/medias/caviar-olives.jpg
title: Caviar d'olives
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 12/05/2017
nombre: 4/8 personnes
temps-cuisson: 0 min
temps-preparation: 10 min
temps-repos: 15 min
layout: recette
---

## Ingrédients

- 8 tomates séchées
- 1 sachet d'olives noires dénoyautées
- 1 gousse d'ail
- 8 branches de basilic
- huile d'olive

## Déroulé

1. Faire tremper les tomates séchées dans un récipient d'eau tiède minimum 15 min.
2. Mixer les olives avec les tomates, l'ail et le basilic.
3. Ajouter de l'huile si la texture n'est pas assez onctueuse. (Cela dépend du mixer)

**Astuces**

Ajouter de l'artichaut, des poivrons grillés ou des amandes.
