---
author: Stéphanie Labadie
description: Crème de lentilles et sa garniture de pomme, sardines et aneth
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/creme-lentilles.jpg
title: Crème de lentilles
licence: CC-by-sa
categories: accompagnement
niveau: débutant
date: 15/02/2019
nombre: 6 personnes
temps-cuisson: 40 min
temps-preparation: 15 min
layout: recette
---

### Ingrédients

- 1 oignon
- 200 g de lentilles corail
- 2 gousses d’ail
- 1 petite butternut
- 1 grosse patate douce
- 1 c à café de curry
- 1 c à café de cannelle
- 40 cl de crème de coco
- Le jus d’1/2 citron
- 2 feuilles de laurier
- 1 pommes Boskoop
- 20 g de coco râpée torréfiée

### Déroulé

1.Faire revenir l’oignon ciselé dans une grande casserole, y ajouter la butternut et la patate
douce coupées en gros cubes.

1. Laisser rissoler avec les épices, rajouter les lentilles et l’ail écrasé.
2. Mélanger puis mouiller à hauteur, moitié eau, moitié lait de coco.
3. Laisser cuire 30 minutes.
4. Mixer, assaisonner avec du sel, et une pointe de jus de citron si besoin.

Dressage: verser la soupe dans des assiettes ou des bols, avec quelques quartiers de pommes
coupés à la mandoline, quelques brins d’aneth ou de la noix de coco râpée.
Ou des croûtons de pain revenus à la poêle dans du beurre.

**Astuces**

Vous pouvez réaliser le même type de soupe avec d’autres restes de légumineux: petits pois,
pois cassés, haricots blancs, lentilles vertes: l’association avec la butternut les patates douces et
le lait de coco fonctionne aussi très bien!

Recycler le reste de soupe: en faire un flan ou une quiche en rajoutant des oeufs et de la
fécule de mais. Avec des cubes de tofu ou de fromage, des restes de jambon ou de poulet, des
graines torréfiées etc…
