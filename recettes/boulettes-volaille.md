---
categories: plat
layout: recette
author: Bénédicte Lambert
description: sashimis parfumés au thon.
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/boulettes-volaille.png
title: Mini boulettes de volaille aux herbes
licence: non définie
niveau: débutant
date: 12/09/2018
nombre: 6 personnes
temps-cuisson: 15 min
temps-preparation: 10 min
---

### Ingrédients pour 6 personnes

- 3 blancs de poulet
- 4 oignons nouveaux
- 3 cs d'huile d'olive
- 2 gousses d'ail nouveau
- 4 cs d'herbes fraîches ciselées (persil plat, coriandre, menthe)
- 1 radis noir
