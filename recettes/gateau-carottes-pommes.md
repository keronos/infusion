---
author: Stéphanie Labadie
description: Gâteau aux carottes et à la pomme, et à l’huile d’olive et aux noisettes
image_url: https://github.com/bndct-lmbrt/mes-recettes/raw/master/medias/gateau-carottes.jpg
title: Gâteau à la carotte et aux pommes
licence: CC-by-sa
categories: dessert
niveau: débutant
date: 12/10/2021
nombre: 6 personnes
temps-cuisson: 25 min
temps-preparation: 15 min
layout: recette
---

## Ingrédients

* 400 g de carottes
* 200 g de pommes
* 2 oeufs
* 90 g de sucre de canne
* 50 g d'huile d’olive
* 1 pot de yaourt bulgare
* 1 pincée de cannelle
* 200 g de farine d’épeautre
* 30 g de poudre d'amande
* 1/2 cuillère à café de levure chimique
* 1/2 cuillère à café de bicarbonate de soude
* 1 cuillère à soupe d'amande concassées

## Déroulé

Râpez les pommes et les carottes.

Dans un bol mélangez les oeufs, le sucre, ajoutez la crème et l'huile puis la farine, la poudre d’amande, le yaourt, la levure et le bicarbonate de soude, la cannelle.

Ajoutez les carottes et les pommes délicatement

Versez la préparation dans un moule à cake, parsemez avec les noisettes concassées, puis enfournez
dans votre four préchauffé à 180°C pendant environ 40 - 45 minutes.

Servir avec une crème chantilly à la fleur d’oranger ou de la compote pour une version plus light!