---
author: Stéphanie Labadie
description: Petits pains aux olives, parmesan et tomates séchées  
title: Petits pains aux olives, parmesan et tomates séchées  
licence: CC-by-sa
categories: accompagnement 
niveau: débutant
date: 04/01/2022
nombre: 6 personnes
temps-cuisson: 30 min
temps-preparation: 20 min
image_url: https://gitlab.com/keronos/infusion/-/raw/master/recettes/media/petits_pains_aux_olives_parmesan_tomates_sechees.jpeg
layout: recette
---

# Ingrédients : 

* 200 g farine 
* 100 g de farine de sarrasin 
* 100 g de farine d’épeautre
* 1 sachet de levure de boulanger
* 1 cuillère à café de sucre
* 1 pincée de sel
* 3 cuillères à soupe d'huile d'olive
* 200 ml d'eau tiède
* 2 poignées d'olives dénoyautées coupées en 2
* 10 tomates séchées coupées en dés
* 4 cuillères à soupe de parmesan râpé

## Déroulé : 

Mettez l'eau, l'huile, le sel et le sucre dans un saladier. Ajoutez la farine et la levure. Mélangez et pétrissez le tout. Laissez reposer une heure sous un linge. 

Coupez votre pâte en deux, étalez ces deux boules, puis parsemez de la moitié de parmesan sur chaque pâte.

Déposez une ligne d'olives et des tomates séchées et roulez la pâte autour de cette ligne. Ajoutez une autre ligne d'olives, roulez la pâte et continuez jusqu'à utilisation de toutes les olives.

Une fois que le roulé est terminé, formez un anneau puis coupez le en morceaux de 3 à 4 cm d’épaisseur sur sa longueur. 

Ou refaçonnez une grosse pâte et divisez là en plusieurs pâtons: façonnez les petits pains de la forme souhaitée. Laisser reposer encore 15 minutes, puis enfournez à 180 degrés pendant 20 minutes.

**Astuces**: 

A déguster avec quelques tartinades pour l'apéro, ou en compagnie d'une bonne fondue savoyarde, ou tout simplement tel quel! 