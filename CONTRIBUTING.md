---
title: Modèle de fiche recette
layout: recette
categories: info
---

Ce dépôt de recette fonctionne avec des fichiers rédigés en markdown et disposant d'informations en en-tête (frontmatter) pour faciliter l'affichage à partir d'un gabarit pré-défini (layout: recette)
Un [modèle de fiche](recettes/modeleRecette.md) est disponible dans le depôt afin de faciliter la contribution.

Le pré-requis actuel est de créer un compte sur le site gitlab afin de pouvoir créer ou modifier des fichiers dans cet espace de publication.

## Mise en forme

Pour permettre l'affichage des recettes sur ce dépôt, il est préconisé de spécifier quelques informations dans l'entête des fichiers

Ces métadonnées permettent de standardiser l'affichage et de faciliter la navigation grâce notamment au mécanisme des catégories

- author: Le créateur ou la créatrice de la recette. Par exemple Matthieu Dechart
- description: une courte description de la recette. Par exemple "préparation du carrelet en filet accompagné des légumes de saison."
- image_url: l'adresse d'une image d'illustration de la recette. Dans la mesure du possible il est préconisé de n'associer que des images dont la largeur est égale ou inférieure à 500 px. Cette image peut être stockée dans le dossier des recettes de ce dépôt ou quelque part sur Internet. Par exemple "https://raw.githubusercontent.com/akakeronos/recette-gourmandignes/master/images/filet-carrelet.jpg"
- title: le titre de la recette. Par exemple "Filets de carrelet aux légumes de printemps"
- licence: la licence de publication de la recette. Par exemple "CC-by-sa"
- categories: la catégorie de la recette. Par exemple "poisson". Actuellement les catégories utilisées sont "poisson, légumes, tarte, dessert, entrée, salade"
- niveau: le niveau de difficulté d'exécution de la recette. Par exemple "débutant"
- date: la date de publication/création de la recette Par exemple "12/05/2017"
- nombre: lorsque les quantités sont spécifiées dans la recettes, il est possible d'indiquer en en-tête le nombre de personnes auxquelles correspondent ces quantités. Par exemple "8 personnes"
- temps-cuisson: le temps de cuisson si pertinent. Par exemple "30/40 min"
- temps-preparation: le temps de préparation. Par exemple "25 min"
- layout: le modèle de mise en forme de la recette. Actuellement, à moins que vous ne souhaitiez proposer un modèle complémentaire d'affichage, le modèle par défaut est "page"

## Méta

L'affichage des catégories et la mise en forme des recettes est rendu possible grâce à l'utilisation de données sous la forme clé: valeur

Ces valeurs doivent être précédées de trois tirets et conclues par trois tirets sur 2 lignes séparées. Elles doivent être écrites sous la forme méta: valeur

Exemple

```
---
title: Mon titre
---
```

# Syntaxe Markdown

Markdown est une syntaxe légère et facile à utiliser pour mettre en forme des contenus à afficher sur le Web.

Grâce à cette syntaxe vous pouvez mettre en forme du texte (gras, italique), ajouter des images, créer des listes (ordonnées ou liste à puces) ou des liens hypertexte. Pour faire simple le markdown est composé de texte et de quelques caractères non alphabétiques comme # ou \*.

## Exemples

### Text

      C'est très simple de mettre des mots en **gras** et d'autres en _italique_ avec Markdown. Vous pouvez même définir des [liens vers Qwant!](http://qwant.com)

C'est très simple de mettre des mots en **gras** et d'autres en _italique_ avec Markdown. Vous pouvez même définir des [liens vers Qwant!](http://qwant.com)

### Listes

    Parfois vous voulez des listes ordonnées:

    1. Un
    2. Deux
    3. Trois
    Parfois vous voulez des listes à puce

    * Commence une liste avec une étoile
    * Continue!
    *
    *

1. Un
2. Deux
3. Trois

- Commence une liste avec une étoile

### Images

    Si vous voulez intégrer une image, voilà comment rédiger le lien:

    ![Image de Yaktocat](https://octodex.github.com/images/yaktocat.png)

![Image de Yaktocat](https://octodex.github.com/images/yaktocat.png)

### Niveaux de titre et citation

    # Documents structurés

    Parfois c'est utile de structurer son contenu avec différetns niveaux de titre. Débutez votre ligne avec un  `#` pour créer un titre. PLusieurs `##` à la suite indiquent un niveau de titre inférieur.

    ### Ceci est un titre de niveau trois

    Vous pouvez utiliser d'un `#` à 6 `######` pour différents niveaux de titre.

    Si vous voulez citer quelqu'un, utilise le caractère use `>` avant la ligne:

    > Matté : une boisson à base de plante énergisante.

### Ceci est un titre de niveau trois

> Matté : une boisson à base de plante énergisante.

## Guide de la syntaxe

Vous trouverez ci-dessous ces éléments de syntaxe que vous pouvez utiliser

### Titres

    # This is an <h1> tag

    ## This is an <h2> tag

    ###### This is an <h6> tag

### Emphase

    *Ce texte sera en italique*
    _This will also be italic_

    **This text will be bold**
    __This will also be bold__

    _Vous **pouvez** les combiner_

### Listes

    Non ordonnée
    - élément 1
    - élément 2
        - élément 2a
        - élément 2b

    Ordonnée

    1. Elément 1
    2. Elément 2
    3. Elément 3
        1. Elément 3a
        2. Elément 3b

### Images

    ![GitHub Logo](/images/logo.png)
    Format: ![Alt Text](url)

### Links

    http://github.com - automatic!
    [GitHub](http://github.com)

### Citation

Comme disait Bookchin :

> Les principes écologiques qui présidaient au fonctionneùent des sociétés organiques se retrouvent dans les prinbcipes sociaux qui définissent l'utopie
