---
title: A propos de ce site
layout: recette
categories: info
---

# C'est quoi ?

Ce site vise à faciliter la publication de recettes de cuisine en licence libre.

Il est actuellement publié à l'adresse [https://keronos.gitlab.io/infusion](https://keronos.gitlab.io/infusion)

# Comment ?

Des recettes peuvent y être publiées en utilisant la syntaxe markdown (voir le [guide de contribution](CONTRIBUTING.html) pour plus d'explication sur cette de mise en forme).

Un ensemble de propriétés peuvent y être associé afin de permettre la classification.

Ce site utilise jekyll comme générateur de pages statiques. Les fichiers .\_config.yml et Gemfile définissent la manière dont le site est fabriqué et déployé sur gitlab en utilisant le mécanisme des gitlab pages

Dans le dossier \_includes les fichiers categorie.html et listByCategorie.html permettent l'affichage d'un menu à partir des catégories de recettes disponibles dans les métadonnées des fichiers .md et l'affichage des recettes associées à une catégorie

# Comment contribuer ?

1. En créant un compte sur gitlab puis en poussant une recette vers ce dépôt en respectant le formalisme disponible dans le modèle de fiche disponible [ici](CONTRIBUTING.html)
2. En envoyant une recette par email à l'adresse [mailto:contact@gourmandignes.org](contact@gourmandignes.org)

# Crédits

Site développé avec l'aide de @pntbr à partir de l'application [Jekyll](https://www.jekyll.com)

# Licence

Le code et les recettes sont publiés sous la licence MIT
