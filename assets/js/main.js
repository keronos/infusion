// pour afficher les div en fonction de la valeur du hash categorie
document.addEventListener("DOMContentLoaded", evt => hideDivs())
function hideDivs () {
  const divs = document.querySelectorAll("div.categorie")
  divs.forEach(div => div.style.display = "none")
}

function displayOtherDiv (label) {
  hideDivs()
  const selectDiv = document.querySelector(`div.${label}`)
  selectDiv.style.display = "block"

}

window.addEventListener("hashchange", evt => {
   const categorie = decodeURI(document.location.hash.substring(1))
   displayOtherDiv(categorie)
})
